#pragma once
#include "stdafx.h"

#include "TestWindow.h"
#include "FastDelegate.h"
#include "Camera.h"
#include <memory>
#include "DebugEngineEvent.h"

using namespace GraphicsEngine::Math::DirectX;
using namespace GameEngine::Application::Testing;
using namespace GUtils::Graphics::Camera;
using namespace DirectX;
using namespace fastdelegate;
using namespace GameEngine::Logic::Events::Default;

TestWindow::TestWindow() : GameWindow()
{
	m_pEventManager = new EventManager();
	m_pProcessManager = new ProcessManager();
	m_pKeyboardController = new TestKeyboardControls();

}

TestWindow::~TestWindow()
{
	SAFE_DELETE(m_pKeyboardController);
	SAFE_DELETE(m_pProcessManager);
	SAFE_DELETE(m_pEventManager);
}

void TestWindow::SetCamera(IEventDataPtr pEventData)
{
	std::shared_ptr<MoveCameraEvent> pMoveCameraEvent = std::static_pointer_cast<MoveCameraEvent>(pEventData);
	GUtils::Graphics::Camera::Camera camera; 
	pMoveCameraEvent->GetEnginePtr()->GetCamera(camera);
	const float deltaTime = pMoveCameraEvent->GetDeltaTime();

	Vector3D position;
	Vector3D focus;

	camera.GetPosition(position);
	camera.GetFocus(focus);

	Vector3D forwardDirection = focus - position;
	forwardDirection.NormalizeThis();

	if (pMoveCameraEvent->GetDirection() == MoveCameraEvent::Forward)
	{
		position.zf += forwardDirection.zf*(deltaTime * 3);
		focus.zf += forwardDirection.zf*(deltaTime * 3);

		position.xf += forwardDirection.xf*(deltaTime * 3);
		focus.xf += forwardDirection.xf*(deltaTime * 3);

		position.yf += forwardDirection.yf*(deltaTime * 3);
		focus.yf += forwardDirection.yf*(deltaTime * 3);

		printf("Forward\r\n");
	}
	else if (pMoveCameraEvent->GetDirection() == MoveCameraEvent::Left)
	{
		position.xf -= deltaTime * 3;
		focus.xf -= deltaTime * 3;
		printf("Left\r\n");
	}
	else if (pMoveCameraEvent->GetDirection() == MoveCameraEvent::Right)
	{
		position.xf += deltaTime * 3;
		focus.xf += deltaTime * 3;
		printf("Right\r\n");
	}
	else if (pMoveCameraEvent->GetDirection() == MoveCameraEvent::Backward)
	{
		position.zf -= deltaTime * 3;
		focus.zf -= deltaTime * 3;
		printf("Backward\r\n");
	}

	camera.SetPosition(position);
	camera.SetFocus(focus);

	m_pGraphicsEngine->ApplyCamera(camera);
}

void TestWindow::SetDebugEvent(IEventDataPtr pEventData)
{
	std::shared_ptr<DebugEngineEvent> pMoveCameraEvent = std::static_pointer_cast<DebugEngineEvent>(pEventData);
	const bool doDebug = pMoveCameraEvent->GetDebug();

	m_pGraphicsEngine->SetRasterizerState("RASTERIZER_SOLID_CULL_BACK");
	m_pGraphicsEngine->SetScreenDebug(false);
	if (doDebug)
	{
		m_pGraphicsEngine->SetScreenDebug();
		m_pGraphicsEngine->SetRasterizerState("RASTERIZER_WIREFRAME_CULL_BACK");
	}
}

BOOL TestWindow::Initialize(IGraphicEngine* pGraphicEngine, const EngineOptions& options)
{
	if (!GameWindow::Initialize(pGraphicEngine, options))
		return FALSE;
	

	EventListenerDelegate moveCameraFunc = MakeDelegate(this, &TestWindow::SetCamera);
	m_pEventManager->AddListener(moveCameraFunc, 12345);

	EventListenerDelegate debugEngineFunc = MakeDelegate(this, &TestWindow::SetDebugEvent);
	m_pEventManager->AddListener(debugEngineFunc, 0x5390C70F);

	GUtils::Graphics::Camera::Camera camera;
	m_pGraphicsEngine->GetCamera(camera);
	
	uint32 id;

	std::string model = m_ResourceDirectories.GetModelDirectoryPath();
	model += "cube_normal_uv.gmodel";
	m_pGraphicsEngine->RegisterModelFromPath(model.c_str(), id);
	//m_pGraphicsEngine->RegisterModelFromPath("C:\\Users\\Paul\\Documents\\CubeUVTEST.gmodel", id);
	//m_pGraphicsEngine->RegisterModelFromPath("C:\\Users\\Paul\\Source\\Repos\\bin_work\\Models\\cube_normal.gmodel", id);


	if (!m_pGraphicsEngine->AssignVertexShaderToModel(id, "MatLightVS"))
		return FALSE;
	if (!m_pGraphicsEngine->AssignPixelShaderToModel(id, "MatLightPS"))
		return FALSE;
	//if (!m_pGraphicsEngine->AssignVertexShaderToModel(id, "TexLightVS"))
	//	return FALSE;
	//if (!m_pGraphicsEngine->AssignPixelShaderToModel(id, "TexLightPS"))
	//	return FALSE;

	std::string teapotModel = m_ResourceDirectories.GetModelDirectoryPath();
	teapotModel += "teapot.obj";
	m_pGraphicsEngine->RegisterModelFromPath(teapotModel.c_str(), id);
	//m_pGraphicsEngine->RegisterModelFromPath("C:\\Users\\gz9jlb\\Downloads\\cube_simple.obj", id);
	if (!m_pGraphicsEngine->AssignVertexShaderToModel(id, "test_VS"))
		return FALSE;
	if (!m_pGraphicsEngine->AssignPixelShaderToModel(id, "test_PS"))
		return FALSE;

	//m_pGraphicsEngine->RegisterModelFromPath("C:\\Users\\Paul\\Documents\\CubeUVTEST.gmodel", id);
	m_pGraphicsEngine->RegisterModelFromPath(model.c_str(), id);
	//m_pGraphicsEngine->RegisterModelFromPath("C:\\Users\\gz9jlb\\Downloads\\cube_simple.obj", id);
	if (!m_pGraphicsEngine->AssignVertexShaderToModel(id, "TexLightVS"))
		return FALSE;
	if (!m_pGraphicsEngine->AssignPixelShaderToModel(id, "TexLightPS"))
		return FALSE;
	//if (!m_pGraphicsEngine->AssignVertexShaderToModel(id, "MatLightVS"))
	//	return FALSE;
	//if (!m_pGraphicsEngine->AssignPixelShaderToModel(id, "MatLightPS"))
	//	return FALSE;

	m_pGraphicsEngine->RegisterModelFromPath(model.c_str(), id);
	if (!m_pGraphicsEngine->AssignVertexShaderToModel(id, "TexLightVS"))
		return FALSE;
	if (!m_pGraphicsEngine->AssignPixelShaderToModel(id, "TexLightPS"))
		return FALSE;

	if (!m_pGraphicsEngine->CopyToNewModel(4, id))
		return FALSE;
	if (!m_pGraphicsEngine->AssignVertexShaderToModel(id, "TexLightVS"))
		return FALSE;
	if (!m_pGraphicsEngine->AssignPixelShaderToModel(id, "TexLightPS"))
		return FALSE;

	std::string seafloorTexture = m_ResourceDirectories.GetTextureDirectoryPath();
	seafloorTexture += "seafloor.dds";
	m_pGraphicsEngine->RegisterTextureToModel(3, seafloorTexture.c_str(), "SAMPLER_LINEAR");
	m_pGraphicsEngine->RegisterTextureToModel(4, seafloorTexture.c_str(), "SAMPLER_LINEAR");
	m_pGraphicsEngine->RegisterTextureToModel(5, seafloorTexture.c_str(), "SAMPLER_LINEAR");
	//m_pGraphicsEngine->RegisterTextureToModel(3, "C:\\Users\\Paul\\Source\\Repos\\vs2013-directx-tutorials\\Tutorial07\\seafloor.dds", "SAMPLER_LINEAR");
	//m_pGraphicsEngine->RegisterTextureToModel(4, "C:\\Users\\Paul\\Source\\Repos\\vs2013-directx-tutorials\\Tutorial07\\seafloor.dds", "SAMPLER_LINEAR");
	//m_pGraphicsEngine->RegisterTextureToModel(5, "C:\\Users\\Paul\\Source\\Repos\\vs2013-directx-tutorials\\Tutorial07\\seafloor.dds", "SAMPLER_LINEAR");
	//C:\Users\Paul\Source\Repos\vs2013-directx-tutorials\Tutorial07

	DirectionalLight directionalLight1;
	ZeroMemory(&directionalLight1, sizeof DirectionalLight);
	directionalLight1.Ambient.x = directionalLight1.Ambient.y = directionalLight1.Ambient.z = 0.50f;
	directionalLight1.Diffuse.x = 1.0f;
	directionalLight1.Diffuse.y = 1.0f;
	directionalLight1.Diffuse.z = 1.0f;
	//directionalLight.Specular.x = 0.2f;
	//directionalLight.Ambient.w = 16.0f;
	directionalLight1.Diffuse.w = 16.0f;
	directionalLight1.Specular.w = 16.0f;
	directionalLight1.Direction = GUtils::GMath::Point3D<float32>(1.0f, 0.0f, 0.0f);

	DirectionalLight directionalLight2 = directionalLight1;
	directionalLight2.Direction = GUtils::GMath::Point3D<float32>(-1.0f, 0.0f, 0.0f);



	m_pGraphicsEngine->RegisterDirectionalLight(directionalLight1, id);
	m_pGraphicsEngine->RegisterDirectionalLight(directionalLight2, id);

	PointLight pointLight1;
	ZeroMemory(&pointLight1, sizeof PointLight);
	pointLight1.Ambient.x = .1f;
	pointLight1.Specular.x = .2f;
	pointLight1.Diffuse.x = .8f;
	pointLight1.Position.x = 0.0f;
	pointLight1.Position.y = 0.0f;
	pointLight1.Position.z = 25.0f;
	pointLight1.Range = 10;
	pointLight1.Attenuation = GUtils::GMath::Point3D<float32>(0.0f, 0.5f, 0.0f);
	//pointLight.Attenuation 

	PointLight pointLight2 = pointLight1;
	pointLight1.Position.x = -25;

	PointLight pointLight3 = pointLight1;
	pointLight1.Position.x = 25;
	m_pGraphicsEngine->RegisterPointLight(pointLight1, id);
	m_pGraphicsEngine->RegisterPointLight(pointLight2, id);
	m_pGraphicsEngine->RegisterPointLight(pointLight3, id);

	SpotLight spotLight1;
	ZeroMemory(&spotLight1, sizeof SpotLight);
	spotLight1.Specular.y = 0.5f;
	spotLight1.Diffuse.y = 0.5f;
	spotLight1.Position.x = -4.0f;
	spotLight1.Position.y = 5.0f;
	spotLight1.Position.z = 10.0f;
	spotLight1.Attenuation = GUtils::GMath::Point3D<float32>(0.0f, 0.5f, 0.0f);
	spotLight1.Range = 7.0f;
	spotLight1.Direction = GUtils::GMath::Point3D<float32>(0.0f, -1.0f, 0.0f);

	SpotLight spotLight2 = spotLight1;
	spotLight2.Position.x = 12.0f;
	spotLight2.Range = 15.0f;

	m_pGraphicsEngine->RegisterSpotLight(spotLight1, id);
	m_pGraphicsEngine->RegisterSpotLight(spotLight2, id);

	return TRUE;
}

BOOL TestWindow::CleanUp()
{
	EventListenerDelegate moveCameraFunc = MakeDelegate(this, &TestWindow::SetCamera);
	m_pEventManager->RemoveListener(moveCameraFunc, 12345);

	EventListenerDelegate debugEngineFunc = MakeDelegate(this, &TestWindow::SetDebugEvent);
	m_pEventManager->RemoveListener(debugEngineFunc, 0x5390C70F);

	return GameWindow::CleanUp();
}

BOOL TestWindow::Execute()
{
	return GameWindow::Execute();
}

BOOL TestWindow::UpdateGameState(float deltaTime)
{
	// Temp Logic to move models
	// ========================================================================================
	//static float t = 0.0f;
	//t += (float)XM_PI * 0.00125f;

	//static float x = 0.0f;
	//x += deltaTime*.5;

	//// Update Engine & game model
	//XMMATRIX spinY = XMMatrixRotationY(t);
	//XMMATRIX spinX = XMMatrixRotationX(t);
	//XMMATRIX translate = XMMatrixTranslation(x, 0.0f, 0.0f);
	//XMMATRIX scale = XMMatrixScaling(1, 1, 1);
	//XMMATRIX world = spinY * spinX * translate * scale;

	//ConstantBuffer cb;
	//cb.worldViewProjection = world;
	//cb.worldViewProjection = cb.worldViewProjection;
	//m_pGraphicsEngine->TransformModel(1, cb);

	//ConstantBuffer cb2;
	//translate = XMMatrixTranslation(0.0f, 0.0f, (-1)*x);
	//world = spinY * spinX * translate * scale;
	//cb2.worldViewProjection = world;
	//cb2.worldViewProjection = cb2.worldViewProjection;
	//m_pGraphicsEngine->TransformModel(2, cb2);


	ConstantBuffer cb2;
	cb2.worldViewProjection = XMMatrixScaling(.025f, .025f, .025f) * XMMatrixTranslation(-2, 0, 0);
	m_pGraphicsEngine->TransformModel(2, cb2);

	cb2.worldViewProjection = XMMatrixScaling(.025f, .025f, .025f) * XMMatrixTranslation(4, 0, 0);
	m_pGraphicsEngine->TransformModel(2, cb2);

	cb2.worldViewProjection = XMMatrixScaling(.025f, 0.025f, 0.025f) * XMMatrixTranslation(4, 4, 0);
	m_pGraphicsEngine->TransformModel(2, cb2);

	ConstantBuffer cb3;
	cb3.worldViewProjection = XMMatrixScaling(12, 1, 1) * XMMatrixTranslation(0, 0, 5);
	m_pGraphicsEngine->TransformModel(3, cb3);

	ConstantBuffer cb4;
	cb4.worldViewProjection = XMMatrixScaling(5, 1, 1) * XMMatrixTranslation(0, -3, 1);
	m_pGraphicsEngine->TransformModel(4, cb4);

	ConstantBuffer cb5;
	cb5.worldViewProjection = XMMatrixScaling(1, 3, 1) * XMMatrixTranslation(0, 2, 3);
	m_pGraphicsEngine->TransformModel(5, cb5);

	//ScreenTextParams params;
	//params.Text = L"Testing system";
	//params.Color.x = 1.0f; params.Color.y = 1.0f; params.Color.z = 1.0f; params.Color.w = 1.0f;
	//params.Position.x = 100; params.Position.y = 100;
	//m_pGraphicsEngine->WriteScreenText(L"Arial08", params);

	//params.Color.y = 0.0f; params.Color.z = 0.0f;
	//params.Position.x = 100; params.Position.y = 400;
	//m_pGraphicsEngine->WriteScreenText(L"Arial36", params);

	//static float lightTest = -1.0f;
	//DirectionalLight directionalLight;
	//ZeroMemory(&directionalLight, sizeof DirectionalLight);
	//directionalLight.Ambient.x = 0.0f;
	//directionalLight.Diffuse.x = 1.0f;
	//directionalLight.Diffuse.y = 1.0f;
	//directionalLight.Diffuse.z = 1.0f;
	////directionalLight.Specular.x = 0.2f;
	////directionalLight.Ambient.w = 16.0f;
	//directionalLight.Diffuse.w = 16.0f;
	//directionalLight.Specular.w = 16.0f;

	//lightTest += (deltaTime * .1f);

	//directionalLight.Direction = GUtils::GMath::Point3D<float>(1.0f, lightTest, 0.0f);

	//m_pGraphicsEngine->ApplyDirectionalLight(directionalLight, 1);
	//cb2.worldViewProjection = XMMatrixTranslation(2, 4, 0);
	//m_pGraphicsEngine->TransformModel(2, cb2);
	static float textRotate = 0.0f;
	textRotate += deltaTime * 1.5;
	//m_pGraphicsEngine->RemoveTextureTransformation(3);
	if (textRotate < 8)
	{
		XMMATRIX rotateZ = XMMatrixRotationZ(textRotate);
		m_pGraphicsEngine->TransformTexture(3, ConvertXMMATRIX<float>(rotateZ));
		m_pGraphicsEngine->TransformTexture(4, ConvertXMMATRIX<float>(rotateZ));
	}

	static float y = .0f;
	float test = 0.0f;
	float test2 = -30.0f;
	y -= deltaTime * 1;
	int set = 1000;
	if (y < -20)
		set = 10;

	GUtils::Graphics::Camera::Camera camera;
	m_pGraphicsEngine->GetCamera(camera);

	Vector3D position;
	camera.GetPosition(position);

	Vector3D focus;
	camera.GetFocus(focus);

	static InputState lastState;

	if (m_InputPoll.IsLeftMouseButtonDown())
	{
		int lastCursorPosX = lastState.m_CursorPosition[0];
		int lastCursorPosY = lastState.m_CursorPosition[1];
		int currentCursorPosX = m_InputPoll.GetInputState().m_CursorPosition[0];
		int currentCursorPosY = m_InputPoll.GetInputState().m_CursorPosition[1];

		if (currentCursorPosX - lastCursorPosX != 0)
		{
			focus.xf += deltaTime*(currentCursorPosX - lastCursorPosX) * 2;
		}

		if (currentCursorPosY - lastCursorPosY != 0)
		{
			focus.yf += deltaTime*(lastCursorPosY - currentCursorPosY) * 2;
		}

	}
	else
	{

	}

	if (m_InputPoll.IsAKeyDown())
	{

		position.xf -= deltaTime * 3;
		focus.xf -= deltaTime * 3;
	}

	if (m_InputPoll.IsDKeyDown())
	{
		position.xf += deltaTime * 3;
		focus.xf += deltaTime * 3;
	}

	if (m_InputPoll.IsWKeyDown())
	{
		Vector3D forwardDirection = focus - position;
		forwardDirection.NormalizeThis();

		position.zf += forwardDirection.zf*(deltaTime * 3);
		focus.zf += forwardDirection.zf*(deltaTime * 3);

		position.xf += forwardDirection.xf*(deltaTime * 3);
		focus.xf += forwardDirection.xf*(deltaTime * 3);

		position.yf += forwardDirection.yf*(deltaTime * 3);
		focus.yf += forwardDirection.yf*(deltaTime * 3);

	}

	if (m_InputPoll.IsSKeyDown())
	{
		position.zf -= deltaTime * 3;
		focus.zf -= deltaTime * 3;
	}

	//m_pGraphicsEngine->SetScreenDebug(false);
	//if (!m_InputPoll.IsYKeyDown())
	//	m_pGraphicsEngine->SetRasterizerState("RASTERIZER_SOLID_CULL_BACK");
	if (m_pKeyboardController->GetInputPoll().IsYKeyDown())
	{
		std::shared_ptr<DebugEngineEvent> pDebugEngineEvent(new DebugEngineEvent(0, nullptr));
		pDebugEngineEvent->SetDebug();
		m_pEventManager->QueueEvent(pDebugEngineEvent);
		//m_pGraphicsEngine->SetScreenDebug();
		//m_pGraphicsEngine->SetRasterizerState("RASTERIZER_WIREFRAME_CULL_BACK");
	}
	else
	{
		std::shared_ptr<DebugEngineEvent> pDebugEngineEvent(new DebugEngineEvent(0, nullptr));
		pDebugEngineEvent->SetDebug(false);
		m_pEventManager->QueueEvent(pDebugEngineEvent);
	}

	lastState = m_InputPoll.GetInputState();

	camera.SetPosition(position);
	camera.SetFocus(focus);
	
	m_pGraphicsEngine->GetCamera(camera);

	if (m_pKeyboardController->GetInputPoll().IsWKeyDown())
	{
		std::shared_ptr<MoveCameraEvent> pMoveCameraEvent(new MoveCameraEvent(0, m_pGraphicsEngine, deltaTime));
		pMoveCameraEvent->SetDirection(MoveCameraEvent::Forward);
		m_pEventManager->QueueEvent(pMoveCameraEvent);
	}

	if (m_pKeyboardController->GetInputPoll().IsAKeyDown())
	{
		std::shared_ptr<MoveCameraEvent> pMoveCameraEvent(new MoveCameraEvent(0, m_pGraphicsEngine, deltaTime));
		pMoveCameraEvent->SetDirection(MoveCameraEvent::Left);
		m_pEventManager->QueueEvent(pMoveCameraEvent);
	}

	if (m_pKeyboardController->GetInputPoll().IsDKeyDown())
	{
		std::shared_ptr<MoveCameraEvent> pMoveCameraEvent(new MoveCameraEvent(0, m_pGraphicsEngine, deltaTime));
		pMoveCameraEvent->SetDirection(MoveCameraEvent::Right);
		m_pEventManager->QueueEvent(pMoveCameraEvent);
	}

	if (m_pKeyboardController->GetInputPoll().IsSKeyDown())
	{
		std::shared_ptr<MoveCameraEvent> pMoveCameraEvent(new MoveCameraEvent(0, m_pGraphicsEngine, deltaTime));
		pMoveCameraEvent->SetDirection(MoveCameraEvent::Backward);
		m_pEventManager->QueueEvent(pMoveCameraEvent);
	}
	//m_pGraphicsEngine->ApplyCamera(camera);

	//std::shared_ptr<MoveCameraEvent> pMoveCameraEvent(new MoveCameraEvent(0, camera));
	//m_pEventManager->AbortEvent(12345, true);
	//m_pEventManager->QueueEvent(pMoveCameraEvent);

	Material cubeMaterial;
	ZeroMemory(&cubeMaterial, sizeof Material);
	cubeMaterial.Diffuse.x = 1.0f;
	cubeMaterial.Diffuse.y = 1.0f;
	cubeMaterial.Diffuse.z = 1.0f;
	cubeMaterial.Specular.x = 1.0f;
	cubeMaterial.Specular.w = 16.0f;
	m_pGraphicsEngine->RegisterMaterialToModel(1, cubeMaterial);
	cubeMaterial.Ambient.x = cubeMaterial.Ambient.y = cubeMaterial.Ambient.z = 0.1f;
	m_pGraphicsEngine->RegisterMaterialToModel(3, cubeMaterial);
	m_pGraphicsEngine->RegisterMaterialToModel(4, cubeMaterial);
	m_pGraphicsEngine->RegisterMaterialToModel(5, cubeMaterial);

	set = 50;
	for (int j = 0; j < 20; j++)
	{
		test = 0;
		for (int i = 0; i < set; i++)
		{
			//XMMATRIX scale = XMMatrixScaling(.025f, .025f, .025f);
			XMMATRIX scale = XMMatrixScaling(1.0f, 1.0f, 1.0f);
			XMMATRIX translate = XMMatrixTranslation((j*3.5) - (10 * 3.5), y + (j * 2), test);
			ConstantBuffer cb1;
			cb1.worldViewProjection = scale*translate;
			m_pGraphicsEngine->TransformModel(3, cb1);
			test += 4.0f;
		}

		//for (int i = 0; i < 20; i++)
		//{
		//	XMMATRIX scale = XMMatrixScaling(.025f, .025f, .025f);
		//	XMMATRIX translate = XMMatrixTranslation(test2, y, j + 4);
		//	ConstantBuffer cb1;
		//	cb1.worldViewProjection = scale*translate;
		//	m_pGraphicsEngine->TransformModel(1, cb1);
		//	test2 += 3.0f;
		//}
	}

	//m_pGraphicsEngine->ApplyBlendStateToModel(5, "BLEND_DEFAULT");OnKeyUp
	m_pGraphicsEngine->ApplyBlendStateToModel(4, "BLEND_DEFAULT");
	//m_pGraphicsEngine->ApplyBlendStateToModel(3, "BLEND_DEFAULT");

	m_pEventManager->Update();
	return TRUE;
}

LRESULT CALLBACK TestWindow::MessageHandler(HWND hwnd, UINT umsg, WPARAM wparam, LPARAM lparam)
{
	switch (umsg)
	{
	case WM_KEYDOWN:
	{
		char key[2];
		key[0] = MapVirtualKey(wparam, MAPVK_VK_TO_CHAR);
		key[1] = 0;
		m_pKeyboardController->OnKeyDown(key[0]);
		break;
	}
	case WM_TOUCH:
	{
		m_pKeyboardController->OnKeyDown('W');
		break;
	}
	case WM_KEYUP:
	{
		char key[2];
		key[0] = MapVirtualKey(wparam, MAPVK_VK_TO_CHAR);
		key[1] = 0;
		m_pKeyboardController->OnKeyUp(key[0]);
		break;
	}
	case WM_LBUTTONDOWN:
	{
		m_InputPoll.SetMouseButtonDown(PollInput::MouseButton::Left);
		int a = 0;
		break;
	}
	case WM_RBUTTONDOWN:
	{
		m_InputPoll.SetMouseButtonDown(PollInput::MouseButton::Right);
		int a = 0;
		break;
	}
	case WM_LBUTTONUP:
	{
		m_InputPoll.UnsetMouseButtonDown(PollInput::MouseButton::Left);
		int a = 0;
		break;
	}
	case WM_RBUTTONUP:
	{
		m_InputPoll.UnsetMouseButtonDown(PollInput::MouseButton::Right);
		int a = 0;
		break;
	}
	case WM_MBUTTONDOWN:
	{
		m_InputPoll.SetMouseButtonDown(PollInput::MouseButton::Middle);
		int a = 0;
		break;
	}
	case WM_MBUTTONUP:
	{
		m_InputPoll.UnsetMouseButtonDown(PollInput::MouseButton::Middle);
		int a = 0;
		break;
	}

	case WM_MOUSEMOVE:
	{
		m_InputPoll.SetCursorPosition(LOWORD(lparam), HIWORD(lparam));
		break;
	}
	default:
	{

	}
	}
	return DefWindowProc(hwnd, umsg, wparam, lparam);
}

BOOL TestWindow::InitializeSystemWindow(uint32 screenWidth, uint32 screenHeight)
{
	return GameWindow::InitializeSystemWindow(screenWidth, screenHeight);
}

