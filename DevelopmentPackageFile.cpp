#pragma once
#include "stdafx.h"

#ifdef _WIN32
#include "Shlwapi.h"
#endif

#include<list>

#include "DevelopmentPackageFile.h"
#include "FileLoader.h"

using namespace GUtils::IO::Resources::Debug;
using namespace GUtils::IO;

DevelopmentPackageFile::DevelopmentPackageFile(const std::string& filePath)
{
	m_RootDirectory = filePath;
}

DevelopmentPackageFile::~DevelopmentPackageFile()
{

}

BOOL DevelopmentPackageFile::Open()
{
	std::list<std::string> files;

#ifdef _WIN32

	if (!PathFileExists(m_RootDirectory.c_str()))
		return FALSE;

	// right now this only assumes every file is on same directory and ignores directories. I need to implement a solution with 
	// multi-directory structure later

	HANDLE hFind = INVALID_HANDLE_VALUE;
	WIN32_FIND_DATA FFD;
	DWORD dwError = 0;
	std::string directorySearch = m_RootDirectory + "*";
	hFind = FindFirstFile(directorySearch.c_str(), &FFD);

	if (hFind == INVALID_HANDLE_VALUE)
		return 0;

	do
	{
		if (FFD.cFileName[0] == '.' && strlen(FFD.cFileName) < 3)
			continue;

		if (FFD.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			continue;
		}
		else
		{
			files.push_back(FFD.cFileName);
		}

	} while (FindNextFile(hFind, &FFD) != FALSE);

	FindClose(hFind);

#else
	return FALSE; // Each platform will require particular io solution. Move this logic to file/directory classes I think
#endif

	uint32 i = 0;
	for (std::string fileName : files)
		m_ResourceRelativePathMap[i++] = fileName;

	return TRUE;
}

uint32 DevelopmentPackageFile::GetResourceSize(const PackageResource& resource)
{
	std::string fullPathToResource = m_RootDirectory + resource.GetName();
	
	FileLoader fileLoader;
	if (!fileLoader.Open(fullPathToResource))
		return 0;
	
	uint32 sizeInBytes = 0;
	fileLoader.GetFileSize(sizeInBytes);

	return sizeInBytes;
}

uint32 DevelopmentPackageFile::GetResource(const PackageResource& resource, sbyte8* pBuffer)
{
	std::string fullPathToResource = m_RootDirectory + resource.GetName();

	FileLoader fileLoader;
	if (!fileLoader.Open(fullPathToResource))
		return 0;

	std::vector<sbyte8> dataBuffer;
	if (!fileLoader.GetRawData(dataBuffer))
		return FALSE;

	uint32 sizeInBytes = 0;
	if (!fileLoader.GetFileSize(sizeInBytes))
		return 0;

	memcpy(pBuffer, &dataBuffer[0], sizeInBytes);

	return sizeInBytes;
}

uint32 DevelopmentPackageFile::GetNumberOfResources() const
{
	return m_ResourceRelativePathMap.size();
}
std::string DevelopmentPackageFile::GetResourceName(const uint32 number) const
{
	auto mapIter = m_ResourceRelativePathMap.find(number);
	if (mapIter != m_ResourceRelativePathMap.end())
		return mapIter->second;

	return std::string("");
}