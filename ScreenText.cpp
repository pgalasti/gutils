#pragma once
#include "stdafx.h"

#include <DirectXMath.h>
#include "ScreenText.h"
#include "TestMesh.h"
#include "DirectXTKInc\SpriteFont.h"

using namespace DirectX;
using namespace GraphicsEngine::Font::DirectX;

struct ScreenText::Impl
{
	Impl(ID3D11Device* pDevice, ID3D11DeviceContext* pDeviceContext)
	{
		m_pDevice = pDevice;
		m_pDeviceContext = pDeviceContext;
		m_pSpriteBatch = std::unique_ptr<SpriteBatch>(new SpriteBatch(m_pDeviceContext));

		m_pSpriteFont = nullptr;
	}
	~Impl()
	{
	}

	std::wstring m_SpriteFontPath;

	ID3D11Device* m_pDevice;
	ID3D11DeviceContext* m_pDeviceContext;
	std::unique_ptr<SpriteBatch> m_pSpriteBatch;
	std::unique_ptr<SpriteFont> m_pSpriteFont;
};

ScreenText::ScreenText(ID3D11Device* pDevice, ID3D11DeviceContext* pDeviceContext)
	: pImpl(new Impl(pDevice, pDeviceContext))
{
}

ScreenText::~ScreenText()
{
	SAFE_DELETE(this->pImpl);
}

BOOL ScreenText::Load(const std::wstring& spriteFontPath)
{
	this->pImpl->m_pSpriteFont = std::unique_ptr<SpriteFont>(new SpriteFont(this->pImpl->m_pDevice, spriteFontPath.c_str()));
	if (this->pImpl->m_pSpriteFont == nullptr)
		return FALSE;

	return TRUE;
}

BOOL ScreenText::WriteText(const std::wstring& text, const Point2D<uint32>& position, const ColorRGBA<float>& textColor)
{
	this->pImpl->m_pSpriteBatch->Begin(SpriteSortMode_Immediate);
	
	// Position
	XMFLOAT2 xmFloatPosition;
	xmFloatPosition.x = position.x;
	xmFloatPosition.y = position.y;
	FXMVECTOR xmVectorPosition = XMLoadFloat2(&xmFloatPosition);
	
	// Color
	XMFLOAT4 xmFloatColor;
	xmFloatColor.x = textColor.x;
	xmFloatColor.y = textColor.y;
	xmFloatColor.z = textColor.z;
	xmFloatColor.w = textColor.w;
	FXMVECTOR xmVectorColor = XMLoadFloat4(&xmFloatColor);
	
	this->pImpl->m_pSpriteFont->DrawString(this->pImpl->m_pSpriteBatch.get(), text.c_str(), xmVectorPosition, xmVectorColor, 0.0f);
	
	this->pImpl->m_pSpriteBatch->End();

	return TRUE;
}

#pragma region deprecated
#ifdef DEPRECATED_SCREEN_TEXT

ScreenText::ScreenText(const CoordinateMap& coordinateTextureMapping)
{
	m_pVertexBuffer = nullptr;
	m_pIndexBuffer = nullptr;
	m_CoordinateMap = coordinateTextureMapping;
}

ScreenText::~ScreenText()
{
	COM_RELEASE(m_pVertexBuffer);
	COM_RELEASE(m_pIndexBuffer);
}

BOOL ScreenText::CreateScreenText(ID3D11Device* pDevice, const char* pszText)
{
	COM_RELEASE(m_pVertexBuffer);
	COM_RELEASE(m_pIndexBuffer);

	D3D11_BUFFER_DESC bufferDesc;
	D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&bufferDesc, sizeof(D3D11_BUFFER_DESC));
	ZeroMemory(&InitData, sizeof(D3D11_SUBRESOURCE_DATA));

	if (!LoadVerticies(pszText))
		return FALSE;

	// Vertex Buffer
	bufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	bufferDesc.ByteWidth = sizeof(PosTexVertex) * m_Verticies.size();
	bufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bufferDesc.CPUAccessFlags = 0;
	InitData.pSysMem = &m_Verticies[0];
	if (FAILED(pDevice->CreateBuffer(&bufferDesc, &InitData, &m_pVertexBuffer)))
		return FALSE;

	// Index Buffer
	bufferDesc.Usage = D3D11_USAGE_IMMUTABLE;
	bufferDesc.ByteWidth = sizeof(WORD) * m_Indicies.size();
	bufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bufferDesc.CPUAccessFlags = 0;
	InitData.pSysMem = &m_Indicies[0];
	if (FAILED(pDevice->CreateBuffer(&bufferDesc, &InitData, &m_pIndexBuffer)))
		return FALSE;

	return TRUE;
}

BOOL ScreenText::Render(ID3D11DeviceContext* pDeviceContext)
{

	UINT stride = sizeof(PosTexVertex);
	UINT offset = 0;
	pDeviceContext->IASetVertexBuffers(0, 1, &m_pVertexBuffer, &stride, &offset);
	pDeviceContext->IASetIndexBuffer(m_pIndexBuffer, DXGI_FORMAT_R16_UINT, 0);
	pDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	pDeviceContext->DrawIndexed(m_Indicies.size(), 0, 0);

	return TRUE;
}

BOOL ScreenText::LoadVerticies(const char* pszText)
{
	const ushort16 textSize = strlen(pszText);

	float drawX = 0.0f, drawY = 1.0f;
	AsciiValue asciiValue;
	FontCoordinateStructure coordStruct;
	PosTexVertex vertex;
	uint32 index = 0;
	for (ushort16 i = 0; i < textSize; i++)
	{
		asciiValue = pszText[i];

		// If it's a space, just separate the characters as a space
		if (asciiValue == (char)' ')
		{
			drawX += 3.0f;
			continue;
		}

		ZeroMemory(&coordStruct, sizeof FontCoordinateStructure);
		coordStruct = m_CoordinateMap[asciiValue];
		ZeroMemory(&vertex, sizeof PosTexVertex);

		// First triangle

		// Top left
		vertex.Pos.x = drawX;
		vertex.Pos.y = drawY;
		vertex.Pos.z = 0.0f;
		vertex.Tex.x = coordStruct.LeftU;
		vertex.Tex.y = 0.0f;
		m_Verticies.push_back(vertex);
		m_Indicies.push_back(index++);

		// Bottom right
		vertex.Pos.x = drawX + coordStruct.Width;
		vertex.Pos.y = drawY-1;
		vertex.Pos.z = 0.0f;
		vertex.Tex.x = coordStruct.RightU;
		vertex.Tex.y = 1.0f;
		m_Verticies.push_back(vertex);
		m_Indicies.push_back(index++);

		// Bottom Left
		vertex.Pos.x = drawX;
		vertex.Pos.y = drawY - 1;
		vertex.Pos.z = 0.0f;
		vertex.Tex.x = coordStruct.LeftU;
		vertex.Tex.y = 1.0f;
		m_Verticies.push_back(vertex);
		m_Indicies.push_back(index++);

		// Second triangle
		// Top left
		vertex.Pos.x = drawX;
		vertex.Pos.y = drawY;
		vertex.Pos.z = 0.0f;
		vertex.Tex.x = coordStruct.LeftU;
		vertex.Tex.y = 0.0f;
		m_Verticies.push_back(vertex);
		m_Indicies.push_back(index++);

		// Top Right
		vertex.Pos.x = drawX + coordStruct.Width;
		vertex.Pos.y = drawY;
		vertex.Pos.z = 0.0f;
		vertex.Tex.x = coordStruct.RightU;
		vertex.Tex.y = 0.0f;
		m_Verticies.push_back(vertex);
		m_Indicies.push_back(index++);

		// Bottom right
		vertex.Pos.x = drawX + coordStruct.Width;
		vertex.Pos.y = drawY-1;
		vertex.Pos.z = 0.0f;
		vertex.Tex.x = coordStruct.RightU;
		vertex.Tex.y = 1.0f;
		m_Verticies.push_back(vertex);
		m_Indicies.push_back(index++);

		drawX += coordStruct.Width + 1.0f;
	}

	return TRUE;
}

#endif
#pragma endregion deprecated