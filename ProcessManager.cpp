#pragma once
#include "stdafx.h"

#include "ProcessManager.h"

using namespace GameEngine::Logic::Process;

ProcessManager::~ProcessManager()
{
	ClearAllProcesses();
}

uint32 ProcessManager::UpdateProcesses(const uint32 deltaTime)
{
	ushort16 successCount = 0;
	ushort16 failCount = 0;

	ProcessList::iterator processIter = m_ProcessList.begin();

	while (processIter != m_ProcessList.end())
	{
		StrongProcessPtr pProcess = *processIter;
		
		ProcessList::iterator thisIter = processIter;
		++processIter;

		if (pProcess->GetState() == Process::Uninitialized)
			pProcess->OnInit();

		if (pProcess->GetState() == Process::Running)
			pProcess->OnUpdate(deltaTime);

		if (pProcess->IsDead())
		{
			switch (pProcess->GetState())
			{
			case Process::Succeeded:
			{
				pProcess->OnSuccess();
				StrongProcessPtr pChildProcess = pProcess->RemoveChild();
				if (pChildProcess)
					this->AttachProcess(pChildProcess);
				else
					++successCount;

				break;
			}
			case Process::Failed:
			{
				pProcess->OnFail();
				++failCount;

				break;
			}
			case Process::Aborted:
			{
				pProcess->OnAbort();
				++failCount;

				break;
			}
			}
			m_ProcessList.erase(thisIter);
		}
	}

	return ((successCount << 16 | failCount));
}

WeakProcessPtr ProcessManager::AttachProcess(StrongProcessPtr pProcess)
{
	m_ProcessList.push_front(pProcess);
	return WeakProcessPtr(pProcess);
}

void ProcessManager::AbortAllProcesses(const bool immediate)
{
	ProcessList::iterator processIter = m_ProcessList.begin();
	while (processIter != m_ProcessList.end())
	{
		ProcessList::iterator tempIter = processIter;
		++processIter;

		StrongProcessPtr pProcess = *tempIter;
		if (pProcess->IsAlive())
		{
			pProcess->SetState(Process::Aborted);
			if (immediate)
			{
				pProcess->OnAbort();
				m_ProcessList.erase(tempIter);
			}
		}
	}
}