#include "StructMatLight.fx"
#include "LightStruct.fx"
#include "LightFunctions.fx"
//--------------------------------------------------------------------------------------
// Pixel Shader
//--------------------------------------------------------------------------------------
float4 PS(PSInput input) : SV_Target
{
	input.NormalW = normalize(input.NormalW);

	float3 toEyeW;
	toEyeW = normalize(EyePosition - input.PosW);

	float4 ambient;
	float4 diffuse;
	float4 specular;
	ambient = float4(0.0f, 0.0f, 0.0f, 0.0f);
	diffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
	specular = float4(0.0f, 0.0f, 0.0f, 0.0f);

	float4 A, D, S;
	
	[loop]
	for (int i = 0; i < numDirectionalLights; i++)
	{
		ComputeDirectionalLight(material, directionalLight[i], input.NormalW, toEyeW, A, D, S);
		ambient += A;
		diffuse += D;
		specular += S;
	}

	//for (int i = 0; i < 1; i++)
	[loop]
	for (int j = 0; j < numPointLights; j++)
	{
		ComputerPointLight(material, pointLight[j], input.PosW, input.NormalW, toEyeW, A, D, S);
		ambient += A;
		diffuse += D;
		specular += S;
	}
	[loop]
	for (int k = 0; k < numSpotLights; k++)
	{
		ComputeSpotLight(material, spotLight[k], input.PosW, input.NormalW, toEyeW, A, S, D);
		ambient += A;
		diffuse += D;
		specular += S;
	}

	float4 litColor;
	litColor = ambient + diffuse + specular;
	litColor.a = material.Diffuse.a;

	return litColor;
}