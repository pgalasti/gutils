#pragma once
#include "stdafx.h"

#include "IDGenerator.h"

using namespace GUtils::Concurrency;

int IDGenerator::GetNextId()
{
	std::lock_guard<std::mutex> lock(m_Mutex);

	// If we ran out of IDs to use, get a new set of 100.
	if (m_IdQueue.empty())
	{
		for (uint32 i = m_LastId + 1; i < m_LastId + 100; i++)
			m_IdQueue.push(i);
	}

	// Get the next ID from the queue and pop it.
	m_LastId = m_IdQueue.front();
	m_IdQueue.pop();

	return m_LastId;
}

int IDGenerator::GetLatestId()
{
	std::lock_guard<std::mutex> lock(m_Mutex);
	return m_LastId;
}

void IDGenerator::Reset(const uint32 firstValue)
{
	std::lock_guard<std::mutex> lock(m_Mutex);

	// Clear out any remaining values.
	while (!m_IdQueue.empty())
		m_IdQueue.pop();

	// Give the 100 IDs to start with
	for (uint32 i = firstValue; i < firstValue + 100; i++)
		m_IdQueue.push(i);

	m_FirstValue = m_LastId = firstValue;
}

int IDGenerator::GetFirstValue() 
{ 
	std::lock_guard<std::mutex> lock(m_Mutex);
	return m_FirstValue; 
}

void IDGenerator::Release(const uint32 id)
{
	std::lock_guard<std::mutex> lock(m_Mutex);
	if (id > m_LastId)
		return; // Can't release something we never had assigned.

	m_IdQueue.push(id);
}