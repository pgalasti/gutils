#pragma once
#include "stdafx.h"
#include "ResourceDirectories.h"

#include <ShlObj.h>
#include <string>

using namespace GUtils::Graphics;

std::string ResourceDirectories::GetBinaryPathStdString()
{
	if (!m_BinaryPathStr.empty())
		return std::string(m_BinaryPathStr);

	// Get the full executable path
	char szBinaryExecutable[MAX_PATH];
	ZeroMemory(szBinaryExecutable, MAX_PATH);
	GetModuleFileName(NULL, szBinaryExecutable, MAX_PATH);

	char szDrive[_MAX_DRIVE];
	char szPath[_MAX_DIR];
	ZeroMemory(szDrive, _MAX_DRIVE);
	ZeroMemory(szPath, _MAX_DIR);
	_splitpath(szBinaryExecutable, szDrive, szPath, nullptr, nullptr);

	// Grab just the directory path
	std::string binaryPath;
	binaryPath.append(szDrive)
		.append(szPath);

	m_BinaryPathStr = binaryPath;
	return std::string(m_BinaryPathStr);
}

std::wstring ResourceDirectories::GetBinaryPath()
{
	if (!m_BinaryPathStrWide.empty())
		return m_BinaryPathStrWide;

	// Grab just the directory path
	std::string binaryPath = ResourceDirectories::GetBinaryPathStdString();

	// Convert to wide character
	wchar_t szwPath[_MAX_PATH]; ZeroMemory(szwPath, _MAX_PATH);
	MultiByteToWideChar(0, 0, binaryPath.c_str(), binaryPath.length(), szwPath, binaryPath.length());

	m_BinaryPathStrWide = std::wstring(szwPath);
	return std::wstring(m_BinaryPathStrWide);
}

std::string ResourceDirectories::GetLoggingPath()
{
	if (!m_LoggingPathStr.empty())
		return std::string(m_LoggingPathStr);

	// Get the system app data path
	char szPath[MAX_PATH];
	SHGetFolderPath(NULL, CSIDL_APPDATA, NULL, 0, szPath);

	// Add path
	strcat(szPath, "\\GEngine\\");

	CreateDirectory(szPath, NULL);

	 //Convert to wide character
	wchar_t szwPath[_MAX_PATH]; ZeroMemory(szwPath, _MAX_PATH);
	MultiByteToWideChar(0, 0, szPath, strlen(szPath), szwPath, strlen(szPath));
	
	m_LoggingPathStr = std::string(szPath);
	return std::string(m_LoggingPathStr);
}

std::string ResourceDirectories::GetModelDirectoryPath()
{
	if (!m_ModelPathStr.empty())
		return std::string(m_ModelPathStr);

	std::string modelDirectory = ResourceDirectories::GetBinaryPathStdString();
	modelDirectory += "Models\\";

	m_ModelPathStr = modelDirectory;

	return std::string(m_ModelPathStr);
}

std::string ResourceDirectories::GetTextureDirectoryPath()
{
	if (!m_TexturePathStr.empty())
		return std::string(m_TexturePathStr);

	std::string textureDirectory = ResourceDirectories::GetBinaryPathStdString();
	textureDirectory += "Textures\\";

	m_TexturePathStr = textureDirectory;

	return std::string(m_TexturePathStr);
}

std::string ResourceDirectories::GetFontPath()
{
	if (!m_FontPathStr.empty())
		return m_FontPathStr;

	std::string fontDirectory = ResourceDirectories::GetBinaryPathStdString();
	fontDirectory += "Font\\";

	m_FontPathStr = fontDirectory;
	return std::string(m_FontPathStr);
}