#pragma once
#include"stdafx.h"
#include"FileLoader.h"

using GUtils::IO::FileLoader;

FileLoader::FileLoader()
:m_bFileLoaded(false), 
m_pFile(nullptr)
{

}

FileLoader::~FileLoader()
{
	this->Close();
}

FileLoader::FileLoader(const char* pszFilePath) : FileLoader(std::string(pszFilePath))
{
	
}

FileLoader::FileLoader(const std::string& strFilePath) : FileLoader()
{
	this->m_FilePath = strFilePath;
}

BOOL FileLoader::Open()
{
	if (this->m_FilePath.size() < 1)
		return FALSE;

	this->m_pFile = fopen(this->m_FilePath.c_str(), "r+");

	if (!this->m_pFile)
		return FALSE;

	m_bFileLoaded = true;
	return TRUE;
}

BOOL FileLoader::Open(const char* pszFilePath)
{
	return this->Open(std::string(pszFilePath));
}

BOOL FileLoader::Open(const std::string& strFilePath)
{
	this->m_FilePath = strFilePath;
	return this->Open();
}

BOOL FileLoader::Close()
{
	if (this->m_pFile)
		fclose(this->m_pFile);
	
	m_pFile = nullptr;

	m_bFileLoaded = false;
	return TRUE;
}

BOOL FileLoader::GetFileLines(std::list<std::string>& fileLines)
{
	if (!m_bFileLoaded)
		return FALSE;

	if (!m_pFile)
		return FALSE;

	fileLines.clear();

	const ushort16 MAX_LINE_SIZE = 2048;
	char szLine[MAX_LINE_SIZE];
	while (fgets(szLine, MAX_LINE_SIZE, m_pFile))
		fileLines.push_back(std::string(szLine));

	return TRUE;
}

BOOL FileLoader::GetRawData(std::vector<sbyte8>& buffer)
{
	if (!m_bFileLoaded)
		return FALSE;

	if (!m_pFile)
		return FALSE;


	fseek(m_pFile, 0, SEEK_END);
	uint32 fileLength = ftell(m_pFile);
	fseek(m_pFile, 0, SEEK_SET);

	buffer.clear();
	buffer.reserve(fileLength);
	buffer.resize(fileLength);
	fread(&buffer[0], fileLength, 1, m_pFile);

	return TRUE;
}

BOOL FileLoader::GetFileSize(uint32& sizeInBytes)
{
	if(!m_bFileLoaded)
		return FALSE;

	if (!m_pFile)
		return FALSE;

	fseek(m_pFile, 0, SEEK_END);
	sizeInBytes = ftell(m_pFile);

	return TRUE;;
}