#pragma once
#include "stdafx.h"

#include "XmlParser.h"

#include "FileLoader.h"
#include "UtilFunctions.h"
#include "Parser.h"

using namespace GUtils::Utils::XML::PickyParser;
using namespace GUtils::Utils::XML;
using namespace GUtils::Utils::Functions;

XmlParser::XmlParser()
{

}

XmlParser::~XmlParser()
{

}

BOOL XmlParser::Parse(const char* pszFilePath, XmlDocument& xmlDocument)
{
	// Open the file
	GUtils::IO::FileLoader fileLoader;
	if (!fileLoader.Open(pszFilePath))
	{
		m_ErrorMessage = "Unable to open file path: " + std::string(pszFilePath);
		return FALSE;
	}

	// Read the file lines
	std::list<std::string> fileLines;
	if (!fileLoader.GetFileLines(fileLines))
	{
		m_ErrorMessage = "Unable to read file lines for file path: " + std::string(pszFilePath);
		return FALSE;
	}

	fileLoader.Close();

	// Extract the root element
	RootElementPtr pRootElement = std::make_shared<RootElement>();
	ExtractRoot(pRootElement, *fileLines.begin());


	std::string fileLinesConcat;

	bool bSkip = true;
	for (std::string line : fileLines)
	{
		if (bSkip)
		{
			bSkip = false;
			continue;
		}

		fileLinesConcat += line;
	}

	ElementPtr pTopElement = nullptr;
	int position = 0;
	CleanString(fileLinesConcat, "\t\n\r");
	std::string ending;
	if (!BuildElements(pTopElement, fileLinesConcat, position, ending))
		return FALSE;

	xmlDocument.m_pRootElement = pRootElement;
	xmlDocument.m_pTopElement = pTopElement;

	return TRUE;
}


BOOL XmlParser::ExtractRoot(RootElementPtr pRootElement, const std::string& fileLine)
{
	std::string topLine = fileLine;
	CleanString(topLine, "\r\n");
	RemoveTrailingAndLeadingSpaces(topLine);
	const size_t lineSize = topLine.size();

	// Make sure it's the right size
	if (lineSize < 7)
	{
		m_ErrorMessage = "Root XML header element is too small: " + fileLine;
		return FALSE;
	}

	// Check if it's formatted correctly
	if (topLine[0] != '<' || topLine[1] != '?' || topLine[lineSize - 2] != '?' || topLine[lineSize - 1] != '>')
	{
		m_ErrorMessage = "Invalid root XML header element: " + fileLine;
		return FALSE;
	}

	// Trim the <? and ?>
	std::string remainingLine = topLine.substr(2, lineSize - 4);

	GUtils::Util::StringParser lineParser;

	lineParser.Parse(remainingLine, " ");
	lineParser.getFirst();

	std::string xmlCheck = lineParser.getToken();
	ToUpper(xmlCheck);
	if (xmlCheck != "XML")
	{
		m_ErrorMessage = "Element name is not 'XML' " + fileLine;
		return FALSE;
	}

	GUtils::Util::StringParser attributeParser;
	while (lineParser.getNext())
	{
		Attribute attribute;

		const std::string attributeToken = lineParser.getToken();
		attributeParser.Parse(attributeToken, "=");
		if (!attributeParser.getFirst())
		{
			m_ErrorMessage = "Unable to read attribute in root element " + fileLine;
			return FALSE;
		}
		attribute.Key = attributeParser.getToken();

		if (!attributeParser.getNext())
		{
			m_ErrorMessage = "Unable to read attribute in root element " + fileLine;
			return FALSE;
		}
		attribute.Value = attributeParser.getToken();
		CleanString(attribute.Value, "\"'");
		pRootElement->AddAttribute(attribute);
	}

	return TRUE;
}

BOOL XmlParser::BuildElements(ElementPtr& pParentElement, const std::string& remainingLines, int& traversedCharacters, std::string& lastEnding)
{

	//Element* pNewElement = new Element();
	//Element* pParentElement = *ppParentElement;
	//if (*ppParentElement == nullptr)
	//	pParentElement = *ppParentElement = pNewElement;

	ElementPtr pNewElement = std::make_shared<Element>();
	if (pParentElement == nullptr)
		pParentElement = pNewElement;

	// Get the remaining segment to parse through
	std::string parseSegment = remainingLines;

	std::string elementSegment;
	size_t segmentLength = 0;
	for (size_t i = 0; i < parseSegment.size(); i++)
	{
		++traversedCharacters;
		++segmentLength;
		const char currentCharacter = parseSegment[i];
		elementSegment += currentCharacter;

		// Break out when we find the end
		if (currentCharacter == '>')
			break;
	}

	std::string cleanedElement = elementSegment;
	CleanString(cleanedElement, "<>\t\r\n");

	if (cleanedElement[0] == '/' /*|| cleanedElement[cleanedElement.size()-1] == '/'*/)
	{
		lastEnding = cleanedElement;
		return TRUE;
	}

	GUtils::Util::StringParser lineParser;
	lineParser.Parse(cleanedElement, " ");
	if (!lineParser.getFirst())
	{
		m_ErrorMessage = "Unable to parse xml segment: " + elementSegment;
		return FALSE;
	}

	pNewElement->m_Name = lineParser.getToken();

	bool thisIsAnEndingLine = false;
	GUtils::Util::StringParser attributeParser;
	while (lineParser.getNext())
	{
		const std::string attributeToken = lineParser.getToken();
		if (attributeToken == "/")
		{
			thisIsAnEndingLine = true;
			break;
		}

		attributeParser.Parse(attributeToken, "=");
		if (attributeParser.getSize() != 2)
		{
			m_ErrorMessage = "Unable to parse attribute in element: " + pNewElement->m_Name;
			return FALSE;
		}

		Attribute attribute;
		attributeParser.getFirst();
		attribute.Key = attributeParser.getToken();
		attributeParser.getNext();
		attribute.Value = attributeParser.getToken();
		CleanString(attribute.Value, "\"");
		pNewElement->AddAttribute(attribute);
	}

	if (thisIsAnEndingLine)
	{
		pParentElement->AddChildElement(*pNewElement);
		return TRUE;
	}

	std::string contents;
	std::string afterElementSegment = remainingLines.substr(segmentLength, remainingLines.size());
	std::string cleanedAfterElementSegment = afterElementSegment;
	CleanString(cleanedAfterElementSegment, "\t\r\n");
	for (size_t i = 0; i < cleanedAfterElementSegment.size(); i++)
	{
		if (cleanedAfterElementSegment[i] == '<')
			break;

		++segmentLength;
		++traversedCharacters;
		contents += cleanedAfterElementSegment[i];
	}

	pNewElement->m_Value = contents;
	std::string ending;
	for (;;)
	{
		if (remainingLines.size() < segmentLength)
		{
			m_ErrorMessage = "Unable to find ending tag for element: " + pNewElement->m_Name;
			return FALSE;
		}

		int lastTraversed = 0;
		if (!BuildElements(pNewElement, remainingLines.substr(segmentLength, remainingLines.size()), lastTraversed, ending))
			return FALSE;

		segmentLength += lastTraversed;
		if (!ending.empty())
		{
			if (pNewElement->m_Name == ending.substr(1, ending.size()))
				break;
		}
	}

	traversedCharacters = segmentLength;
	if (pNewElement != pParentElement)
		pParentElement->AddChildElement(*pNewElement);

	return TRUE;
}