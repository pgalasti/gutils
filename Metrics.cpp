#include "StdAfx.h"
#include "Metrics.h"
#include <sstream>
#include <MMSystem.h>

using namespace GUtils::Debug;

PerformanceTimer::PerformanceTimer(TimeUnit unit /*= MilliSecond*/)
{
	dblCalibration = 0.0f;
	start.QuadPart = 0;
	stop.QuadPart = 0;
	dblLastTime = 0.0f;
	SetUnit(unit);

	if (QueryPerformanceFrequency(&frequency))
	{
		bHighPerformance = true;
		SetThreadAffinityMask(GetCurrentThread(), 0); // Debugging purposes. Set only to first CPU for metrics
		Calibrate();
	}
	else
		bHighPerformance = false;
}

PerformanceTimer::~PerformanceTimer()
{
}

void PerformanceTimer::Start(std::string sTag /*= ""*/)
{
	start.QuadPart = 0;
	stop.QuadPart = 0;
	strCurrentTag = sTag;
	if (bHighPerformance)
		QueryPerformanceCounter(&start);
	else
		start.QuadPart = timeGetTime();
}

void PerformanceTimer::Stop()
{
	QueryPerformanceCounter(&stop);

	double modifier = 0;
	if (bHighPerformance)
	{
		QueryPerformanceFrequency(&frequency); // Requery - Some CPUs dynamically change frequency for load

		static ushort16 calibrationCounter = 0;
		if (++calibrationCounter == 1000)
		{
			calibrationCounter = 0;
			std::string tempStr = strCurrentTag;
			LARGE_INTEGER local_start = start, temp_stop = stop;
			Calibrate();
			start = local_start; stop = temp_stop;
			strCurrentTag = tempStr;
		}

		if (unit == Second)
			modifier = 1;
		else if (unit == MilliSecond)
			modifier = 1000;
		else if (unit == MicroSecond)
			modifier = 1000000;
		dblLastTime = (stop.QuadPart - start.QuadPart) * ((double)modifier / frequency.QuadPart);
		dblLastTime -= dblCalibration;
	}
	else
	{
		stop.QuadPart = timeGetTime();

		if (unit == Second)
			modifier = .001;
		else if (unit == MilliSecond)
			modifier = 1;
		else if (unit == MicroSecond)
			modifier = 1000;

		dblLastTime = (stop.QuadPart - start.QuadPart)*modifier;
	}



	std::ostringstream oss;
	oss << dblLastTime;
	std::string strTime = oss.str();

	if (unit == Second)
	{
		strTime += "s";
		dblLastTime *= .001;
	}
	else if (unit == MilliSecond)
	{
		strTime += "ms";
	}
	else if (unit == MicroSecond)
	{
		strTime += "us";
		dblLastTime *= 1000;
	}

	TagTimes.push_back(TagPlotPair_t(strCurrentTag, strTime));

	auto iter = TagSummaryMap.find(strCurrentTag);
	if (iter != TagSummaryMap.end())
	{
		iter->second.first += dblLastTime;
		++iter->second.second;
	}
	else
	{
		TimeFreqPair_t timefreq(dblLastTime, 1);
		TagSummaryMap[strCurrentTag] = timefreq;
	}
}

void PerformanceTimer::ClearPlots()
{
	TagTimes.clear();
}

double PerformanceTimer::LastTime() const
{
	return dblLastTime; // Always MS
}

BOOL PerformanceTimer::DumpToFile(const char* pszPath)
{
	FILE* pFile = fopen(pszPath, "w+");
	if (pFile == NULL)
		return FALSE;

	for (auto iter = TagTimes.begin(); iter != TagTimes.end(); iter++)
		fprintf(pFile, "%s\t%s\n", iter->first.c_str(), iter->second.c_str());

	fprintf(pFile, "\r\n\r\n=== SUMMARY ===\r\n");

	for (auto iter = TagSummaryMap.begin(); iter != TagSummaryMap.end(); iter++)
		fprintf(pFile, "%s\t%fms\r\n\t\t%d plots\t%fms average\r\n\r\n",
		iter->first.c_str(), iter->second.first, iter->second.second, ((double)iter->second.first / iter->second.second));

	fclose(pFile);
	pFile = NULL;

	ClearPlots();

	return TRUE;
}

bool PerformanceTimer::IsHighPerformance() const
{
	return bHighPerformance;
}

void PerformanceTimer::SetUnit(TimeUnit unit)
{
	this->unit = unit;
}

void PerformanceTimer::Calibrate() // Attempts to remove the overhead
{
	Start();
	CalStop();
	dblCalibration = (stop.QuadPart - start.QuadPart) * ((double)1000 / frequency.QuadPart);
	dblCalibration *= .1;
}

void PerformanceTimer::CalStop()
{
	QueryPerformanceCounter(&stop);
}