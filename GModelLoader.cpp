#pragma once
#include "stdafx.h"

#include "GModelLoader.h"

#include "TestMesh.h"
#include "FileLoader.h"
#include "Parser.h"

using namespace GraphicsEngine::Model;
using namespace GUtils::IO;
using namespace GUtils::Util;

/*static */
BOOL GModelLoader::LoadFile(const char* pszPath, IMesh** pMeshData)
{
	FileLoader fileLoader;

	// Open the file
	if (!fileLoader.Open(pszPath))
		return FALSE;

	// Get the lines from the file
	std::list<std::string> fileLines;
	if (!fileLoader.GetFileLines(fileLines))
		return FALSE;

	if (!fileLoader.Close())
		return FALSE;

	// If it doesn't have at least 4 lines, it shouldn't be considered valid
	if (fileLines.size() < 5)
		return FALSE;

	// Get the version number
	auto fileLinesIter = fileLines.begin();
	std::string versionString;
	versionString = fileLinesIter->substr(10);
	if (versionString[0] != 'V' && versionString[0] != 'v')
		return FALSE;
	
	// Parse out the version number
	std::string versionNumber;
	int i = 1;
	do
	{
		versionNumber += versionString[i++];
	} while (isdigit(versionString[i]) || versionString[i] == '.');
	double version = atof(versionNumber.c_str());

	if (version == 1.0f)
	{
		std::vector<std::string> fileLinesVec;
		for (std::string line : fileLines)
			fileLinesVec.push_back(line);

		if (!GModelLoader::LoadVersion1(fileLinesVec, pMeshData))
			return FALSE;
		
		return TRUE;
	}
	else
	{
		return FALSE; // Can't find version implementation.
	}

	return TRUE;
}

/*static */
BOOL GModelLoader::Load(const ubyte8* pData)
{
	StringParser parser;
	std::string dataAsString = (char*)pData;

	//dataAsString

	return TRUE;
}

/*static */
BOOL GModelLoader::LoadVersion1(const std::vector<std::string>& fileLines, IMesh** pMeshData)
{
	StringParser parser;
	// Components to look for
	std::string components = fileLines[1].substr(3);
	parser.Parse(components, "-");
	
	if (!parser.getFirst())
		return FALSE;

	bool hasVertex = false;
	bool hasNormal = false;
	bool hasUV = false;
	bool hasIndex = false;

	do
	{
		std::string token = parser.getToken();

		if (token[0] == 'V')
			hasVertex = true;
		else if (token[0] == 'N')
			hasNormal = true;
		else if (token[0] == 'I')
			hasIndex = true;
		else if (token[0] == 'T')
			hasUV = true;

	} while (parser.getNext());

	// We need these two at the least
	if (!hasVertex || !hasIndex)
		return FALSE;

	uint32 vertexCount = 0;
	uint32 indexCount = 0;
	
	// Vertex Number line
	std::string vertexNumberLine = fileLines[2];
	parser.Parse(vertexNumberLine, ":");
	if (!parser.getFirst())
		return FALSE;
	if (!parser.getLast())
		return FALSE;
	vertexCount = atoi(parser.getToken().c_str());

	// Index Number Line
	std::string indexNUmberLine = fileLines[3];
	parser.Parse(indexNUmberLine, ":");
	if (!parser.getFirst())
		return FALSE;
	if (!parser.getLast())
		return FALSE;
	indexCount = atoi(parser.getToken().c_str());

	std::vector<PosNormalTexVertex> positionNormalTexVerticies(vertexCount);
	std::vector<WORD> indicies(indexCount);

	int iVertex = 0;
	int iIndex = 0;

	for (std::string line : fileLines)
	{
		// Skip comments and statistics
		if (line[0] == COMMENT_INDICATOR || line[0] == STATISTICS_INDICATOR)
			continue;

		if (line[0] == VERTEX_INDICATOR)
		{
			parser.Parse(line, "|");
			if (!parser.getFirst())
				return FALSE;
			if (parser.getSize() < 4)
				return FALSE;

			PosNormalTexVertex vertex;

			
			// First vertex
			parser.getNext();
			vertex.Pos.x = (float) atof(parser.getToken().c_str());
			
			// Second vertex
			parser.getNext();
			vertex.Pos.y = (float) atof(parser.getToken().c_str());

			// Third vertex
			parser.getNext();
			vertex.Pos.z = (float) atof(parser.getToken().c_str());

			int skips = 3;
			if (hasNormal)
			{
				// First Normal
				parser.getNext();
				vertex.Normal.x = (float) atof(parser.getToken().c_str());

				// Second Normal
				parser.getNext();
				vertex.Normal.y = (float) atof(parser.getToken().c_str());

				// Third Normal
				parser.getNext();
				vertex.Normal.z = (float) atof(parser.getToken().c_str());

				skips = 0;
			}

			if (hasUV)
			{
				for (int i = 0; i < skips; i++)
					parser.getNext();

				parser.getNext();
				vertex.Tex.x = (float)atof(parser.getToken().c_str());

				parser.getNext();
				vertex.Tex.y = (float)atof(parser.getToken().c_str());
			}

			positionNormalTexVerticies[iVertex++] = vertex;
			continue;
		}

		if (line[0] == INDEX_INDICATOR)
		{
			parser.Parse(line, "|");
			if (!parser.getFirst())
				return FALSE;
			if (parser.getSize() < 4)
				return FALSE;

			WORD index;

			// First vertex
			parser.getNext();
			index =  atoi(parser.getToken().c_str());
			indicies[iIndex++] = index - 1;

			// Second vertex
			parser.getNext();
			index = atoi(parser.getToken().c_str());
			indicies[iIndex++] = index - 1;

			// Third vertex
			parser.getNext();
			index = atoi(parser.getToken().c_str());
			indicies[iIndex++] = index - 1;
			
			continue;
		}
	}

	if (!hasNormal)
	{
		// Convert to simple
		std::vector<SimpleVertex> simpleVerticies(vertexCount);
		for (uint32 i = 0; i < vertexCount; i++)
			simpleVerticies[i] = TruncateNormalTex(positionNormalTexVerticies[i]);

		*pMeshData = new TestMesh();
		((TestMesh*)(*pMeshData))->SetVerticies(&simpleVerticies[0], vertexCount, &indicies[0], indicies.size());
	}
	else if (hasNormal && !hasUV)
	{
		// Convert to Pos/Normal
		std::vector<PosNormalVertex> posNormalVerticies(vertexCount);
		for (uint32 i = 0; i < vertexCount; i++)
			posNormalVerticies[i] = TruncateTex(positionNormalTexVerticies[i]);

		*pMeshData = new PosNormalMesh();
		((PosNormalMesh*)(*pMeshData))->SetVerticies(&posNormalVerticies[0], vertexCount, &indicies[0], indexCount);
	}
	else if (hasNormal && hasUV)
	{
		*pMeshData = new PosNormalTexMesh();
		((PosNormalTexMesh*)(*pMeshData))->SetVerticies(&positionNormalTexVerticies[0], vertexCount, &indicies[0], indexCount);
	}

	return TRUE;
}