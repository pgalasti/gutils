#pragma once
#include "stdafx.h"

#include "ResourceCache.h"
#include "UtilFunctions.h"

using namespace GUtils::Utils::Functions;

using namespace GUtils::IO::Resources;
using namespace GUtils::IO::Resources::Cache;

ResourceCache::ResourceCache(const uint32 sizeInBytes, IPackageFile* pPackageFile)
{
	m_TotalCacheSize = sizeInBytes;
	m_AllocatedMemory = 0;

	m_pPackageFile = pPackageFile;
}

ResourceCache::~ResourceCache()
{
	while (!m_LeastUsedResourceHandles.empty())
	{
		this->FreeOneResource();
	}

	SAFE_DELETE(m_pPackageFile);
}

BOOL ResourceCache::Initialize()
{
	if (m_pPackageFile == nullptr)
		return FALSE;

	if (!m_pPackageFile->Open())
		return FALSE;

	// We'll always have a default loader so it doesn't fail when attempting to open an unknown file
	this->RegisterLoader(SharedIResourceLoaderPtr(new DefaultResourceLoader()));

	return TRUE;
}

void ResourceCache::RegisterLoader(SharedIResourceLoaderPtr pLoader)
{
	m_ResourceLoaders.push_front(pLoader);
}

SharedResourceHandlePtr ResourceCache::GetHandle(const PackageResource& resource)
{
	SharedResourceHandlePtr pResourceHandle = this->FindCacheResource(resource);
	if (pResourceHandle == nullptr)
	{
		// Allocate new memory if not found
		pResourceHandle = this->Load(resource);
	}
	else
	{
		// Just update the recently used list that we need access to the memory
		this->Update(pResourceHandle);
	}

	return pResourceHandle;
}

void ResourceCache::Flush()
{
	while (!m_LeastUsedResourceHandles.empty())
	{
		SharedResourceHandlePtr pResourceHandle = *m_LeastUsedResourceHandles.begin();
		Free(pResourceHandle);
		m_LeastUsedResourceHandles.pop_front();
	}
}

SharedResourceHandlePtr ResourceCache::FindCacheResource(const PackageResource& resource)
{
	auto iter = m_ResourceHandleMap.find(resource.GetName());
	if (iter == m_ResourceHandleMap.end())
		return SharedResourceHandlePtr();

	return iter->second;
}

void ResourceCache::Update(SharedResourceHandlePtr resourceHandle)
{
	m_LeastUsedResourceHandles.remove(resourceHandle);
	m_LeastUsedResourceHandles.push_front(resourceHandle);
}

SharedResourceHandlePtr ResourceCache::Load(const PackageResource& resource)
{
	SharedIResourceLoaderPtr pLoader = nullptr;
	SharedResourceHandlePtr pResourceHandle = nullptr;

	// Find our loader interface for this file type
	for (auto iter = m_ResourceLoaders.begin(); iter != m_ResourceLoaders.end(); iter++)
	{
		SharedIResourceLoaderPtr pCurrentLoader = *iter;
		if (WildcardMatch(pCurrentLoader->GetPattern().c_str(), resource.GetName().c_str()))
		{
			pLoader = pCurrentLoader;
			break;
		}
	}
	if (pLoader == nullptr)
	{
		assert(false);
		return pResourceHandle;
	}

	// Make sure we have the resource loaded correctly
	const uint32 rawSize = m_pPackageFile->GetResourceSize(resource);
	if (rawSize < 1)
	{
		assert(false);
		return pResourceHandle;
	}

	// Request manager to make space for the file size if it's raw data; 
	// otherwise allocate new memory perform handling and request later
	sbyte8* pRawBuffer = nullptr;
	if (pLoader->UseRawFile())
		pRawBuffer = this->Allocate(rawSize);
	else
		pRawBuffer = new sbyte8[rawSize];

	if (pRawBuffer == nullptr)
		return pResourceHandle;

	// Load the raw data from the resource into a buffer
	if (m_pPackageFile->GetResource(resource, pRawBuffer) < 1)
	{
		assert(false);
		return pResourceHandle;
	}

	sbyte8* pBuffer = nullptr;
	uint32 size = 0;

	// If we're using raw data type, just throw the data into a resource handle;
	// otherwise let the loader interface determine how much memory and how it should be loaded
	if (pLoader->UseRawFile())
	{
		pBuffer = pRawBuffer;
		pResourceHandle = SharedResourceHandlePtr(new ResourceHandle(resource, rawSize, pBuffer));
	}
	else
	{
		// Get the actual size according to the loader interface and request it from the
		// manager
		size = pLoader->GetLoadedResourceSize(pRawBuffer, rawSize);
		pBuffer = this->Allocate(size);
		if (pRawBuffer == nullptr || pBuffer == nullptr)
			return pResourceHandle;

		// Load the resource handle with the processed size and load it to the resource
		pResourceHandle = SharedResourceHandlePtr(new ResourceHandle(resource, size, pBuffer));
		const bool loadSuccessful = pLoader->LoadResource(pRawBuffer, rawSize, pResourceHandle);
		SAFE_ARR_DELETE(pRawBuffer);

		if (!loadSuccessful)
			return SharedResourceHandlePtr();
	}

	// If successful, we want to set it as last used and add it to our mapping
	if (pResourceHandle)
	{
		m_LeastUsedResourceHandles.push_front(pResourceHandle);
		m_ResourceHandleMap[resource.GetName()] = pResourceHandle;
	}

	return pResourceHandle;
}

sbyte8* ResourceCache::Allocate(const uint32 size)
{
	if (!MakeRoom(size))
		return nullptr;

	sbyte8* pMemory = nullptr;
	pMemory = new sbyte8[size];
	if (pMemory)
		m_AllocatedMemory += size;

	return pMemory;
}

void ResourceCache::Free(SharedResourceHandlePtr pResourceHandle)
{
	m_LeastUsedResourceHandles.remove(pResourceHandle);
	std::string resourceName = pResourceHandle->GetPackageResource().GetName();
	if (m_ResourceHandleMap.find(resourceName) == m_ResourceHandleMap.end())
		return;

	m_AllocatedMemory -= pResourceHandle->GetSize();
	m_ResourceHandleMap.erase(resourceName);
}

BOOL ResourceCache::MakeRoom(const uint32 size)
{
	if (size > m_TotalCacheSize)
		return FALSE;

	while (size > (m_TotalCacheSize - m_AllocatedMemory))
	{
		if (m_LeastUsedResourceHandles.empty())
			return FALSE;

		this->FreeOneResource();
	}

	return TRUE;
}

void ResourceCache::FreeOneResource()
{
	auto lastHandleIter = m_LeastUsedResourceHandles.end();
	lastHandleIter--;

	SharedResourceHandlePtr  pHandle = *lastHandleIter;
	m_LeastUsedResourceHandles.pop_back();

	const std::string resourceName = pHandle->GetPackageResource().GetName();
	m_ResourceHandleMap.erase(resourceName);
}

uint32 ResourceCache::Preload(const std::string& pattern, void(*progressCallback)(int, bool&))
{
	if (m_pPackageFile == nullptr)
		return 0;

	const uint32 numberOfFiles = m_pPackageFile->GetNumberOfResources();
	uint32 currentLoadedFiles = 0;
	bool cancelLoad = false;

	for (uint32 i = 0; i < numberOfFiles; i++)
	{
		if (cancelLoad)
			break;

		PackageResource resource(m_pPackageFile->GetResourceName(i));
		if (WildcardMatch(pattern.c_str(), resource.GetName().c_str()))
		{
			SharedResourceHandlePtr pResourceHandle = this->GetHandle(resource);
			if (pResourceHandle == nullptr)
				break;

			++currentLoadedFiles;
		}

		if (progressCallback)
			progressCallback(i * 100 / numberOfFiles, cancelLoad);
	}

	return currentLoadedFiles;
}

uint32 ResourceCache::Preload(const std::list<PackageResource>& resources, void(*progressCallback)(int, bool&))
{
	if (m_pPackageFile == nullptr)
		return 0;

	if (resources.empty())
		return 0;

	uint32 currentLoadedFiles = 0;
	bool cancelLoad = false;

	//for (PackageResource resource : resources)
	for (auto resourcesIter = resources.begin(); resourcesIter != resources.end(); resourcesIter++)
	{
		if (cancelLoad)
			break;

		SharedResourceHandlePtr pResourceHandle = this->GetHandle(*resourcesIter);
		if (pResourceHandle == nullptr)
			break;

		++currentLoadedFiles;

		if (progressCallback)
			progressCallback(currentLoadedFiles, cancelLoad);
	}

	return currentLoadedFiles;
}

void ResourceCache::ReleaseHandle(const PackageResource& resource)
{
	auto iter = m_ResourceHandleMap.find(resource.GetName());
	if (iter == m_ResourceHandleMap.end())
		return;
	
	SharedResourceHandlePtr pHandle = iter->second;
	
	Free(pHandle);
}

void ResourceCache::ReleaseHandle(SharedResourceHandlePtr handle)
{
	const PackageResource resource = handle->GetPackageResource();
	this->ReleaseHandle(resource);
}