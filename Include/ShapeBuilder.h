#pragma once
#include "stdafx.h"

#include "TestMesh.h"

namespace GraphicsEngine {
	namespace Model {

		class DllExport ShapeBuilder
		{
		public:
			ShapeBuilder() = delete;
			ShapeBuilder(const ShapeBuilder& other) = delete;
			~ShapeBuilder() = delete;

			static void CreateBox(const float width, const float height, const float depth, TestMesh& meshData);
			static void CreateSphere(float radius, UINT sliceCount, UINT stackCount, TestMesh& meshData);
		};



	}
}
