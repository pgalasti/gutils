#pragma once
#include "stdafx.h"

#include "Actor.h"
#include "GMath.h"

using namespace GameEngine::Logic::Actors;
using namespace GUtils::GMath;

namespace GameEngine {
	namespace Logic {
		namespace Actors {
			namespace Default {

				class TransformComponent : public ActorComponent
				{
					Values4X4Struct<float> m_WorldMatrix;

				public:
					virtual ComponentId GetComponentId() const override { return 0xFBD6D33D; }

					virtual void PostInit() override {}
					virtual void Update(const uint32 deltaTime) override {}

					virtual BOOL Init(ElementPtr pElement) override
					{
						ZeroMemory(&m_WorldMatrix, sizeof Values4X4Struct<float>);

						std::list<Element> scaleElement;
						pElement->FindElements("Scale", scaleElement);
						if (scaleElement.size() != 1)
							return FALSE;

						auto element = scaleElement.begin();
						AttributePtr pXAttribute = element->GetAttributeByName("x");
						AttributePtr pYAttribute = element->GetAttributeByName("y");
						AttributePtr pZAttribute = element->GetAttributeByName("z");

						const float x = atof(pXAttribute->Value.c_str());
						const float y = atof(pYAttribute->Value.c_str());
						const float z = atof(pZAttribute->Value.c_str());

						m_WorldMatrix = ScaleStruct<float>(x, y, z);

						return TRUE;
					}

					Values4X4Struct<float> GetScaleMatrix() const { return m_WorldMatrix; }

				};

			}
		}
	}
}