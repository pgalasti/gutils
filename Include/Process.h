#pragma once
#include "stdafx.h"

#include <memory>

namespace GameEngine {
	namespace Logic {
		namespace Process {

			class DllExport Process;

			typedef std::weak_ptr<Process> WeakProcessPtr;
			typedef std::shared_ptr<Process> StrongProcessPtr;
			typedef StrongProcessPtr ProcessPtr;

			class DllExport Process
			{
			
			public:
				enum State
				{
					Uninitialized = 0,
					Removed,
					Running,
					Paused,
					Succeeded,
					Failed,
					Aborted,
				};

			private:
				State m_State;
				StrongProcessPtr m_pChild;

			public:
				Process() : m_State(Uninitialized) {};
				virtual ~Process() { if (m_pChild) m_pChild->OnAbort(); }

				void Succeed()	{ m_State = Succeeded; }
				void Fail()		{ m_State = Failed; }
				void Pause()	{ m_State = Paused; }
				void UnPaused()	{ m_State = Running; }
				
				void AttachChild(StrongProcessPtr pChild) { m_pChild = pChild; }
				StrongProcessPtr PeekChild() { return m_pChild; }
				StrongProcessPtr RemoveChild()
				{
					if (m_pChild)
					{
						StrongProcessPtr pChild = m_pChild;
						m_pChild.reset();
						return pChild;
					}

					return StrongProcessPtr();
				}

				void SetState(const State state) { m_State = state; }
				State GetState() const { return m_State; }

				bool IsAlive() const { return (m_State == Running || m_State == Paused); }
				bool IsDead() const { return (m_State == Succeeded || m_State == Failed || m_State == Aborted); }
				bool IsRemoved() const { return m_State == Removed;  }
				bool IsPaused() const { return m_State == Paused; }
			
				virtual BOOL OnInit() { m_State = Running; return TRUE; }
				virtual BOOL OnSuccess() { return TRUE; }
				virtual BOOL OnFail() { return TRUE; }
				virtual BOOL OnAbort() { return TRUE; }
				virtual BOOL OnUpdate(const uint32 deltaTime) = VIRTUAL;



			};


		}
	}
}