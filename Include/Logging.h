#pragma once
#include "stdafx.h"
#include <fstream>
#include <list>
#include <string>

namespace GUtils {
	namespace Debug {
		
// Turn on/off
#define DEBUG_DUMP

#define LOG m_pLog

#ifdef DEBUG_DUMP

#define MEMBER_LOGGING_ENABLED GUtils::Debug::Logger* LOG;
#define DEFAULT_LOG_PATH "C:\\Users\\Public\\"

#define DEBUG_LOG(pL, pszMsg) if(pL) {pL->WriteTo(pszMsg);}
#define INITIALIZE_LOG(LOGPATH)	LOG = new GUtils::Debug::Logger(LOGPATH);	\
						LOG->Startup();

#define DELETE_LOG SAFE_DELETE(LOG)
#else
#define MEMBER_LOGGING_ENABLED
#define DEBUG_LOG(pL, pMsg)
#define INITIALIZE_LOG(LOGPATH)
#define DELETE_LOG
#endif

		// Logging class
		class DllExport Logger
		{

		public:
			// Method for when log token should be written to file.
			// WriteOnDump: Delayed until specified
			// WriteOnFly: Written immediately.
			static enum WriteMethod {
				WriteOnDump = 0,
				WriteOnFly = 1,
			};


			// Sets the path for the logging file. File is not yet open or created. Defaults to Logger::WriteMethod::WriteOnFly.
			Logger(const std::string strPath, Logger::WriteMethod method = WriteOnFly);
			// Sets the path for the logging file. File is not yet open or created. Defaults to Logger::WriteMethod::WriteOnFly.
			Logger(const char* szPath, Logger::WriteMethod method = WriteOnFly);
			~Logger();

			// Will open the file stream.
			// Return (bool) : If the file stream state is good.
			const bool Startup();

			// Will write debug tokens not already written to the file and clear the saved tokens.
			// Return (bool) : If writing was successful.
			const bool DumpToFile();
			
			// Sets the Logger::WriteMethod.
			void SetWriteMethod(Logger::WriteMethod method);

			// Will write to file immediately if the Logger::WriteMethod::WriteOnFly is set. Otherwise, token will be saved to be written to file from DumpToFile().
			void WriteTo(const std::string str);
			// Will write to file immediately if the Logger::WriteMethod::WriteOnFly is set. Otherwise, token will be saved to be written to file from DumpToFile().
			void WriteTo(const char* szStr);
			// Will write to file immediately if the Logger::WriteMethod::WriteOnFly is set. Otherwise, token will be saved to be written to file from DumpToFile().
			void operator<<(const std::string str);
			// Will write to file immediately if the Logger::WriteMethod::WriteOnFly is set. Otherwise, token will be saved to be written to file from DumpToFile().
			void operator<<(const char* szStr);


		private:
			void setPath(const char* szPath);
			std::string GetTimeStamp() const;

			bool					m_bWriteOnFly;
			std::ofstream			m_File;
			std::string				m_StrPath;
			std::list<std::string>	m_DebugTokens;
		};

	}
}