#pragma once
#include "stdafx.h"

// Taken from Frank Luna 3d Game Programming Direct3d 11.0 game timer http://d3dcoder.net/
namespace GUtils {
	namespace Time {

		class DllExport GameTimer
		{
		public:
			GameTimer();

			float TotalTime()const;  // in seconds
			float DeltaTime()const; // in seconds

			void Reset(); // Call before message loop.
			void Start(); // Call when unpaused.
			void Stop();  // Call when paused.
			void Tick();  // Call every frame.

		private:
			double mSecondsPerCount;
			double mDeltaTime;

			__int64 mBaseTime;
			__int64 mPausedTime;
			__int64 mStopTime;
			__int64 mPrevTime;
			__int64 mCurrTime;

			bool mStopped;

			bool m_FPSTracking;
			float m_FPS;
		};
	}
}

