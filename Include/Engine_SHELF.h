//#pragma once
//#include "stdafx.h"
//
//#include <future>
//#include <map>
//#include <set>
//
//#include "Engine.h"
//#include "Logging.h"
//#include "Camera.h"
//#include "DirectX11Renderer.h"
//#include "TestMesh.h"
//#include "MemoryPool.h"
//#include "IDGenerator.h"
//#include "Metrics.h"
//
//// Compiler complains it doesn't like std structures being exposed (e.g. map<>) since 
//// it doesn't have a DLL export interface. This won't be a problem since the compiler
//// for modules based on the engine will be the same. 
//#pragma warning(disable:4251) 
//
//namespace GUtils {
//	namespace Graphics {
//
//
//		class DllExport DirectX11Engine : public IGraphicEngine
//		{
//		public:
//			DirectX11Engine();
//			virtual ~DirectX11Engine() override;
//
//			virtual BOOL Initialize(HWND hwnd, const EngineOptions& options) override;
//			virtual BOOL Shutdown() override;
//			virtual BOOL Render(const float dt) override;
//
//			virtual std::string GetVideoCardDesc() const override;
//			virtual uint32 GetVideoMemory() const override;
//			virtual float FPS() const override;
//
//			virtual BOOL RegisterModel(const char* pszModelResourcePath, uint32& resourceID) override;
//			virtual void DeregisterModel(const uint32 resourceID) override;
//			virtual void TransformModel(const int resourceID, const ConstantBuffer& constantBuffer) override;
//
//			virtual BOOL RegisterMaterialToModel(const int resourceIDOfModel, const Material& material) override;
//			virtual BOOL DeregisterMaterialToModel(const int resourceIDOfModel) override;
//
//			virtual BOOL RegisterTextureToModel(const uint32 resourceID, const char* pszTexturePath, const char* pszSamplerName) override;
//			virtual BOOL DeregisterTextureToModel(const uint32 resourceID) override;
//
//			virtual BOOL RegisterDirectionalLight(const DirectionalLight& directionalLight, uint32& resourceID) override;
//			virtual BOOL DergisterDirectionalLight(const uint32 resourceID) override;
//			virtual BOOL ApplyDirectionalLight(const DirectionalLight& directionalLight, const uint32 resourceID) override;
//
//			virtual BOOL RegisterPointLight(const PointLight& pointLight, uint32& resourceID)override;
//			virtual BOOL DergisterPointLight(const uint32 resourceID) override;
//			virtual BOOL ApplyPointLight(const PointLight& pointLight, const uint32 resourceID) override;
//
//			virtual BOOL RegisterSpotLight(const SpotLight& spotLight, uint32& resourceID) override;
//			virtual BOOL DergisterSpotLight(const uint32 resourceID) override;
//			virtual BOOL ApplySpotLight(const SpotLight& spotLight, const uint32 resourceID) override;
//
//			virtual BOOL AssignVertexShaderToModel(const int resourceID, std::string shaderID) override;
//			virtual BOOL AssignPixelShaderToModel(const int resourceID, std::string shaderID) override;
//
//			virtual uint32 GetLoadedVerticiesNumber() override { return m_nLoadedVerticies; }
//			virtual uint32 GetLoadedIndiciesNumber() override { return m_nLoadedIndicies; }
//
//			virtual void ApplyCamera(const Camera& camera) override;
//			virtual void GetCamera(Camera& camera) override;
//
//			virtual void SetRasterizerState(const char* pszRasterizerStateName) override;
//
//		protected:
//
//			// Initialization Functions
//			virtual BOOL LoadDisplayAdapter(const EngineOptions& options, DXGI_ADAPTER_DESC& adapterDescription, DXGI_RATIONAL& refreshRate);
//			virtual BOOL LoadDeviceAndSwapChain(HWND hwnd, const EngineOptions& options, const DXGI_RATIONAL& refreshRate);
//			virtual BOOL LoadDepthStencilBufferAndState(const EngineOptions& options);
//			virtual BOOL LoadViewport(const EngineOptions& options, D3D11_VIEWPORT& viewport);
//			virtual BOOL LoadRasterizerStates();
//			virtual BOOL LoadShaders(); // Make this on the interface level?
//			virtual BOOL LoadConstantBuffers();
//			virtual BOOL InitializeCamera(const EngineOptions& options);
//			virtual BOOL LoadSamplers();
//
//			virtual BOOL LoadPendingModel(const int resourceID); // Make this on the interface level?
//			virtual BOOL RenderRegisteredResources(); // Make this on the interface level?
//			virtual BOOL RenderRegisteredLights();
//
//			ID3D11Device* m_pDevice;
//			ID3D11DeviceContext* m_pDeviceContext;
//			IDXGISwapChain*	m_pSwapChain;
//			ID3D11RenderTargetView* m_pRenderTargetView;
//			ID3D11Texture2D* m_pDepthStencilBuffer;
//			ID3D11DepthStencilState* m_pDepthStencilState;
//			ID3D11DepthStencilView* m_pDepthStencilView;
//			ID3D11RasterizerState* m_pRasterState;
//			ID3D11Buffer* m_pPerObjectConstantBuffer;
//			ID3D11Buffer* m_pPerFrameConstantBuffer;
//
//			Camera m_Camera;
//
//			typedef std::pair<ID3D11VertexShader*, ID3D11InputLayout*> VertexShaderAndInputPair_t;
//			std::map<std::string, VertexShaderAndInputPair_t> m_VertexShaderMap;
//			std::map<std::string, ID3D11PixelShader*> m_PixelShaderMap;
//			std::map<std::string, ID3D11RasterizerState*> m_RasterizerStateMap;
//			std::map<std::string, ID3D11SamplerState*> m_SamplerStateMap;
//			// <ID, Shader Name>
//			std::vector<VertexShaderAndInputPair_t> m_RegisteredModelToVSVector;
//			std::vector<ID3D11PixelShader*> m_RegisteredModelToPSVector;
//
//			std::map<int, Material> m_MaterialToRegisteredModelMap;
//
//			// Id to the texture resource as well as the sampler state for the texture
//			std::map<uint32, std::pair<ID3D11ShaderResourceView*, ID3D11SamplerState*>> m_TextureToRegisteredModelMap;
//
//			// Move to IGraphicsEngine?
//			IRenderer<SimpleVertex>* m_pRenderer;
//
//			std::unique_ptr<GUtils::Concurrency::IDGenerator> m_pMeshIdGenerator;
//			std::unique_ptr<GUtils::Concurrency::IDGenerator> m_pDirectionalLightIdGenerator;
//			std::unique_ptr<GUtils::Concurrency::IDGenerator> m_pPointLightIdGenerator;
//			std::unique_ptr<GUtils::Concurrency::IDGenerator> m_pSpotLightIdGenerator;
//
//			std::map<int, std::future<IMesh*>> m_LoadingMeshMap;
//			// Since the path is loaded as an async function parameter, we need to keep track of it to make sure it doesn't go out of stack
//			// I feel like this is an ugly hack
//			std::map<int, std::string> m_LoadingModelPath;
//
//			std::map<int, IMesh*> m_LoadedMeshMap;
//			std::set<int> m_MeshIds;
//			std::set<std::string> m_LoadedModels;
//			std::set<int> m_RegisteredIds;
//
//			std::vector<DirectionalLight> m_LoadedDirectionalLightVector;
//			std::vector<PointLight> m_LoadedPointLightVector;
//			std::vector<SpotLight> m_LoadedSpotLightVector;
//
//			typedef std::pair <int, ConstantBuffer> TransformOnModelKey_t;
//			std::vector<TransformOnModelKey_t> m_ModelTransformations;
//			std::map<int, ConstantBuffer> m_ModelTransformationMap;
//
//			bool m_bShutdown;
//
//		private:
//
//			bool m_bMultithreadingSupport;
//			std::vector<ID3D11DeviceContext*> m_DeferredContextList;
//
//			BOOL AsyncMeshRender(ID3D11DeviceContext* pContext, IMesh* pMesh, const ConstantBuffer& perModelBuffer);
//
//			GUtils::Time::GameTimer m_EngineTimer;
//			uint32 m_nVideoMemory;
//			std::string m_VideoCardDesc;
//			uint32 m_nLoadedVerticies;
//			uint32 m_nLoadedIndicies;
//			IMesh* LoadModelAsync(const char* pszModelResourcePath);
//
//			BOOL LoadVertexShaderFiles();
//			BOOL LoadPixelShaderFiles();
//
//			BOOL SetRegisteredResourceVertexShaderAndLayout(const int resourceID, ID3D11DeviceContext* pContext);
//			BOOL SetRegisteredResourcePixelShader(const int resourceID, ID3D11DeviceContext* pContext);
//			bool GetRegisteredResourceMaterial(const int resourceID, Material& material);
//			bool GetRegisteredResourceTexture(const uint32 resourceID, ID3D11DeviceContext* pContext);
//
//			BOOL GetRegisteredMesh(const int resourceID, IMesh** pMesh);
//
//			void ClearScreenToDefaultColor(ID3D11DeviceContext* pContext);
//
//			bool m_bRasterizerStateChanged;
//
//			MEMBER_LOGGING_ENABLED
//		};
//	}
//}
//
//#pragma warning(default:4251) 