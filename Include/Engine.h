#pragma once

#include "stdafx.h"
#include <d3dcommon.h>
#include <d3d11_1.h>
#include <DirectXMath.h>
#include "Timer.h"
#include "GMath.h"
#include "Camera.h"

#include<string>

using namespace GUtils::GMath;

namespace GUtils { // Rename to graphics later when in own DLL
	namespace Graphics {

		// temp
		struct DllExport Material
		{
			GMath::Vector4DValues<float32> Ambient;
			GMath::Vector4DValues<float32> Diffuse;
			GMath::Vector4DValues<float32> Specular;
		};

		struct DllExport ConstantBuffer
		{
			// Eventually will need my own matrix class to make it not dependent on API .
			//GMath::Values4X4Struct<float> worldViewProjection;
			//GMath::Values4X4Struct<float> world;
			//GMath::Values4X4Struct<float> worldInverseTranspose;

			DirectX::XMMATRIX worldViewProjection;
			DirectX::XMMATRIX world;
			DirectX::XMMATRIX worldInverseTranspose;
			Material material;
			DirectX::XMMATRIX TextureTransformation;

			ConstantBuffer& operator= (const ConstantBuffer& otherVector) 
			{ 
				//static int debug = 0;
				//printf("%d\n", ++debug);
				//this->worldViewProjection = otherVector.worldViewProjection;
				//this->world = otherVector.world;
				//this->worldInverseTranspose = otherVector.worldInverseTranspose;
				//this->TextureTransformation = otherVector.TextureTransformation;
				//memcpy(&this->material, &otherVector.material, sizeof Material);

				memcpy(this, &otherVector, sizeof ConstantBuffer);

				return *this;
			};
		};

		struct DllExport DirectionalLight
		{
			GMath::Vector4DValues<float32> Ambient;
			GMath::Vector4DValues<float32> Diffuse;
			GMath::Vector4DValues<float32> Specular;
			GMath::Vector3DValues<float32> Direction;
			float pad;
		};

		struct PointLight
		{
			GMath::Vector4DValues<float32> Ambient;
			GMath::Vector4DValues<float32> Diffuse;
			GMath::Vector4DValues<float32> Specular;
			GMath::Vector3DValues<float32> Position;
			float Range;
			GMath::Vector3DValues<float32> Attenuation;
			float pad;
		};

		struct SpotLight
		{
			GMath::Vector4DValues<float32> Ambient;
			GMath::Vector4DValues<float32> Diffuse;
			GMath::Vector4DValues<float32> Specular;
			GMath::Vector3DValues<float32> Position;
			float Range;
			GMath::Vector3DValues<float32> Direction;
			float Spot;
			GMath::Vector3DValues<float32> Attenuation;
			float Pad;
		};

		struct DllExport PerFrameConstantBuffer
		{
			DirectionalLight directionalLight[3];
			PointLight pointLight[8];
			SpotLight spotLight[8];
			GMath::Vector3DValues<float32> EyePosition;
			int numDirectionalLights;
			int numPointLights;
			int numSpotLights;
			float Pad[2];
		};

		struct DllExport EngineOptions
		{
			bool FullScreen;
			bool VSync;
			uint32 width;
			uint32 height;
			float screenDepth;
			float screenNear;

			EngineOptions() { Initialize(); }
			void Initialize() { ZeroMemory(this, sizeof EngineOptions); }

		};

		struct DllExport ScreenTextParams
		{
			void Initialize()
			{
				Text.clear();
				ZeroMemory(&Position, sizeof Point2D<uint32>);
				ZeroMemory(&Color, sizeof ColorRGBA<float>);
			};

			std::wstring Text;
			Point2D<uint32> Position;
			ColorRGBA<float> Color;
		};

		interface DllExport IGraphicEngine
		{
		public:
			IGraphicEngine() {};
			virtual ~IGraphicEngine() {};

			// Calling functions
			virtual BOOL Initialize(HWND hwnd, const EngineOptions& options) = VIRTUAL;
			virtual BOOL Shutdown() = VIRTUAL;
			virtual BOOL Render(const float dt) = VIRTUAL;
			
			// Statistics functions
			virtual std::string GetVideoCardDesc() const = VIRTUAL;
			virtual uint32 GetVideoMemory() const = VIRTUAL;
			virtual float FPS() = VIRTUAL;

			virtual uint32 GetLoadedVerticiesNumber() const = VIRTUAL;
			virtual uint32 GetLoadedIndiciesNumber() const = VIRTUAL;

			// Model functions
			virtual BOOL RegisterModel(const sbyte8* pModel, uint32& resourceID) = VIRTUAL;
			virtual BOOL RegisterModelFromPath(const char* pszModelResourcePath, uint32& resourceID) = VIRTUAL;
			virtual BOOL CopyToNewModel(const uint32 resourceID, uint32& newResourceID) = VIRTUAL;
			virtual void DeregisterModel(const uint32 resourceID) = VIRTUAL;
			virtual void TransformModel(const uint32 resourceID, const ConstantBuffer& constantBuffer) = VIRTUAL;
			virtual bool IsRegistered(const uint32 resourceID) = VIRTUAL;
			
			// Blend functions
			virtual void ApplyBlendStateToModel(const uint32 resourceID, const std::string& blendState) = VIRTUAL;

			// Texture Functions
			virtual void TransformTexture(const uint32 resourceID, const Values4X4Struct<float>& textureTransformation) = VIRTUAL;
			virtual void RemoveTextureTransformation(const uint32 resourceID) = VIRTUAL;

			virtual BOOL AssignVertexShaderToModel(const uint32 resourceID, std::string shaderID) = VIRTUAL; // TODO add parameters to RegisterModel to do it all in one call
			virtual BOOL AssignPixelShaderToModel(const uint32 resourceID, std::string shaderID) = VIRTUAL;

			// Texture Functions
			virtual BOOL RegisterTextureToModel(const uint32 resourceID, const char* pszTexturePath, const char* pszSamplerName) = VIRTUAL;
			virtual BOOL DeregisterTextureToModel(const uint32 resourceID) = VIRTUAL;

			// Font Functions
			virtual BOOL WriteScreenText(const std::wstring& font, const ScreenTextParams& params) = VIRTUAL;

			// Material Functions
			virtual BOOL RegisterMaterialToModel(const uint32 resourceIDOfModel, const Material& material) = VIRTUAL;
			virtual BOOL DeregisterMaterialToModel(const uint32 resourceIDOfModel) = VIRTUAL;

			// Light functions
			virtual BOOL RegisterDirectionalLight(const DirectionalLight& directionalLight, uint32& resourceID) = VIRTUAL;
			virtual BOOL DergisterDirectionalLight(const uint32 resourceID) = VIRTUAL;
			virtual BOOL ApplyDirectionalLight(const DirectionalLight& directionalLight, const uint32 resourceID) = VIRTUAL;

			virtual BOOL RegisterPointLight(const PointLight& directionalLight, uint32& resourceID) = VIRTUAL;
			virtual BOOL DergisterPointLight(const uint32 resourceID) = VIRTUAL;
			virtual BOOL ApplyPointLight(const PointLight& directionalLight, const uint32 resourceID) = VIRTUAL;

			virtual BOOL RegisterSpotLight(const SpotLight& spotLight, uint32& resourceID) = VIRTUAL;
			virtual BOOL DergisterSpotLight(const uint32 resourceID) = VIRTUAL;
			virtual BOOL ApplySpotLight(const SpotLight& spotLight, const uint32 resourceID) = VIRTUAL;

			// View functions
			virtual void GetCamera(GUtils::Graphics::Camera::Camera& camera) const = VIRTUAL;
			virtual void ApplyCamera(const GUtils::Graphics::Camera::Camera& camera) = VIRTUAL;

			// Misc functions
			virtual void SetRasterizerState(const char* pszRasterizerStateName) = VIRTUAL;
			virtual void SetScreenDebug(const bool set = true) = VIRTUAL;
		};

#pragma region deprecated
#ifdef DEPRECATED_ENGINE_CLASS
		// Original test engine
		using namespace DirectX;
		class DllExport SimpleEngine
		{
		public:
			SimpleEngine();
			~SimpleEngine();

			BOOL Initialize(HWND hwnd, const EngineOptions& options);
			void Shutdown();
			BOOL Render();

			//void GetVideoCardInfo(char*, int&);

			float FPS() const;

		private:
			GUtils::Time::GameTimer m_Timer;

			bool m_VSyncEnabled;
			int m_nVideoMemory; // Memory in MB
			std::string m_VideoCardDesc;

			ID3D11Device* m_pDevice;
			ID3D11DeviceContext* m_pDeviceContext;
			IDXGISwapChain*		m_pSwapChain;
			ID3D11RenderTargetView* m_pRenderTargetView;
			ID3D11Texture2D* m_pDepthStencilBuffer;
			ID3D11DepthStencilState* m_pDepthStencilState;
			ID3D11DepthStencilView* m_pDepthStencilView;
			ID3D11RasterizerState* m_pRasterState;

			XMMATRIX m_ProjectionMatrix;
			XMMATRIX m_WorldMatrix;
			XMMATRIX m_OrthoMatrix;
		};
#endif
#pragma endregion deprecated
	}
}