
#pragma once
#include<iostream>
#include <windows.h>
#include <list>
#include <map>

namespace GUtils {
	namespace Debug {

		// Performance timer for determining the time of operations between calling Start() and Stop().
		// Any measurement on program operations will always be best guess due to hardware/timer overhead.
		// Accuracy has been measured to be off by around 100 microseconds due to class overhead
		class DllExport PerformanceTimer
		{
		public:
			// Time unit measurement should be expressed in.
			enum TimeUnit { Second, MilliSecond, MicroSecond };
		private:
			typedef std::pair<std::string, std::string> TagPlotPair_t;
			typedef std::pair<double, unsigned long> TimeFreqPair_t;

			bool bHighPerformance;
			LARGE_INTEGER frequency;
			LARGE_INTEGER start;
			LARGE_INTEGER stop;
			double dblLastTime;

			std::string strCurrentTag;
			std::list<TagPlotPair_t> TagTimes;
			std::map<std::string, TimeFreqPair_t> TagSummaryMap;

			TimeUnit unit;
			double dblCalibration;

			void Calibrate();
			void CalStop();

		public:
			// Defaults the PerformanceTimer object to use TimeUnit::MilliSecond
			PerformanceTimer(TimeUnit unit = MilliSecond);
			~PerformanceTimer();

			// The start plot for the PerformanceTimer. Defaults with a blank tag.
			void Start(std::string sTag = "");
			// The stop plot for the PerformanceTimer. Internal counters will be stopped.
			void Stop();
			// Clears the prior measured plot points.
			void ClearPlots();
			// Sets the TimeUnit measurement should be expressed in.
			void SetUnit(TimeUnit unit);

			// Dumps results to the file path specified.
			// Return (BOOL) : TRUE on success to write to file. FALSE on failure to write to file.
			BOOL DumpToFile(const char* pszPath);
			// The last time recorded between a Start() and Stop() plot points.
			// Return (double) : The time (in milliseconds) of the last time measured.
			double LastTime() const;
			// Checks to confirm the CPU has the higher performance measurement requirements.
			// Return (bool) : true if the CPU has the high performance hardware. false if the CPU lacks high performance hardware.
			bool IsHighPerformance() const;



		};

	}
}