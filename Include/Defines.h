#define SYSTEM_EXIT 0
#define SYSTEM_ERROR -1

#define SAFE_DELETE(p) if(p) {delete p; p = nullptr; }
#define SAFE_ARR_DELETE(p) if(p) {delete[] p; p = nullptr;}
#define COM_RELEASE(p) if(p) p->Release(); p = nullptr;

#define FAILS FAILED 

#define STRINGS_EQUAL 0

#define KILOBYTE 1024
#define MEGABYTE KILOBYTE*1024
#define GIGABYTE MEGABYTE*1024

#ifndef ZeroMemory
#define ZeroMemory(p, s) memset(p, 0, s)
#endif

// Fast Log Macros
#ifdef _DEBUG 
#define DEBUG_LOG_MEMBER GUtils::Debug::Logger* LOG; 
static int logCounter = 0;

#define DEBUG_CREATE_LOG(name)									\
	char szBuffer[_MAX_PATH];									\
	sprintf(szBuffer, "C:\\Users\\Public\\logs\\%s_%dLog.txt", name, ++logCounter);	\
	LOG = new GUtils::Debug::Logger(szBuffer);					\
	LOG->Startup();												\
	sprintf(szBuffer, "Creating Log for class %s", name);		\
	LOG->WriteTo(szBuffer);										

#define DEBUG_DESTROY_LOG if(LOG) { DEBUG_MESSAGE("Destroying log") \
	SAFE_DELETE(LOG)}

#define DEBUG_MESSAGE(msg) if(LOG) {LOG->WriteTo(msg);}
#define ERROR_MESSAGE(msg) DEBUG_MESSAGE(msg)
#else

#define DEBUG_LOG_MEMBER
#define DEBUG_CREATE_LOG(null)
#define DEBUG_DESTROY_LOG
#define DEBUG_MESSAGE(null)
#define ERROR_MESSAGE(null)

#endif 

