#pragma once
#include "stdafx.h"

#include <string>

namespace GUtils {
	namespace Graphics {

		class DllExport ResourceDirectories
		{
		public:

			ResourceDirectories(){};
			ResourceDirectories(const ResourceDirectories& other) = delete;

			std::wstring GetBinaryPath();

			// TODO resolve this wide character stuff later
			std::string GetLoggingPath();

			std::string GetModelDirectoryPath();

			std::string GetFontPath();

			std::string GetTextureDirectoryPath();

		private:
			std::string GetBinaryPathStdString();

			std::string m_BinaryPathStr;
			std::wstring m_BinaryPathStrWide;
			std::string m_LoggingPathStr;
			std::string m_ModelPathStr;
			std::string m_FontPathStr;
			std::string m_TexturePathStr;
		};
	}
}