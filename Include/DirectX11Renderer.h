#pragma once

#include "stdafx.h"
#include <d3d11_1.h>

#include "IRenderer.h"

template <typename T>
class DllExport DirectX11Renderer : public IRenderer<T>
{
public:

	DirectX11Renderer(ID3D11Device* pDevice, ID3D11DeviceContext* pDeviceContext)
	{
		m_pDevice = pDevice;
		m_pDeviceContext = pDeviceContext;
		m_pVertexBuffer = nullptr;
		m_pIndexBuffer = nullptr;
	}

	virtual ~DirectX11Renderer()
	{
		COM_RELEASE(m_pVertexBuffer);
		COM_RELEASE(m_pIndexBuffer);
	}

	virtual BOOL Render(const std::vector<T>& verticies, UINT numVerticies, const std::vector<WORD>& indicies, UINT numIndicies) override
	{

		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));
		bd.Usage = D3D11_USAGE_DYNAMIC;
		bd.ByteWidth = sizeof(T) * numVerticies;
		bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		static bool bFirst = true;
		if (bFirst)
		{
			bFirst = false;
			//ID3D11Buffer* pVertexBuffer;
			if (FAILED(m_pDevice->CreateBuffer(&bd, NULL, &m_pVertexBuffer)))
				return FALSE;
		
		}
		D3D11_MAPPED_SUBRESOURCE vertexBufferResource;
		m_pDeviceContext->Map(m_pVertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &vertexBufferResource);
		memcpy(vertexBufferResource.pData, &verticies[0], sizeof(T) * numVerticies);
		m_pDeviceContext->Unmap(m_pVertexBuffer, 0);

		// Set vertex buffer
		UINT stride = sizeof(T);
		UINT offset = 0;
		m_pDeviceContext->IASetVertexBuffers(0, 1, &m_pVertexBuffer, &stride, &offset);

		D3D11_SUBRESOURCE_DATA InitData;
		bd.Usage = D3D11_USAGE_DYNAMIC;
		bd.ByteWidth = sizeof(WORD) * numIndicies;
		bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
		bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		InitData.pSysMem = &indicies[0];

		ID3D11Buffer* pIndexBuffer;
		if (FAILED(m_pDevice->CreateBuffer(&bd, &InitData, &pIndexBuffer)))
			return FALSE;

		m_pDeviceContext->IASetIndexBuffer(pIndexBuffer, DXGI_FORMAT_R16_UINT, 0);
		pIndexBuffer->Release();
		// Set primitive topology
		m_pDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		m_pDeviceContext->DrawIndexed(numIndicies, 0, 0);

		return TRUE;
	}

protected:
	ID3D11Device* m_pDevice;
	ID3D11DeviceContext* m_pDeviceContext;
	ID3D11Buffer* m_pVertexBuffer;
	ID3D11Buffer* m_pIndexBuffer;
};