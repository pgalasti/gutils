#pragma once
#include "stdafx.h"

#include "PackageFiles.h"
#include "ZipLoader.h"
#include "Resources.h"

#include<string>

using namespace GUtils::IO::Zip;

namespace GUtils {
	namespace IO	{
		namespace Resources {
			namespace Packaging {

				class DllExport ZipPackageFile : public IPackageFile
				{
				public:
					ZipPackageFile(const std::string& zipFilePath);
					virtual ~ZipPackageFile();
					virtual BOOL Open() override;
					virtual uint32 GetResourceSize(const PackageResource& resource) override;
					virtual uint32 GetResource(const PackageResource& resource, sbyte8* pBuffer) override;
					virtual uint32 GetNumberOfResources() const override;
					virtual std::string GetResourceName(const uint32 number) const override;

				private:
					ZipLoader* m_pZipLoader;
					std::wstring m_ZipFileName;
				};

			}
		}
	}
}