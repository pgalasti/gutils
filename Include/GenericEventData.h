#pragma once
#include "stdafx.h"

#include "IEventData.h"

namespace GameEngine {
	namespace Logic {
		namespace Events {

			class GenericEventData : public IEventData
			{
				const uint32 m_Timestamp;
			public:
				GenericEventData(const uint32 timestamp) : m_Timestamp(timestamp) {}
				virtual ~GenericEventData() {}

				virtual const EventType GetEventType() const = VIRTUAL;
				virtual void Serialize(ubyte8* bytes) const = VIRTUAL;
				virtual IEventDataPtr Copy() const = VIRTUAL;
				virtual const char* GetName() const = VIRTUAL;

				virtual const uint32 GetTimeStamp() const { return m_Timestamp; }
			};

		}
	}
}