#pragma once
#include "stdafx.h"
#include <memory>

#include "FastDelegate.h"

namespace GameEngine {
	namespace Logic {
		namespace Events {

			interface IEventData;
			
			typedef uint32 EventType;
			typedef std::shared_ptr<IEventData> IEventDataPtr;
			typedef fastdelegate::FastDelegate1<IEventDataPtr> EventListenerDelegate;

			interface DllExport IEventData
			{
				virtual const EventType GetEventType() const = VIRTUAL;
				virtual const uint32 GetTimeStamp() const = VIRTUAL;
				virtual void Serialize(ubyte8* bytes) const = VIRTUAL;
				virtual IEventDataPtr Copy() const = VIRTUAL;
				virtual const char* GetName() const = VIRTUAL;
			};

			
		}
	}
}