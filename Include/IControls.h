#pragma once
#include "stdafx.h"

#include "GMath.h"

using namespace GUtils::GMath;

namespace GameEngine {
	namespace Application {
		namespace Control {

			interface DllExport IKeyboardHandler
			{
				virtual BOOL OnKeyDown(const uint32 code) = VIRTUAL;
				virtual BOOL OnKeyUp(const uint32 code) = VIRTUAL;
			};

			interface DllExport IPointerHandler
			{
				virtual BOOL OnPointerMove(const Point2D<uint32>& position) = VIRTUAL;
				virtual BOOL OnPointerButtonDown(const Point2D<uint32>& position, const ubyte8 buttonId) = VIRTUAL;
				virtual BOOL OnPointerButtonUp(const Point2D<uint32>& position, const ubyte8 buttonId) = VIRTUAL;
				virtual ushort16 GetPointerRadius() = VIRTUAL;
			};

			interface DllExport IJoystickHandler
			{
				virtual BOOL OnButtonDown(const ubyte8 buttonId, const uint32 pressure) = VIRTUAL;
				virtual BOOL OnButtonUp(const ubyte8 buttonId) = VIRTUAL;
				virtual BOOL OnStickMove(const Point2D<float>& position) = VIRTUAL;
			};

			interface DllExport IGamepadHandler
			{
				virtual BOOL OnTriggerDown(const ubyte8 triggerId, const uint32 pressure) = VIRTUAL;
				virtual BOOL OnTriggerUp(const ubyte8 triggerId) = VIRTUAL;
				virtual BOOL OnButtonDown(const ubyte8 buttonId, const uint32 pressure) = VIRTUAL;
				virtual BOOL OnButtonUp(const ubyte8 buttonId) = VIRTUAL;
			};

		}
	}
}