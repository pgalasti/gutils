#pragma once

#include"stdafx.h"
#include<string>
#include<list>
#include<vector>

namespace GUtils {
	namespace IO {

		class DllExport FileLoader
		{
		public:
			FileLoader();
			FileLoader(const char* pszFilePath);
			FileLoader(const std::string& strFilePath);
			~FileLoader();

			BOOL Open();
			BOOL Open(const char* pszFilePath);
			BOOL Open(const std::string& strFilePath);
			BOOL Close();
			BOOL GetFileLines(std::list<std::string>& fileLines);
			BOOL GetRawData(std::vector<sbyte8>& buffer);
			BOOL GetFileSize(uint32& sizeInBytes);
			
		protected:

			std::string m_FilePath;
			FILE* m_pFile;
			bool m_bFileLoaded;
		};
	}
}
