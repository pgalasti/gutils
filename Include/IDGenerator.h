#pragma once
#include "stdafx.h"

#include <mutex>
#include <queue>

namespace GUtils {
	namespace Concurrency {

		class DllExport IDGenerator
		{
		public:
			IDGenerator(const uint32 firstValue)  { this->Reset(firstValue); }

			int GetNextId();
			int GetLatestId();
			void Reset(const uint32 firstValue);
			int GetFirstValue();
			void Release(const uint32 id);

		private:
			std::mutex m_Mutex;
			uint32 m_LastId;
			uint32 m_FirstValue;

			std::queue<uint32> m_IdQueue;
		};

	}
}