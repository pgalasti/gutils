#pragma once
#include "stdafx.h" 
#include <ctime>
#define CMATH_N 624
#define CMATH_M 397
#define CMATH_MATRIX_A 0x9908b0df   /* constant vector a */
#define CMATH_UPPER_MASK 0x80000000 /* most significant w-r bits */
#define CMATH_LOWER_MASK 0x7fffffff /* least significant r bits */

#define CMATH_TEMPERING_MASK_B 0x9d2c5680
#define CMATH_TEMPERING_MASK_C 0xefc60000
#define CMATH_TEMPERING_SHIFT_U(y)  (y >> 11)
#define CMATH_TEMPERING_SHIFT_S(y)  (y << 7)
#define CMATH_TEMPERING_SHIFT_T(y)  (y << 15)
#define CMATH_TEMPERING_SHIFT_L(y)  (y >> 18)

namespace GUtils {
	namespace Math {
		
		// Class grabbed from Game Coding Complete 4th Edition. http://www.mcshaffry.com/GameCode/
		// Generation method known as 'Mersenne Twister' pseudorandom number generator.
		class DllExport RandomGenerator
		{
		private:
			// DATA
			uint32		rseed;
			uint32		rseed_sp;
			unsigned long mt[CMATH_N]; /* the array for the state vector  */
			int mti; /* mti==N+1 means mt[N] is not initialized */

			// FUNCTIONS
		public:
			RandomGenerator(void);	

			uint32	Random(uint32 n);
			float			Random( );
			void			SetRandomSeed(uint32 n);
			uint32	GetRandomSeed(void);
			void			Randomize(void);
		};
	}

}

