#pragma once
#include "stdafx.h"
#include <string>
#include "Logging.h"
#include "Engine.h"
#include <memory>
#include "PollInput.h"
#include "DirectXMathConversions.h"
#include "ResourceDirectories.h"

using namespace GUtils::Graphics;

namespace GUtils {
	namespace System {

		class DllExport GameWindow
		{
		public:
			GameWindow();
			virtual ~GameWindow();


			virtual BOOL Initialize(IGraphicEngine* pGraphicEngine, const EngineOptions& options);
			virtual BOOL CleanUp();
			virtual BOOL Execute();
			virtual BOOL UpdateGameState(float deltaTime) = VIRTUAL;

			void SetWindowTitle(const char* pszName)		{ m_WindowTitle = pszName; }
			void SetApplicationName(const char* pszName)	{ m_ApplicationName = pszName; }
			void SetFullScreen(bool fullScreen = true)		{ m_bFullScreen = fullScreen; }

			virtual LRESULT CALLBACK MessageHandler(HWND, UINT, WPARAM, LPARAM);

		protected:
			HINSTANCE m_hInstance;
			HWND m_hWND;
			IGraphicEngine* m_pGraphicsEngine;

			bool m_bFullScreen;
			std::string m_ApplicationName;
			std::string m_WindowTitle;
			GUtils::Time::GameTimer m_Timer;
			ResourceDirectories m_ResourceDirectories;
			virtual BOOL InitializeSystemWindow(uint32 screenWidth, uint32 screenHeight);
			BOOL Render(const float dt);
			PollInput m_InputPoll;
		private:
			MEMBER_LOGGING_ENABLED
		};
		static GameWindow* pGameWindowReference;

	}
}



#ifdef DEPRECATED_WINDOW_CLASS
		class DllExport SystemWindow
		{
		public:
			SystemWindow();
			~SystemWindow();

			virtual BOOL Initialize();
			virtual BOOL Shutdown();
			virtual void Run();
			virtual BOOL Frame();
			virtual LRESULT CALLBACK MessageHandler(HWND, UINT, WPARAM, LPARAM);

			void SetWindowTitle(const char* pszName)		{ m_WindowTitle = pszName; }
			void SetApplicationName(const char* pszName)	{ m_ApplicationName = pszName; }
			void SetFullScreen(bool fullScreen = true)		{ m_bFullScreen = fullScreen; }

			SimpleEngine* m_pEngine;

		protected:
			HINSTANCE m_hInstance;
			HWND m_hWND;
			BOOL InitializeWindow(unsigned int& screenWidth, unsigned int& screenHeight);
			void ShutdownWindows();

		private:
			std::string m_ApplicationName;
			std::string m_WindowTitle;
			bool m_bFullScreen;

			GUtils::Debug::Logger* m_pLog;
		};
		static SystemWindow* ApplicationHandle;


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PSTR pCmdLine, int iCmdShow)
{
	Framework::System::SystemWindow* pSystemWindow = new Framework::System::SystemWindow();

	if (pSystemWindow == nullptr)
	{
		SAFE_DELETE(pSystemWindow);
		return SYSTEM_ERROR;
	}

	//if (!pSystemWindow->Initialize())
	//{
	//	return SYSTEM_ERROR;
	//	SAFE_DELETE(pSystemWindow);
	//}

	//pSystemWindow->Run();
	//pSystemWindow->Shutdown();

	SAFE_DELETE(pSystemWindow);

	return SYSTEM_EXIT;
}
#endif