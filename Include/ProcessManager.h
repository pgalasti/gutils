#pragma once
#include "stdafx.h"

#include "Process.h"

#include <list>

namespace GameEngine {
	namespace Logic {
		namespace Process {

			typedef std::list<StrongProcessPtr> ProcessList;

			class DllExport ProcessManager
			{
				ProcessList m_ProcessList;

			public:
				ProcessManager() {}
				~ProcessManager();

				uint32 UpdateProcesses(const uint32 deltaTime);
				WeakProcessPtr AttachProcess(StrongProcessPtr pProcess);

				void AbortAllProcesses(const bool immediate);

				uint32 GetProcessCount() const { return m_ProcessList.size(); }

			private:
				void ClearAllProcesses() { m_ProcessList.clear(); }

			};

		}
	}
}