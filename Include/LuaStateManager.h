#pragma once
#include "stdafx.h"

#include<string>

#include "IScriptManager.h"

#include "LuaPlus.h"

namespace LuaScripting {

	using namespace GUtils::Scripting;

	class DllExport LuaStateManager : public IScriptManager
	{
		LuaPlus::LuaState* m_pLuaState;
		std::string m_LastError;

	public:
		LuaStateManager();
		virtual ~LuaStateManager();

		// Interface
		virtual BOOL Initialize() override;
		virtual BOOL ExecuteFile(const char* pszPath) override;
		virtual BOOL ExecuteLine(const char* pszLine) override;
		virtual BOOL Destroy() override;

		// I don't like exposing these
		LuaPlus::LuaObject GetGlobalVars() const { return m_pLuaState->GetGlobals(); }
		LuaPlus::LuaState* GetLuaState() const { return m_pLuaState; }

		std::string GetLastError() const { return m_LastError; }

	private:
		void SetError(const int32 errorNumber);
		void ClearStack();
	};

}