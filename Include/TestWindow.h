#pragma once
#include "stdafx.h"
#include "Window.h"
#include "IControls.h"
#include "ProcessManager.h"
#include "EventManager.h"
#include "GenericEventData.h"

using namespace GUtils::System;
using namespace GameEngine::Application::Control;
using namespace GameEngine::Logic::Process;
using namespace GameEngine::Logic::Events;

namespace GameEngine {
	namespace Application {
		namespace Testing {

			class MoveCameraEvent : public GenericEventData
			{
				GUtils::Graphics::Camera::Camera m_Camera;
				GUtils::Graphics::IGraphicEngine* m_pEngine;
				float m_DeltaTime;
				

			public:
				MoveCameraEvent() : GenericEventData(0) {}
				MoveCameraEvent(const uint32 timestamp, GUtils::Graphics::IGraphicEngine* pEngine, const float deltaTime)
					: GenericEventData(timestamp)
				{
					m_pEngine = pEngine;
					m_DeltaTime = deltaTime;
				}
				~MoveCameraEvent() {}

				GUtils::Graphics::Camera::Camera GetCamera() { return m_Camera; }
				const float GetDeltaTime() const { return m_DeltaTime; }

				GUtils::Graphics::IGraphicEngine* GetEnginePtr() { return m_pEngine; }

				virtual const EventType GetEventType() const override { return 12345; }
				virtual void Serialize(ubyte8* bytes) const override {}
				virtual IEventDataPtr Copy() const override { return IEventDataPtr(new MoveCameraEvent(this->GetTimeStamp(), m_pEngine, m_DeltaTime)); }
				virtual const char* GetName() const override { return "MoveCameraEvent"; }


				enum Direction {
					Forward,
					Backward,
					Left,
					Right,
				};
				void SetDirection(const Direction direction) { m_Direction = direction; }
				Direction GetDirection() const { return m_Direction; }

			private:
				Direction m_Direction;
			};

			class TestKeyboardControls : public IKeyboardHandler
			{
				PollInput m_PollInput;

			public:
				TestKeyboardControls()
				{
					m_PollInput.ClearState();
				}

				virtual BOOL OnKeyDown(const uint32 code) override
				{
					m_PollInput.SetCharacter(code);
					return TRUE;
				}
				virtual BOOL OnKeyUp(const uint32 code) override
				{
					m_PollInput.UnsetCharacter(code);
					return TRUE;
				}

				const PollInput& GetInputPoll() const { return m_PollInput; }
				void ClearState() { m_PollInput.ClearState(); }
			};

			class DllExport TestWindow : public GameWindow
			{
			public:
				TestWindow();
				virtual ~TestWindow();

				virtual BOOL Initialize(IGraphicEngine* pGraphicEngine, const EngineOptions& options) override;
				virtual BOOL CleanUp() override;
				virtual BOOL Execute() override;
				virtual BOOL UpdateGameState(float deltaTime) override;

				void SetCamera(IEventDataPtr pEventData);
				void SetDebugEvent(IEventDataPtr pEventData);
				virtual LRESULT CALLBACK MessageHandler(HWND, UINT, WPARAM, LPARAM) override;

			protected:
				virtual BOOL InitializeSystemWindow(uint32 screenWidth, uint32 screenHeight) override;

				ProcessManager* m_pProcessManager;
				EventManager* m_pEventManager;

				TestKeyboardControls* m_pKeyboardController;
				GUtils::Graphics::Camera::Camera m_Camera;

			};

		}
	}
}
