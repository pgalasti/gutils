#pragma once
#include "stdafx.h"


namespace GUtils {
	namespace System {

		struct DllExport InputState
		{
		public:
			unsigned long lCharacterKeys;
			unsigned long lSymbolKeys;
			unsigned long lSpecialKeys;
			unsigned long lFunctionKeys;
			unsigned long lNumberKeys;
			unsigned long lMouseButtons;
			int m_CursorPosition[2];

			// Characters
			static const unsigned long KEY_DOWN_A = 0x00000001;
			static const unsigned long KEY_DOWN_B = 0x00000002;
			static const unsigned long KEY_DOWN_C = 0x00000004;
			static const unsigned long KEY_DOWN_D = 0x00000008;
			static const unsigned long KEY_DOWN_E = 0x00000010;
			static const unsigned long KEY_DOWN_F = 0x00000020;
			static const unsigned long KEY_DOWN_G = 0x00000040;
			static const unsigned long KEY_DOWN_H = 0x00000080;
			static const unsigned long KEY_DOWN_I = 0x00000100;
			static const unsigned long KEY_DOWN_J = 0x00000200;
			static const unsigned long KEY_DOWN_K = 0x00000400;
			static const unsigned long KEY_DOWN_L = 0x00000800;
			static const unsigned long KEY_DOWN_M = 0x00001000;
			static const unsigned long KEY_DOWN_N = 0x00002000;
			static const unsigned long KEY_DOWN_O = 0x00004000;
			static const unsigned long KEY_DOWN_P = 0x00008000;
			static const unsigned long KEY_DOWN_Q = 0x00010000;
			static const unsigned long KEY_DOWN_R = 0x00020000;
			static const unsigned long KEY_DOWN_S = 0x00040000;
			static const unsigned long KEY_DOWN_T = 0x00080000;
			static const unsigned long KEY_DOWN_U = 0x00100000;
			static const unsigned long KEY_DOWN_V = 0x00200000;
			static const unsigned long KEY_DOWN_W = 0x00400000;
			static const unsigned long KEY_DOWN_X = 0x00800000;
			static const unsigned long KEY_DOWN_Y = 0x01000000;
			static const unsigned long KEY_DOWN_Z = 0x02000000;
			static const unsigned long KEY_DOWN_SPACE = 0x04000000;

			// SYMBOL
			static const unsigned long SYMBOL_KEY_DOWN_SEMICOLON		= 0x00000001;
			static const unsigned long SYMBOL_KEY_DOWN_SINGLE_QUOTE		= 0x00000002;
			static const unsigned long SYMBOL_KEY_DOWN_FRONT_SLASH		= 0x00000004;
			static const unsigned long SYMBOL_KEY_DOWN_PERIOD			= 0x00000008;
			static const unsigned long SYMBOL_KEY_DOWN_COMMA			= 0x00000010;
			static const unsigned long SYMBOL_KEY_DOWN_LEFT_BRACKET		= 0x00000020;
			static const unsigned long SYMBOL_KEY_DOWN_RIGHT_BRACKET	= 0x00000040;
			static const unsigned long SYMBOL_KEY_DOWN_BACK_SLASH		= 0x00000080;
			static const unsigned long SYMBOL_KEY_DOWN_TILDE			= 0x00000100;
			static const unsigned long SYMBOL_KEY_DOWN_HYPHEN			= 0x00000200;
			static const unsigned long SYMBOL_KEY_DOWN_ASTRICK			= 0x00000400;
			static const unsigned long SYMBOL_KEY_DOWN_PLUS				= 0x00000800;
			static const unsigned long SYMBOL_KEY_DOWN_EQUAL			= 0x00001000;
			static const unsigned long SYMBOL_KEY_DOWN_UNUSED1			= 0x00002000;
			static const unsigned long SYMBOL_KEY_DOWN_UNUSED2			= 0x00004000;
			static const unsigned long SYMBOL_KEY_DOWN_UNUSED3			= 0x00008000;
			static const unsigned long SYMBOL_KEY_DOWN_UNUSED4			= 0x00010000;
			static const unsigned long SYMBOL_KEY_DOWN_UNUSED5			= 0x00020000;
			static const unsigned long SYMBOL_KEY_DOWN_UNUSED6			= 0x00040000;
			static const unsigned long SYMBOL_KEY_DOWN_UNUSED7			= 0x00080000;
			static const unsigned long SYMBOL_KEY_DOWN_UNUSED8			= 0x00100000;
			static const unsigned long SYMBOL_KEY_DOWN_UNUSED9			= 0x00200000;
			static const unsigned long SYMBOL_KEY_DOWN_UNUSED10			= 0x00400000;
			static const unsigned long SYMBOL_KEY_DOWN_UNUSED11			= 0x00800000;
			static const unsigned long SYMBOL_KEY_DOWN_UNUSED12			= 0x01000000;
			static const unsigned long SYMBOL_KEY_DOWN_UNUSED13			= 0x02000000;
			static const unsigned long SYMBOL_KEY_DOWN_UNUSED14			= 0x04000000;

			// Special Characters
			static const unsigned long SPECIAL_KEY_DOWN_CTRL		= 0x00000001;
			static const unsigned long SPECIAL_KEY_DOWN_SHIFT		= 0x00000002;
			static const unsigned long SPECIAL_KEY_DOWN_CAPS_LOCK	= 0x00000004;
			static const unsigned long SPECIAL_KEY_DOWN_TAB			= 0x00000008;
			static const unsigned long SPECIAL_KEY_DOWN_WINDOWS		= 0x00000010;
			static const unsigned long SPECIAL_KEY_DOWN_ALT			= 0x00000020;
			static const unsigned long SPECIAL_KEY_DOWN_ENTER		= 0x00000040;
			static const unsigned long SPECIAL_KEY_DOWN_ESCAPE		= 0x00000080;
			static const unsigned long SPECIAL_KEY_DOWN_BACKSPACE	= 0x00000100;
			//static const unsigned long SPECIAL_KEY_DOWN_EQUAL		= 0x00000200;
			//static const unsigned long SPECIAL_KEY_DOWN_HYPHEN		= 0x00000400;
			static const unsigned long SPECIAL_KEY_DOWN_INSERT		= 0x00000800;
			static const unsigned long SPECIAL_KEY_DOWN_HOME		= 0x00001000;
			static const unsigned long SPECIAL_KEY_DOWN_PAGE_UP		= 0x00002000;
			static const unsigned long SPECIAL_KEY_DOWN_PAGE_DOWN	= 0x00004000;
			static const unsigned long SPECIAL_KEY_DOWN_DELETE		= 0x00008000;
			static const unsigned long SPECIAL_KEY_DOWN_END			= 0x00010000;
			static const unsigned long SPECIAL_KEY_DOWN_NUMBER_LOCK = 0x00020000;
			static const unsigned long SPECIAL_KEY_DOWN_PAUSE_BREAK = 0x00040000;
			static const unsigned long SPECIAL_KEY_DOWN_SCREEN_LOCK = 0x00080000;
			static const unsigned long SPECIAL_KEY_DOWN_DOWN_ARROW  = 0x00100000;
			static const unsigned long SPECIAL_KEY_DOWN_UP_ARROW	= 0x00200000;
			static const unsigned long SPECIAL_KEY_DOWN_LEFT_ARROW	= 0x00400000;
			static const unsigned long SPECIAL_KEY_DOWN_RIGHT_ARROW = 0x00800000;
			// Room for unused

			// Function keys
			static const unsigned long FUNCTION_KEY_DOWN_1			= 0x00000001;
			static const unsigned long FUNCTION_KEY_DOWN_2			= 0x00000002;
			static const unsigned long FUNCTION_KEY_DOWN_3			= 0x00000004;
			static const unsigned long FUNCTION_KEY_DOWN_4			= 0x00000008;
			static const unsigned long FUNCTION_KEY_DOWN_5			= 0x00000010;
			static const unsigned long FUNCTION_KEY_DOWN_6			= 0x00000020;
			static const unsigned long FUNCTION_KEY_DOWN_7			= 0x00000040;
			static const unsigned long FUNCTION_KEY_DOWN_8			= 0x00000080;
			static const unsigned long FUNCTION_KEY_DOWN_9			= 0x00000100;
			static const unsigned long FUNCTION_KEY_DOWN_10			= 0x00000200;
			static const unsigned long FUNCTION_KEY_DOWN_11			= 0x00000400;
			static const unsigned long FUNCTION_KEY_DOWN_12			= 0x00000800;
			// Room for unused

			// Number Keys
			static const unsigned long NUMBER_KEY_DOWN_0		= 0x00000001;
			static const unsigned long NUMBER_KEY_DOWN_1		= 0x00000002;
			static const unsigned long NUMBER_KEY_DOWN_2		= 0x00000004;
			static const unsigned long NUMBER_KEY_DOWN_3		= 0x00000008;
			static const unsigned long NUMBER_KEY_DOWN_4		= 0x00000010;
			static const unsigned long NUMBER_KEY_DOWN_5		= 0x00000020;
			static const unsigned long NUMBER_KEY_DOWN_6		= 0x00000040;
			static const unsigned long NUMBER_KEY_DOWN_7		= 0x00000080;
			static const unsigned long NUMBER_KEY_DOWN_8		= 0x00000100;
			static const unsigned long NUMBER_KEY_DOWN_9		= 0x00000200;
			static const unsigned long NUMBERPAD_KEY_DOWN_0		= 0x00000400;
			static const unsigned long NUMBERPAD_KEY_DOWN_1		= 0x00000800;
			static const unsigned long NUMBERPAD_KEY_DOWN_2		= 0x00001000;
			static const unsigned long NUMBERPAD_KEY_DOWN_3		= 0x00002000;
			static const unsigned long NUMBERPAD_KEY_DOWN_4		= 0x00004000;
			static const unsigned long NUMBERPAD_KEY_DOWN_5		= 0x00008000;
			static const unsigned long NUMBERPAD_KEY_DOWN_6		= 0x00010000;
			static const unsigned long NUMBERPAD_KEY_DOWN_7		= 0x00020000;
			static const unsigned long NUMBERPAD_KEY_DOWN_8		= 0x00040000;
			static const unsigned long NUMBERPAD_KEY_DOWN_9		= 0x00080000;

			// MOUSE BUTTONS
			static const unsigned long MOUSE_BUTTON_LEFT_DOWN = 0x00000001;
			static const unsigned long MOUSE_BUTTON_RIGHT_DOWN = 0x00000002;
			static const unsigned long MOUSE_BUTTON_MIDDLE_DOWN = 0x00000004;
		};

		enum DllExport FunctionKey
		{
			F1 = 0,
			F2 = 1,
			F3 = 2,
			F4 = 3,
			F5 = 4,
			F6 = 5,
			F7 = 6,
			F8 = 7,
			F9 = 8,
			F10 = 9,
			F11 = 10,
			F12 = 11
		};

		static const unsigned long SPECIAL_KEY_DOWN_CTRL = 0x00000001;
		static const unsigned long SPECIAL_KEY_DOWN_SHIFT = 0x00000002;
		static const unsigned long SPECIAL_KEY_DOWN_CAPS_LOCK = 0x00000004;
		static const unsigned long SPECIAL_KEY_DOWN_TAB = 0x00000008;
		static const unsigned long SPECIAL_KEY_DOWN_WINDOWS = 0x00000010;
		static const unsigned long SPECIAL_KEY_DOWN_ALT = 0x00000020;
		static const unsigned long SPECIAL_KEY_DOWN_ENTER = 0x00000040;
		static const unsigned long SPECIAL_KEY_DOWN_ESCAPE = 0x00000080;
		static const unsigned long SPECIAL_KEY_DOWN_BACKSPACE = 0x00000100;
		//static const unsigned long SPECIAL_KEY_DOWN_EQUAL = 0x00000200;
		//static const unsigned long SPECIAL_KEY_DOWN_HYPHEN = 0x00000400;
		static const unsigned long SPECIAL_KEY_DOWN_INSERT = 0x00000800;
		static const unsigned long SPECIAL_KEY_DOWN_HOME = 0x00001000;
		static const unsigned long SPECIAL_KEY_DOWN_PAGE_UP = 0x00002000;
		static const unsigned long SPECIAL_KEY_DOWN_PAGE_DOWN = 0x00004000;
		static const unsigned long SPECIAL_KEY_DOWN_DELETE = 0x00008000;
		static const unsigned long SPECIAL_KEY_DOWN_END = 0x00010000;
		static const unsigned long SPECIAL_KEY_DOWN_NUMBER_LOCK = 0x00020000;
		static const unsigned long SPECIAL_KEY_DOWN_PAUSE_BREAK = 0x00040000;
		static const unsigned long SPECIAL_KEY_DOWN_SCREEN_LOCK = 0x00080000;
		static const unsigned long SPECIAL_KEY_DOWN_DOWN_ARROW = 0x00100000;
		static const unsigned long SPECIAL_KEY_DOWN_UP_ARROW = 0x00200000;
		static const unsigned long SPECIAL_KEY_DOWN_LEFT_ARROW = 0x00400000;
		static const unsigned long SPECIAL_KEY_DOWN_RIGHT_ARROW = 0x00800000;

		enum DllExport SpecialKey
		{
			Control,
			Shift,
			CapsLock,
			Tab,
			Windows,
			Alt,
			Enter,
			Escape,
			Backspace,
			Insert,
			Home,
			PageUp,
			PageDown,
			Delete,
			End,
			NumberLock,
			PauseBreak,
			ScreenLock,
			DownArrow,
			UpArrow,
			LeftArrow,
			RightArrow,
		};

		class DllExport PollInput
		{
		public:
			PollInput(){ ClearState(); }
			~PollInput(){}
			void ClearState() { ZeroMemory(&m_InputState, sizeof InputState); }

			InputState GetInputState() { return m_InputState; }

			void SetCharacter(const char character);
			void UnsetCharacter(const char character);

			void SetFunctionKey(FunctionKey key);
			void UnsetFunctionKey(FunctionKey key);

			enum NumberOrigin
			{
				KeyPad,
				Row,
			};

			void SetNumberCharacter(ubyte8 number, NumberOrigin origin);
			void UnsetNumberCharacter(ubyte8 number, NumberOrigin origin);

			void SetSpecialKey(SpecialKey key);
			void UnsetSpecialKey(SpecialKey key);

			void SetCursorPosition(const int x, const int y) { m_InputState.m_CursorPosition[0] = x; m_InputState.m_CursorPosition[1] = y; }

			enum MouseButton
			{
				Left,
				Middle,
				Right,
			};

			void SetMouseButtonDown(MouseButton button);
			void UnsetMouseButtonDown(MouseButton button);

#pragma warning(disable : 4800)
			// Character keys ================================================================================================
			bool IsAKeyDown() const { return m_InputState.lCharacterKeys & InputState::KEY_DOWN_A; }
			bool IsBKeyDown() const { return m_InputState.lCharacterKeys & InputState::KEY_DOWN_B; }
			bool IsCKeyDown() const { return m_InputState.lCharacterKeys & InputState::KEY_DOWN_C; }
			bool IsDKeyDown() const { return m_InputState.lCharacterKeys & InputState::KEY_DOWN_D; }
			bool IsEKeyDown() const { return m_InputState.lCharacterKeys & InputState::KEY_DOWN_E; }
			bool IsFKeyDown() const { return m_InputState.lCharacterKeys & InputState::KEY_DOWN_F; }
			bool IsGKeyDown() const { return m_InputState.lCharacterKeys & InputState::KEY_DOWN_G; }
			bool IsHKeyDown() const { return m_InputState.lCharacterKeys & InputState::KEY_DOWN_H; }
			bool IsIKeyDown() const { return m_InputState.lCharacterKeys & InputState::KEY_DOWN_I; }
			bool IsJKeyDown() const { return m_InputState.lCharacterKeys & InputState::KEY_DOWN_J; }
			bool IsKKeyDown() const { return m_InputState.lCharacterKeys & InputState::KEY_DOWN_K; }
			bool IsLKeyDown() const { return m_InputState.lCharacterKeys & InputState::KEY_DOWN_L; }
			bool IsMKeyDown() const { return m_InputState.lCharacterKeys & InputState::KEY_DOWN_M; }
			bool IsNKeyDown() const { return m_InputState.lCharacterKeys & InputState::KEY_DOWN_N; }
			bool IsOKeyDown() const { return m_InputState.lCharacterKeys & InputState::KEY_DOWN_O; }
			bool IsPKeyDown() const { return m_InputState.lCharacterKeys & InputState::KEY_DOWN_P; }
			bool IsQKeyDown() const { return m_InputState.lCharacterKeys & InputState::KEY_DOWN_Q; }
			bool IsRKeyDown() const { return m_InputState.lCharacterKeys & InputState::KEY_DOWN_R; }
			bool IsSKeyDown() const { return m_InputState.lCharacterKeys & InputState::KEY_DOWN_S; }
			bool IsTKeyDown() const { return m_InputState.lCharacterKeys & InputState::KEY_DOWN_T; }
			bool IsUKeyDown() const { return m_InputState.lCharacterKeys & InputState::KEY_DOWN_U; }
			bool IsVKeyDown() const { return m_InputState.lCharacterKeys & InputState::KEY_DOWN_V; }
			bool IsWKeyDown() const { return m_InputState.lCharacterKeys & InputState::KEY_DOWN_W; }
			bool IsXKeyDown() const { return m_InputState.lCharacterKeys & InputState::KEY_DOWN_X; }
			bool IsYKeyDown() const { return m_InputState.lCharacterKeys & InputState::KEY_DOWN_Y; }
			bool IsZKeyDown() const { return m_InputState.lCharacterKeys & InputState::KEY_DOWN_Z; }

			// Number Keys ================================================================================================
			// Any number 
			bool Is0KeyDown() const { return m_InputState.lNumberKeys & InputState::NUMBERPAD_KEY_DOWN_0 | m_InputState.lNumberKeys & InputState::NUMBER_KEY_DOWN_0; }
			bool Is1KeyDown() const { return m_InputState.lNumberKeys & InputState::NUMBERPAD_KEY_DOWN_1 | m_InputState.lNumberKeys & InputState::NUMBER_KEY_DOWN_1; }
			bool Is2KeyDown() const { return m_InputState.lNumberKeys & InputState::NUMBERPAD_KEY_DOWN_2 | m_InputState.lNumberKeys & InputState::NUMBER_KEY_DOWN_2; }
			bool Is3KeyDown() const { return m_InputState.lNumberKeys & InputState::NUMBERPAD_KEY_DOWN_3 | m_InputState.lNumberKeys & InputState::NUMBER_KEY_DOWN_3; }
			bool Is4KeyDown() const { return m_InputState.lNumberKeys & InputState::NUMBERPAD_KEY_DOWN_4 | m_InputState.lNumberKeys & InputState::NUMBER_KEY_DOWN_4; }
			bool Is5KeyDown() const { return m_InputState.lNumberKeys & InputState::NUMBERPAD_KEY_DOWN_5 | m_InputState.lNumberKeys & InputState::NUMBER_KEY_DOWN_5; }
			bool Is6KeyDown() const { return m_InputState.lNumberKeys & InputState::NUMBERPAD_KEY_DOWN_6 | m_InputState.lNumberKeys & InputState::NUMBER_KEY_DOWN_6; }
			bool Is7KeyDown() const { return m_InputState.lNumberKeys & InputState::NUMBERPAD_KEY_DOWN_7 | m_InputState.lNumberKeys & InputState::NUMBER_KEY_DOWN_7; }
			bool Is8KeyDown() const { return m_InputState.lNumberKeys & InputState::NUMBERPAD_KEY_DOWN_8 | m_InputState.lNumberKeys & InputState::NUMBER_KEY_DOWN_8; }
			bool Is9KeyDown() const { return m_InputState.lNumberKeys & InputState::NUMBERPAD_KEY_DOWN_9 | m_InputState.lNumberKeys & InputState::NUMBER_KEY_DOWN_9; }
			// Row Number
			bool Is0RowKeyDown() const { return m_InputState.lNumberKeys & InputState::NUMBER_KEY_DOWN_0; }
			bool Is1RowKeyDown() const { return m_InputState.lNumberKeys & InputState::NUMBER_KEY_DOWN_1; }
			bool Is2RowKeyDown() const { return m_InputState.lNumberKeys & InputState::NUMBER_KEY_DOWN_2; }
			bool Is3RowKeyDown() const { return m_InputState.lNumberKeys & InputState::NUMBER_KEY_DOWN_3; }
			bool Is4RowKeyDown() const { return m_InputState.lNumberKeys & InputState::NUMBER_KEY_DOWN_4; }
			bool Is5RowKeyDown() const { return m_InputState.lNumberKeys & InputState::NUMBER_KEY_DOWN_5; }
			bool Is6RowKeyDown() const { return m_InputState.lNumberKeys & InputState::NUMBER_KEY_DOWN_6; }
			bool Is7RowKeyDown() const { return m_InputState.lNumberKeys & InputState::NUMBER_KEY_DOWN_7; }
			bool Is8RowKeyDown() const { return m_InputState.lNumberKeys & InputState::NUMBER_KEY_DOWN_8; }
			bool Is9RowKeyDown() const { return m_InputState.lNumberKeys & InputState::NUMBER_KEY_DOWN_9; }
			// Key Pad
			bool Is0PadKeyDown() const { return m_InputState.lNumberKeys & InputState::NUMBERPAD_KEY_DOWN_0; }
			bool Is1PadKeyDown() const { return m_InputState.lNumberKeys & InputState::NUMBERPAD_KEY_DOWN_1; }
			bool Is2PadKeyDown() const { return m_InputState.lNumberKeys & InputState::NUMBERPAD_KEY_DOWN_2; }
			bool Is3PadKeyDown() const { return m_InputState.lNumberKeys & InputState::NUMBERPAD_KEY_DOWN_3; }
			bool Is4PadKeyDown() const { return m_InputState.lNumberKeys & InputState::NUMBERPAD_KEY_DOWN_4; }
			bool Is5PadKeyDown() const { return m_InputState.lNumberKeys & InputState::NUMBERPAD_KEY_DOWN_5; }
			bool Is6PadKeyDown() const { return m_InputState.lNumberKeys & InputState::NUMBERPAD_KEY_DOWN_6; }
			bool Is7PadKeyDown() const { return m_InputState.lNumberKeys & InputState::NUMBERPAD_KEY_DOWN_7; }
			bool Is8PadKeyDown() const { return m_InputState.lNumberKeys & InputState::NUMBERPAD_KEY_DOWN_8; }
			bool Is9PadKeyDown() const { return m_InputState.lNumberKeys & InputState::NUMBERPAD_KEY_DOWN_9; }

			// Function Keys ================================================================================================
			bool IsF1KeyDown() const { return m_InputState.lFunctionKeys & InputState::FUNCTION_KEY_DOWN_1; }
			bool IsF2KeyDown() const { return m_InputState.lFunctionKeys & InputState::FUNCTION_KEY_DOWN_1; }
			bool IsF3KeyDown() const { return m_InputState.lFunctionKeys & InputState::FUNCTION_KEY_DOWN_1; }
			bool IsF4KeyDown() const { return m_InputState.lFunctionKeys & InputState::FUNCTION_KEY_DOWN_1; }
			bool IsF5KeyDown() const { return m_InputState.lFunctionKeys & InputState::FUNCTION_KEY_DOWN_1; }
			bool IsF6KeyDown() const { return m_InputState.lFunctionKeys & InputState::FUNCTION_KEY_DOWN_1; }
			bool IsF7KeyDown() const { return m_InputState.lFunctionKeys & InputState::FUNCTION_KEY_DOWN_1; }
			bool IsF8KeyDown() const { return m_InputState.lFunctionKeys & InputState::FUNCTION_KEY_DOWN_1; }
			bool IsF9KeyDown() const { return m_InputState.lFunctionKeys & InputState::FUNCTION_KEY_DOWN_1; }
			bool IsF10KeyDown() const { return m_InputState.lFunctionKeys & InputState::FUNCTION_KEY_DOWN_1;}
			bool IsF11KeyDown() const { return m_InputState.lFunctionKeys & InputState::FUNCTION_KEY_DOWN_1;}
			bool IsF12KeyDown() const { return m_InputState.lFunctionKeys & InputState::FUNCTION_KEY_DOWN_1;}

			// Speceial Keys ================================================================================================
			bool IsShiftKeyDown() const			{ return m_InputState.lSpecialKeys & InputState::SPECIAL_KEY_DOWN_SHIFT; }
			bool IsCapsLockDown() const			{ return m_InputState.lSpecialKeys & InputState::SPECIAL_KEY_DOWN_CAPS_LOCK; }
			bool IsTabKeyDown() const			{ return m_InputState.lSpecialKeys & InputState::SPECIAL_KEY_DOWN_TAB; }
			bool IsWindowsKeyDown() const		{ return m_InputState.lSpecialKeys & InputState::SPECIAL_KEY_DOWN_WINDOWS; }
			bool IsAltKeyDown() const			{ return m_InputState.lSpecialKeys & InputState::SPECIAL_KEY_DOWN_ALT; }
			bool IsEnterKeyDown() const			{ return m_InputState.lSpecialKeys & InputState::SPECIAL_KEY_DOWN_ENTER; }
			bool IsEscapeKeyDown() const		{ return m_InputState.lSpecialKeys & InputState::SPECIAL_KEY_DOWN_ESCAPE; }
			bool IsBackspaceKeyDown() const		{ return m_InputState.lSpecialKeys & InputState::SPECIAL_KEY_DOWN_BACKSPACE; }
			bool IsInsertKeyDown() const		{ return m_InputState.lSpecialKeys & InputState::SPECIAL_KEY_DOWN_HOME; }
			bool IsHomeKeyDown() const			{ return m_InputState.lSpecialKeys & InputState::SPECIAL_KEY_DOWN_SHIFT; }
			bool IsPageUpKeyDown() const		{ return m_InputState.lSpecialKeys & InputState::SPECIAL_KEY_DOWN_PAGE_UP; }
			bool IsPageDownKeyDown() const		{ return m_InputState.lSpecialKeys & InputState::SPECIAL_KEY_DOWN_PAGE_DOWN; }
			bool IsDeleteKeyDown() const		{ return m_InputState.lSpecialKeys & InputState::SPECIAL_KEY_DOWN_DELETE; }
			bool IsEndKeyDown() const			{ return m_InputState.lSpecialKeys & InputState::SPECIAL_KEY_DOWN_END; }
			bool IsNumberLockKeyDown() const	{ return m_InputState.lSpecialKeys & InputState::SPECIAL_KEY_DOWN_NUMBER_LOCK; }
			bool IsPauseBreakKeyDown() const	{ return m_InputState.lSpecialKeys & InputState::SPECIAL_KEY_DOWN_PAUSE_BREAK; }
			bool IsScreenLockKeyDown() const	{ return m_InputState.lSpecialKeys & InputState::SPECIAL_KEY_DOWN_SCREEN_LOCK; }
			bool IsDownArrowKeyDown() const		{ return m_InputState.lSpecialKeys & InputState::SPECIAL_KEY_DOWN_DOWN_ARROW; }
			bool IsUpArrowKeyDown() const		{ return m_InputState.lSpecialKeys & InputState::SPECIAL_KEY_DOWN_UP_ARROW; }
			bool IsLeftArrowKeyDown() const		{ return m_InputState.lSpecialKeys & InputState::SPECIAL_KEY_DOWN_LEFT_ARROW; }
			bool IsRightArrowKeyDown() const	{ return m_InputState.lSpecialKeys & InputState::SPECIAL_KEY_DOWN_RIGHT_ARROW; }

			// Mouse Buttons ================================================================================================
			bool IsLeftMouseButtonDown() const  { return m_InputState.lMouseButtons & InputState::MOUSE_BUTTON_LEFT_DOWN; }
			bool IsMiddleMouseButtonDown() const  { return m_InputState.lMouseButtons & InputState::MOUSE_BUTTON_MIDDLE_DOWN; }
			bool IsRightMouseButtonDown() const  { return m_InputState.lMouseButtons & InputState::MOUSE_BUTTON_RIGHT_DOWN; }
#pragma warning(default : 4800)
		private:
			InputState m_InputState;
		};
	}
}
