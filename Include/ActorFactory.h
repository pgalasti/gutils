#pragma once
#include "stdafx.h"

#include "Actor.h"
#include <map>
#include <string>
#include <functional>

#include "IDGenerator.h"
#include "XML.h"
#include "ResourceDirectories.h"

// Keeping this here for debugging for now. Need to find better solution for debug/profile/release build
#include "Logging.h"

namespace GameEngine {
	namespace Logic {
		namespace Actors {

			using namespace GUtils::Concurrency;

			//typedef std::function<ActorComponent(void)> ActorComponentCreatorFunc;
			typedef ActorComponent *(*ActorComponentCreatorFunc)(void);
			typedef std::map<std::string, ActorComponentCreatorFunc> ActorComponentCreatorMap;

			class DllExport ActorFactory
			{
				IDGenerator* m_pIdGenerator;

			public:
				ActorFactory();
				~ActorFactory();

				void AddComponentCreator(const std::string& componentName, ActorComponentCreatorFunc creationFunction);
				StrongActorPtr CreateActor(const char* pszActorResource);

			protected:
				ActorComponentCreatorMap m_ActorComponentCreators;
				virtual StrongActorComponentPtr CreateComponent(ElementPtr pElement);


			private:
				MEMBER_LOGGING_ENABLED
			};

		}
	}
}