#pragma once
#include "stdafx.h"

#include <functional>


namespace GUtils {
	namespace Function {

		// Usage
		//GUtils::Function::Delegate<BOOL> BareFunction(std::bind(TestFunc, 3, "This is a test", 3.141592653f));
		//GUtils::Function::Delegate<BOOL> MemberFunction(std::bind(&MyStruct::Return5PlusParam, object, 1, 2));

		template <class RetType>
		class DllExport Delegate
		{
			std::function<RetType(void)> m_StoredFunction;

		public:
			Delegate(function<RetType(void)> function)
			{
				m_StoredFunction = function;
			}

			RetType Execute()
			{
				return m_StoredFunction();
			}

			RetType operator()()
			{
				return this->Execute();
			}
		};

	}
}