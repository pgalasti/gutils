#pragma once
#include "stdafx.h"

#include "TestMesh.h"
#include "GMath.h"
#include <list>

namespace GraphicsEngine {
	namespace Model {

		class DllExport GModelLoader
		{
		public:
			static BOOL LoadFile(const char* pszPath, IMesh** pMeshData);
			static BOOL Load(const ubyte8* pData);

		private:
			static BOOL LoadVersion1(const std::vector<std::string>& fileLines, IMesh** pMeshData);

			static const char COMMENT_INDICATOR = '#';
			static const char VERTEX_INDICATOR = 'V';
			static const char INDEX_INDICATOR = 'i';
			static const char STATISTICS_INDICATOR = '$';

		};

	}
}
