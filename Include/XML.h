#pragma once
#include "stdafx.h"

#include <list>
#include<vector>
#include<memory>

namespace GUtils {
	namespace Utils {
		namespace XML {

			struct RootElement;
			struct Element;
			struct Attribute;

			typedef std::shared_ptr <RootElement> RootElementPtr;
			typedef std::shared_ptr <Element> ElementPtr;
			typedef std::shared_ptr <Attribute> AttributePtr;

			struct DllExport RootElement
			{
				RootElement();
				~RootElement();
				std::list<AttributePtr> m_Atributes;

				void AddAttribute(const Attribute& attribute);
			};

			struct DllExport Element
			{
				Element();
				~Element();

				void FindElements(const char* pszElementName, std::list<Element>& elements);
				AttributePtr GetAttributeByName(const std::string& name);
				void AddAttribute(const Attribute& attribute);
				void AddChildElement(Element element);

				std::string m_Name;
				std::string m_Value;

				std::vector<ElementPtr> GetChildElements() { return m_Elements; }
				std::vector<AttributePtr> GetAttributes() { return m_Atributes; }

			private:

				std::vector<ElementPtr> m_Elements;
				std::vector<AttributePtr> m_Atributes;

			};

			struct DllExport Attribute
			{
				std::string Key;
				std::string Value;
			};

			struct DllExport XmlDocument
			{
				XmlDocument();
				~XmlDocument();

				RootElementPtr m_pRootElement;
				ElementPtr m_pTopElement;
			};
		}
	}

}