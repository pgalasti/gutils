#pragma once
#include "stdafx.h"

#include "GenericEventData.h"
#include "Engine.h"

namespace GameEngine {
	namespace Logic {
		namespace Events {
			namespace Default {

				using namespace GUtils::Graphics;

				class DllExport DebugEngineEvent : public GenericEventData
				{
					IGraphicEngine* m_pGraphicsEngine;
					bool m_bDisplayDebug;
				public:
					DebugEngineEvent(const uint32 timestamp, IGraphicEngine* pGraphicsEngine) : GenericEventData(timestamp)
					{
						m_pGraphicsEngine = pGraphicsEngine;
						m_bDisplayDebug = false;
					}

					~DebugEngineEvent() {}

					virtual const EventType GetEventType() const override { return 0x5390C70F; }
					virtual void Serialize(ubyte8* bytes) const override {}
					virtual IEventDataPtr Copy() const override { return IEventDataPtr(new DebugEngineEvent(this->GetTimeStamp(), m_pGraphicsEngine)); }
					virtual const char* GetName() const override { return "DebugEngineEvent"; }
					void SetDebug(const bool debug = true) { m_bDisplayDebug = debug; }
					bool GetDebug() const { return m_bDisplayDebug; }
				};
			}
		}
	}
}