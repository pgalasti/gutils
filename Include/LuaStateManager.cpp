#pragma once
#include "stdafx.h"
#include "LuaStateManager.h"

using namespace LuaScripting;
using namespace GUtils::Scripting;

LuaStateManager::LuaStateManager()
{
	m_pLuaState = nullptr;
}

LuaStateManager::~LuaStateManager()
{
	this->Destroy();
}

BOOL LuaStateManager::Initialize()
{
	m_pLuaState = LuaPlus::LuaState::Create(true);
	if (m_pLuaState == nullptr)
	{
		m_LastError = "LuaState was unable to be created!";
		return FALSE;
	}

	this->GetGlobalVars().RegisterDirect("ExecuteFile", (*this), &LuaStateManager::ExecuteFile);
	this->GetGlobalVars().RegisterDirect("ExecuteLine", (*this), &LuaStateManager::ExecuteLine);

	return TRUE;
}

BOOL LuaStateManager::Destroy()
{
	if (m_pLuaState)
	{
		LuaPlus::LuaState::Destroy(m_pLuaState);
		m_pLuaState = nullptr;
	}

	return TRUE;
}

BOOL LuaStateManager::ExecuteFile(const char* pszPath)
{
	int32 result = m_pLuaState->DoFile(pszPath);
	if (result != 0)
	{
		SetError(result);
		return FALSE;
	}
		
	return TRUE;
}

BOOL LuaStateManager::ExecuteLine(const char* pszLine)
{
	int32 result = m_pLuaState->DoString(pszLine);
	if (result != 0)
	{
		SetError(result);
		return FALSE;
	}

	return TRUE;
}

void LuaStateManager::SetError(const int32 errorNumber)
{
	LuaPlus::LuaStackObject stackObject(m_pLuaState, -1);
	m_LastError = stackObject.GetString();
	if (m_LastError.empty())
	{
		m_LastError = "Unknown error!";
		return;
	}

	ClearStack();
}

void LuaStateManager::ClearStack()
{
	if (m_pLuaState)
		m_pLuaState->SetTop(0);
}