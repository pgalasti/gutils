#pragma once

#include "stdafx.h"
#include "Parser.h"
#include <fstream>
#include <string>
#include <map>
#include <algorithm>

namespace GUtils {
	namespace IO {

		class DllExport OptionLoader
		{
		public:
			OptionLoader(){}
			OptionLoader(std::ifstream& file);
			OptionLoader(std::string strFilePath);

			virtual void VLoad(std::ifstream& file);
			virtual void VLoad(std::string strFilePath);
			

			const bool GetFlagVal(const std::string strTag) const;
			const long GetIntVal(const std::string strTag) const;
			const double GetFloatVal(const std::string strTag) const;
			const std::string GetTokenVal(const std::string strTag) const;

		protected:
			virtual void VInit();

			GUtils::Util::StringParser m_LineParser;
			std::map<std::string, bool> m_BoolMap;
			std::map<std::string, int> m_IntMap;
			std::map<std::string, double> m_FloatMap;
			std::map<std::string, std::string> m_StringMap;
		};
	}
}