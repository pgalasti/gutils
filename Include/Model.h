#pragma once

#include "stdafx.h"
#include <d3d11_1.h>
#include "TestMesh.h"

using namespace GraphicsEngine::Model;

namespace GraphicsEngine {
	namespace Model {
		namespace DirectX {

			class DllExport SimpleModel
			{
			public:
				SimpleModel();
				~SimpleModel();

				UINT GetVertrexCount() const	{ return m_nVertices; }
				UINT GetIndexCount() const		{ return m_nIndicies; }

				BOOL InitializeModel(ID3D11Device* pDevice, const SimpleVertex* pVerticies, const UINT nVerticies, const UINT* pIndicies);
				void ClearBuffers();
				void Render(ID3D11DeviceContext* pDeviceContext);

			private:
				ID3D11Buffer* m_pVertexBuffer;
				ID3D11Buffer* m_pIndexBuffer;
				UINT m_nVertices;
				UINT m_nIndicies;
			};
		}
	}
}
