#pragma once
#include "stdafx.h"

#include "Resources.h"

#include<string>

namespace GUtils {
	namespace IO	{
		namespace Resources {
			namespace Packaging {

				interface DllExport IPackageFile
				{
					virtual BOOL Open() = VIRTUAL;
					virtual uint32 GetResourceSize(const PackageResource& resource) = VIRTUAL;
					virtual uint32 GetResource(const PackageResource& resource, sbyte8* pBuffer) = VIRTUAL;
					virtual uint32 GetNumberOfResources() const = VIRTUAL;
					virtual std::string GetResourceName(const uint32 number) const = VIRTUAL;
					virtual ~IPackageFile() {}
				};

			}
		}
	}
}