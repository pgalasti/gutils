#pragma once
#include "stdafx.h"
#include "XML.h"

#include <list>


namespace GUtils {
	namespace Utils {
		namespace XML {
			namespace PickyParser {

				/**
				* It's rather picky in terms of the format of what it parses (hence the name). 
				* It may be better to use a full XML library for larger files.
				*/
				class DllExport XmlParser
				{
				public:
					XmlParser();
					~XmlParser();

					BOOL Parse(const char* pszFilePath, XmlDocument& xmlDocument);

					const std::string GetLastError() const { return m_ErrorMessage; }

				private:

					BOOL BuildElements(ElementPtr& pParentElement, const std::string& remainingLines, int& traversedCharacters, std::string& lastEnding);
					BOOL ExtractRoot(RootElementPtr pRootElement, const std::string& fileLine);
					std::string m_ErrorMessage;
				};

			}
		}
	}
}
