#pragma once
#define _CRT_SECURE_NO_WARNINGS
#ifdef _WIN32
#include<Windows.h>
#endif
#include"Defines.h"

#include<assert.h>

#define _CRTDBG_MAP_ALLOC
#include <stdlib.h>
#include <crtdbg.h>

#include "GDefs.h"


#ifdef _WIN32

// Compiler complains it doesn't like std structures being exposed (e.g. map<>) since 
// it doesn't have a DLL export interface. This won't be a problem since the compiler
// for modules based on the engine will be the same. 
#pragma warning(disable: 4251)
#define DllExport   __declspec( dllexport ) 
#define DllImport   __declspec( dllimport ) 
#define DLL extern "C++"

#else

#define DllExport
#define DllImport
#define DLL

#endif



