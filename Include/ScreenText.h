#pragma once
#include "stdafx.h"

#include <d3d11_1.h>
#include<vector>
#include<memory>

#include "Font.h"
#include "TestMesh.h"
#include "GMath.h"

using namespace GUtils::GMath;


namespace GraphicsEngine{
		namespace Font {
			namespace DirectX {

			class DllExport ScreenText
			{
			public:
				ScreenText(ID3D11Device* pDevice, ID3D11DeviceContext* pDeviceContext);
				~ScreenText();

				BOOL Load(const std::wstring& spriteFontPath);
				BOOL WriteText(const std::wstring& text, const Point2D<uint32>& position, const ColorRGBA<float>& textColor);

			protected:

				struct Impl;
				Impl* pImpl;

			};


#pragma region deprecated
#ifdef DEPRECATED_SCREEN_TEXT
			class DllExport ScreenText
			{
			public:
				ScreenText(const CoordinateMap& coordinateTextureMapping);
				~ScreenText();

				BOOL CreateScreenText(ID3D11Device* pDevice, const char* pszText);
				BOOL Render(ID3D11DeviceContext* pDeviceContext);

			protected:

				BOOL LoadVerticies(const char* pszText);

				std::vector<PosTexVertex> m_Verticies;
				std::vector<WORD> m_Indicies;

				CoordinateMap m_CoordinateMap;

				ID3D11Buffer* m_pVertexBuffer;
				ID3D11Buffer* m_pIndexBuffer;
			};

#endif
#pragma endregion deprecated
		}
	}
}
