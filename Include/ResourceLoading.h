#pragma once
#include "stdafx.h"

#include<string>
#include<memory>

#include "Resources.h"

namespace GUtils {
	namespace IO	{
		namespace Resources {

			interface DllExport IResourceLoader
			{
				virtual std::string GetPattern() const = VIRTUAL;
				virtual bool UseRawFile() const = VIRTUAL;
				virtual uint32 GetLoadedResourceSize(sbyte8* pRawBuffer, const uint32 rawSize) = VIRTUAL;
				virtual bool LoadResource(sbyte8* pRawBuffer, const uint32 rawSize, std::shared_ptr<ResourceHandle> pResourceHandle) = VIRTUAL;
			};

			class DefaultResourceLoader : public IResourceLoader
			{
			public:
				virtual std::string GetPattern() const override { return "*"; }
				virtual bool UseRawFile() const override { return true; }
				virtual uint32 GetLoadedResourceSize(sbyte8* pRawBuffer, const uint32 rawSize) { return rawSize; }
				virtual bool LoadResource(sbyte8* pRawBuffer, const uint32 rawSize, std::shared_ptr<ResourceHandle> pResourceHandle) { return true; }
			};

			typedef std::shared_ptr<IResourceLoader> SharedIResourceLoaderPtr;
			typedef std::shared_ptr<DefaultResourceLoader> SharedDefaultResourceLoaderPtr;
		}
	}
}