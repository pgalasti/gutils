#pragma once
#include "stdafx.h"

#include "PackageFiles.h"
#include "Resources.h"

#include<string>
#include<map>

using namespace GUtils::IO::Resources::Packaging;

namespace GUtils {
	namespace IO	{
		namespace Resources {
			namespace Debug {

				using namespace GUtils::IO::Resources;

				class DllExport DevelopmentPackageFile : public IPackageFile
				{
				public:

					DevelopmentPackageFile(const std::string& filePath);
					virtual ~DevelopmentPackageFile();
					virtual BOOL Open() override;
					virtual uint32 GetResourceSize(const PackageResource& resource) override;
					virtual uint32 GetResource(const PackageResource& resource, sbyte8* pBuffer) override;
					virtual uint32 GetNumberOfResources() const override;
					virtual std::string GetResourceName(const uint32 number) const override;

				private:

					std::string m_RootDirectory;
					std::map<uint32, std::string> m_ResourceRelativePathMap;

				};

			}
		}
	}
}
