#pragma once
#include "stdafx.h"

#include<list>
#include<queue>
#include<thread>
#include<future>
#include<mutex>

namespace GUtils {
	namespace Concurrency {
		
		// Not fully tested.
		// Probably should expand to run threads with parameters passed.
		class DllExport ThreadPool
		{
		public:
			ThreadPool();
			ThreadPool(uint32 numConcurrentThreads);
			~ThreadPool();

			void SetConcurrentLimit(const uint32 numConcurrentThreads);
			void Execute();
			void Shutdown();

			void AddThread(const std::function <void(void)>& func);
			void AddThreads(std::list<std::function<void(void)>> functions);
			void AddThreads(std::queue<std::function <void(void)>> functions);

			uint32 GetRunningThreadCount();
			uint32 GetQueuedThreadCount();

			bool IsRunning();
		protected:

			std::queue<std::function <void(void)>> m_ThreadQueue;
			std::list<std::future<void>> m_RunningThreads;
			std::mutex m_QueueMutex;
			std::mutex m_MonitorMutex;
			std::mutex m_ThreadListMutex;
			std::thread* m_pMonitorThread;
			uint32 m_Limit;

			bool m_DoMonitor;

			bool IsMonitoring();
			void SetMonitor(const bool monitor = true);
			void ThreadMonitor();

		private:

			const int MONITOR_CHECK = 50;
		};

	}
}
