#pragma once
#include "stdafx.h"

#include "Font.h"
#include <map>

namespace GraphicsEngine {
	namespace Font {
		namespace DirectX {

			class DllExport FontCoordinateLoader
			{
			public:

				enum LoadType
				{
					Normal
				};

				FontCoordinateLoader(LoadType loadType = Normal);
				~FontCoordinateLoader();

				BOOL LoadCharactersFromData(const ubyte8* pData, CoordinateMap& map);
				BOOL LoadCharactersFromPath(const char* pszFilePath, CoordinateMap& map);


			protected:

				LoadType m_LoadType;
			};

		}
	}
}
