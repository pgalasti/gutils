#pragma once

#include "stdafx.h"
#include <d3d11_1.h>
#include <DirectXMath.h>
#include "IRenderer.h"
#include <vector>
#include "GMath.h"

using namespace GUtils;

namespace GraphicsEngine {
	namespace Model {

		// temp
		using namespace DirectX;
		
		struct DllExport SimpleVertex
		{
			XMFLOAT3 Pos;
			XMFLOAT4 Color;
		};

		struct DllExport PosNormalVertex
		{
			GMath::Vector3DValues<float32> Pos;
			GMath::Vector3DValues<float32> Normal;
		};

		struct DllExport PosTexVertex
		{
			GMath::Vector3DValues<float32> Pos;
			GMath::Vector2DValues<float32> Tex;
		};

		struct DllExport PosNormalTexVertex
		{
			GMath::Vector3DValues<float32> Pos;
			GMath::Vector3DValues<float32> Normal;
			GMath::Vector2DValues<float32> Tex;
		};

		DllExport PosNormalVertex TruncateTex(const PosNormalTexVertex& vertex);
		DllExport SimpleVertex TruncateNormalTex(const PosNormalTexVertex& vertex);
		
		interface DllExport IMesh
		{
		public:
			virtual BOOL BuildBuffers(ID3D11Device* pDevice) = VIRTUAL;
			virtual BOOL Render(ID3D11DeviceContext* pDeviceContext) = VIRTUAL;
			virtual ~IMesh() {}
			virtual uint32 GetNumberVerticies() const = VIRTUAL;
			virtual uint32 GetNumberIndicies() const = VIRTUAL;
		};

		class DllExport TestMesh : public IMesh
		{
		public:
			TestMesh() 
			{
				m_pVertexBuffer = nullptr;
				m_pIndexBuffer = nullptr;
			}

			virtual ~TestMesh()
			{
				COM_RELEASE(m_pVertexBuffer);
				COM_RELEASE(m_pIndexBuffer);
				m_Verticies.clear();
				m_Indicies.clear();
			};

			void SetVerticies(const SimpleVertex* pVerticies, UINT numVerticies, WORD* pIndicies, UINT numIndicies);
			virtual BOOL BuildBuffers(ID3D11Device* pDevice);
			virtual BOOL Render(ID3D11DeviceContext* pDeviceContext);

			TestMesh& operator= (const TestMesh& other) = delete;
			TestMesh& operator= (TestMesh&& other);

			virtual uint32 GetNumberVerticies() const override { return m_Verticies.size(); }
			uint32 GetNumberIndicies() const { return m_Indicies.size(); }


		private:
			std::vector<SimpleVertex> m_Verticies;
			std::vector<WORD> m_Indicies;

			ID3D11Buffer* m_pVertexBuffer;
			ID3D11Buffer* m_pIndexBuffer;
		};

		class DllExport PosNormalMesh : public IMesh
		{
		public:
			PosNormalMesh()
			{
				m_pVertexBuffer = nullptr;
				m_pIndexBuffer = nullptr;
			}

			virtual ~PosNormalMesh()
			{
				COM_RELEASE(m_pVertexBuffer);
				COM_RELEASE(m_pIndexBuffer);
				m_Verticies.clear();
				m_Indicies.clear();
			};

			void SetVerticies(const PosNormalVertex* pVerticies, UINT numVerticies, WORD* pIndicies, UINT numIndicies);
			virtual BOOL BuildBuffers(ID3D11Device* pDevice);
			virtual BOOL Render(ID3D11DeviceContext* pDeviceContext);

			virtual uint32 GetNumberVerticies() const override { return m_Verticies.size(); }
			virtual uint32 GetNumberIndicies() const { return m_Indicies.size(); }

		private:
			std::vector<PosNormalVertex> m_Verticies;
			std::vector<WORD> m_Indicies;

			ID3D11Buffer* m_pVertexBuffer;
			ID3D11Buffer* m_pIndexBuffer;
		};

		class DllExport PosNormalTexMesh : public IMesh
		{
		public:
			PosNormalTexMesh()
			{
				m_pVertexBuffer = nullptr;
				m_pIndexBuffer = nullptr;
			}

			virtual ~PosNormalTexMesh()
			{
				COM_RELEASE(m_pVertexBuffer);
				COM_RELEASE(m_pIndexBuffer);
				m_Verticies.clear();
				m_Indicies.clear();
			};

			void SetVerticies(const PosNormalTexVertex* pVerticies, UINT numVerticies, WORD* pIndicies, UINT numIndicies);
			virtual BOOL BuildBuffers(ID3D11Device* pDevice);
			virtual BOOL Render(ID3D11DeviceContext* pDeviceContext);

			virtual uint32 GetNumberVerticies() const override { return m_Verticies.size(); }
			virtual uint32 GetNumberIndicies() const { return m_Indicies.size(); }

		private:
			std::vector<PosNormalTexVertex> m_Verticies;
			std::vector<WORD> m_Indicies;

			ID3D11Buffer* m_pVertexBuffer;
			ID3D11Buffer* m_pIndexBuffer;
		};
	}
}