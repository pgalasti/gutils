#pragma once
#include "stdafx.h"

#include "IEventManager.h"

#include <list>
#include <queue>
#include <map>


namespace GameEngine {
	namespace Logic {
		namespace Events {

			typedef std::list<EventListenerDelegate> EventListenerList;
			typedef std::map< EventType, EventListenerList > EventListenerMap; // Use a multimap instead?
			typedef std::list<IEventDataPtr> EventQueue;

			class DllExport EventManager : public IEventManager
			{
				EventListenerMap m_EventListeners;
				EventQueue m_Queues[2];
				ubyte8 m_ActiveQueue;

			public:

				EventManager();
				virtual ~EventManager();

				virtual BOOL AddListener(const EventListenerDelegate& eventDelegate, const EventType type) override;
				virtual BOOL RemoveListener(const EventListenerDelegate& eventDelegate, const EventType type) override;

				virtual BOOL TriggerEventImmediately(const IEventDataPtr& pEvent) override;
				virtual BOOL QueueEvent(const IEventDataPtr& pEvent) override;
				virtual BOOL AbortEvent(const EventType type, bool allOfThisType = false) override;

				virtual BOOL Update() override;

				static const BOOL LISTENER_ALREADY_REGISTERED = -1;
				static const BOOL LISTENER_NOT_FOUND = -2;
				static const BOOL TYPE_NOT_FOUND = -3;

			};

		}
	}
}