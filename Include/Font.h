#pragma once
#include "stdafx.h"
#include <map>

using namespace std;

namespace GraphicsEngine {
	namespace Font {

		struct DllExport FontCoordinateStructure
		{
			char Character;
			float LeftU;
			float RightU;
			ushort16 Width;
		};

		typedef ubyte8 AsciiValue;
		typedef map<AsciiValue, FontCoordinateStructure> CoordinateMap;

	}
}
