#pragma once
#include "stdafx.h"

#include <future>
#include <map>
#include <set>

#include "DirectXMathConversions.h"
#include "Engine.h"
#include "Logging.h"
#include "Camera.h"
#include "DirectX11Renderer.h"
#include "TestMesh.h"
#include "MemoryPool.h"
#include "IDGenerator.h"
#include "Metrics.h"
#include "ResourceDirectories.h"
#include "ScreenText.h"


using namespace GUtils::Graphics;
using namespace GUtils::Graphics::Camera;
using namespace GraphicsEngine::Font::DirectX;
using namespace GraphicsEngine::Model;

namespace GraphicsEngine {
	namespace Engine {
		namespace DirectX {

			class DllExport DirectX11Engine : public IGraphicEngine
			{
			public:
				DirectX11Engine();
				virtual ~DirectX11Engine() override;

				virtual BOOL Initialize(HWND hwnd, const EngineOptions& options) override;
				virtual BOOL Shutdown() override;
				virtual BOOL Render(const float dt) override;

				virtual std::string GetVideoCardDesc() const override;
				virtual uint32 GetVideoMemory() const override;
				virtual float FPS() override;

				virtual BOOL RegisterModel(const sbyte8* pModel, uint32& resourceID) override;
				virtual BOOL RegisterModelFromPath(const char* pszModelResourcePath, uint32& resourceID) override;
				virtual BOOL CopyToNewModel(const uint32 resourceID, uint32& newResourceID) override;
				virtual void DeregisterModel(const uint32 resourceID) override;
				virtual void TransformModel(const uint32 resourceID, const ConstantBuffer& constantBuffer) override;
				virtual bool IsRegistered(const uint32 resourceID) override;

				virtual void ApplyBlendStateToModel(const uint32 resourceID, const std::string& blendState) override;

				virtual void TransformTexture(const uint32 resourceID, const Values4X4Struct<float>& textureTransformation) override;
				virtual void RemoveTextureTransformation(const uint32 resourceID) override;

				virtual BOOL RegisterMaterialToModel(const uint32 resourceIDOfModel, const Material& material) override;
				virtual BOOL DeregisterMaterialToModel(const uint32 resourceIDOfModel) override;

				virtual BOOL RegisterTextureToModel(const uint32 resourceID, const char* pszTexturePath, const char* pszSamplerName) override;
				virtual BOOL DeregisterTextureToModel(const uint32 resourceID) override;

				virtual BOOL WriteScreenText(const std::wstring& font, const ScreenTextParams& params) override;

				virtual BOOL RegisterDirectionalLight(const DirectionalLight& directionalLight, uint32& resourceID) override;
				virtual BOOL DergisterDirectionalLight(const uint32 resourceID) override;
				virtual BOOL ApplyDirectionalLight(const DirectionalLight& directionalLight, const uint32 resourceID) override;

				virtual BOOL RegisterPointLight(const PointLight& pointLight, uint32& resourceID)override;
				virtual BOOL DergisterPointLight(const uint32 resourceID) override;
				virtual BOOL ApplyPointLight(const PointLight& pointLight, const uint32 resourceID) override;

				virtual BOOL RegisterSpotLight(const SpotLight& spotLight, uint32& resourceID) override;
				virtual BOOL DergisterSpotLight(const uint32 resourceID) override;
				virtual BOOL ApplySpotLight(const SpotLight& spotLight, const uint32 resourceID) override;

				virtual BOOL AssignVertexShaderToModel(const uint32 resourceID, std::string shaderID) override;
				virtual BOOL AssignPixelShaderToModel(const uint32 resourceID, std::string shaderID) override;

				virtual uint32 GetLoadedVerticiesNumber() const override { return m_nLoadedVerticies; }
				virtual uint32 GetLoadedIndiciesNumber() const override { return m_nLoadedIndicies; }

				virtual void ApplyCamera(const GUtils::Graphics::Camera::Camera& camera) override;
				virtual void GetCamera(GUtils::Graphics::Camera::Camera& camera) const override;

				virtual void SetRasterizerState(const char* pszRasterizerStateName) override;
				virtual void SetScreenDebug(const bool set = true) override;

			protected:

				// <Vertex Shader, Input Layout for shader>
				typedef std::pair<ID3D11VertexShader*, ID3D11InputLayout*> VertexShaderAndInputPair_t;
				// <Model ID, Transformation>
				typedef std::pair <uint32, ConstantBuffer> TransformOnModelKey_t;
				// <Shader Resource View, Sampler State>
				typedef std::pair<ID3D11ShaderResourceView*, ID3D11SamplerState*> TextureResourceAndSampler_t;
				// <Model ID, Texture Transformation>
				typedef std::pair <int, Values4X4Struct<float>> TextureTransformationOnModelKey_t;
				// <BlendState, Factor to blend>
				typedef std::pair<ID3D11BlendState*, Vector4DValues<float>> BlendStateAndFactor_t;
				// <Font Name, ScreenTextParams>
				typedef std::pair<std::wstring, ScreenTextParams> WriteTextKey_t;

				typedef std::shared_ptr<ScreenText> ScreenTextPtr;

				// DirectX11 interfaces
				ID3D11Device* m_pDevice;
				ID3D11DeviceContext* m_pDeviceContext;
				IDXGISwapChain*	m_pSwapChain;
				ID3D11RenderTargetView* m_pRenderTargetView;
				ID3D11Texture2D* m_pDepthStencilBuffer;
				ID3D11DepthStencilState* m_pDepthStencilState;
				ID3D11DepthStencilView* m_pDepthStencilView;
				ID3D11RasterizerState* m_pRasterState;
				ID3D11Buffer* m_pPerObjectConstantBuffer;
				ID3D11Buffer* m_pPerFrameConstantBuffer;

				GUtils::Graphics::Camera::Camera m_Camera;

				// Initialization Functions
				virtual BOOL LoadDisplayAdapter(const EngineOptions& options, DXGI_ADAPTER_DESC& adapterDescription, DXGI_RATIONAL& refreshRate);
				virtual BOOL LoadDeviceAndSwapChain(HWND hwnd, const EngineOptions& options, const DXGI_RATIONAL& refreshRate);
				virtual BOOL LoadDepthStencilBufferAndState(const EngineOptions& options);
				virtual BOOL LoadViewport(const EngineOptions& options);
				virtual BOOL LoadRasterizerStates();
				virtual BOOL LoadBlendingStates();
				virtual BOOL LoadShaders();
				virtual BOOL LoadStaticVertexShaderFiles(); // probably remove this and rely on startup script
				virtual BOOL LoadStaticPixelShaderFiles(); // probably remove this and rely on startup script
				virtual BOOL LoadConstantBuffers();
				virtual BOOL LoadSamplers();
				virtual BOOL LoadFonts();
				virtual BOOL InitializeCamera(const EngineOptions& options);
				virtual BOOL InitializeDefaultStates();

				// Rendering functions
				void ClearScreenToDefaultColor();
				virtual BOOL RenderRegisteredLights();
				virtual BOOL RenderRegisteredResources();
				virtual BOOL ResetStateForModelRender();
				virtual BOOL RenderOpaqueModels(std::vector<TransformOnModelKey_t>& deferredModels);
				virtual BOOL RenderBlendedModels(std::vector<TransformOnModelKey_t>& deferredModels);
				virtual BOOL ApplyStateForTransformation(const TransformOnModelKey_t& transformation, ConstantBuffer& perModelBuffer, const XMMATRIX& viewProjection);
				virtual BOOL GetRegisteredMesh(const uint32 resourceID, std::shared_ptr<IMesh>& pMesh);
				virtual BOOL LoadPendingModel(const uint32 resourceID);
				virtual BOOL RenderScreenText();
				virtual BOOL WriteDebugInfoToScreen();

				// ID Generators
				std::unique_ptr<GUtils::Concurrency::IDGenerator> m_pMeshIdGenerator;
				std::unique_ptr<GUtils::Concurrency::IDGenerator> m_pDirectionalLightIdGenerator;
				std::unique_ptr<GUtils::Concurrency::IDGenerator> m_pPointLightIdGenerator;
				std::unique_ptr<GUtils::Concurrency::IDGenerator> m_pSpotLightIdGenerator;

				// Mapped states / models
				// Since the path is loaded as an async function parameter, we need to keep track of it to make sure it doesn't go out of stack
				// I feel like this is an ugly hack
				std::map<uint32, std::string> m_LoadingModelPath;
				std::map<uint32, std::shared_ptr<IMesh>> m_LoadedMeshMap;
				std::set<uint32> m_RegisteredModelIds;
				std::vector<VertexShaderAndInputPair_t> m_RegisteredModelToVSVector;
				std::vector<ID3D11PixelShader*> m_RegisteredModelToPSVector;
				std::vector<DirectionalLight> m_LoadedDirectionalLightVector;
				std::vector<PointLight> m_LoadedPointLightVector;
				std::vector<SpotLight> m_LoadedSpotLightVector;
				std::multimap<uint32, ConstantBuffer> m_ModelTransformations; // TransformOnModelKey_t
				std::map<uint32, std::string> m_BlendModelTransformations;
				std::vector<Values4X4Struct<float>> m_TextureTransformations;
				std::map<std::string, VertexShaderAndInputPair_t> m_VertexShaderMap;
				std::map<std::string, ID3D11PixelShader*> m_PixelShaderMap;
				std::map<std::string, ID3D11RasterizerState*> m_RasterizerStateMap;
				std::map<std::string, BlendStateAndFactor_t> m_BlendStateMap;
				std::map<std::string, ID3D11SamplerState*> m_SamplerStateMap;
				std::map<uint32, ConstantBuffer> m_ModelTransformationMap;
				std::map<uint32, Material> m_MaterialToRegisteredModelMap;
				std::map<uint32, TextureResourceAndSampler_t> m_TextureToRegisteredModelMap;
				std::map<uint32, std::future<IMesh*>> m_LoadingMeshMap;
				std::map<std::wstring, ScreenTextPtr> m_RegisteredScreenText;
				std::vector<WriteTextKey_t> m_TextToWrite;

				// Flags
				bool m_bShutdown;
				bool m_bRasterizerStateChanged;

				float m_LastFPS;
			private:

				GUtils::Time::GameTimer m_EngineTimer;
				uint32 m_nVideoMemory;
				std::string m_VideoCardDesc;
				uint32 m_nLoadedVerticies;
				uint32 m_nLoadedIndicies;
				bool m_bDebugInfo;
				ResourceDirectories m_ResourceDirectories;

				BOOL SetRegisteredResourceVertexShaderAndLayout(const uint32 resourceID);
				BOOL SetRegisteredResourcePixelShader(const uint32 resourceID);
				bool GetRegisteredResourceMaterial(const uint32 resourceID, Material& material);
				bool GetRegisteredResourceTexture(const uint32 resourceID);

				IMesh* LoadModelAsyncPath(const char* pszModelResourcePath);

				MEMBER_LOGGING_ENABLED
			};
		}
	}
}