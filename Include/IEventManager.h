#pragma once
#include "stdafx.h"

#include "IEventData.h"

namespace GameEngine {
	namespace Logic {
		namespace Events {

			interface DllExport IEventManager 
			{
				virtual ~IEventManager() {}

				virtual BOOL AddListener(const EventListenerDelegate& eventDelegate, const EventType type) = VIRTUAL;
				virtual BOOL RemoveListener(const EventListenerDelegate& eventDelegate, const EventType type) = VIRTUAL;
				
				virtual BOOL TriggerEventImmediately(const IEventDataPtr& pEvent) = VIRTUAL;
				virtual BOOL QueueEvent(const IEventDataPtr& pEvent) = VIRTUAL;
				virtual BOOL AbortEvent(const EventType type, bool allOfThisType = false) = VIRTUAL;

				virtual BOOL Update() = VIRTUAL;

				// Not sure I like this.
				static IEventManager* GetManager();
			};

		}
	}
}