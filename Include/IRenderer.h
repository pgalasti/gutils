#pragma once

#include "stdafx.h"
#include <vector>

// Interface for different APIs to render a template vertex type
template<typename T>
class DllExport IRenderer
{
public:

	virtual BOOL Render(const std::vector<T>& verticies, UINT numVerticies, const std::vector<WORD>& indicies, UINT numIndicies) = VIRTUAL;
	virtual ~IRenderer(){};
};