#pragma once
#include "stdafx.h"

#include <string>

namespace GUtils {
	namespace Utils {
		namespace Functions {

			DllExport void TrimTrailingSpaces(char * pszString);

			DllExport void RemoveLeadingSpaces(char* pszString);

			DllExport void RemoveTrailingAndLeadingSpaces(char * pszString);

			DllExport void TrimTrailingSpaces(std::string& string);
			
			DllExport void RemoveLeadingSpaces(std::string& string);

			DllExport void RemoveTrailingAndLeadingSpaces(std::string& string);

			DllExport void CleanString(std::string& string, const std::string& characters);

			DllExport void ToUpper(std::string& string);
			
			DllExport void ToLower(std::string& string);

			DllExport BOOL WildcardMatch(const char* pattern, const char* string);
		}
	}
}