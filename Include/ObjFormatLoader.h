#pragma once
#include "stdafx.h"

#include <list>

#include "TestMesh.h"
#include "GMath.h"

namespace GraphicsEngine {
	namespace Model {

		enum ModelType
		{
			UnknownType = -1,
			SimpleVertexType = 0,
			PositionNormalType = 1,
		};

		class DllExport ObjFormatLoader
		{
		public:
			static BOOL LoadFile(const char* pszPath, IMesh** pMeshData);
			static ModelType DetermineFileType(const std::list<std::string>& fileLines);

		private:
			static BOOL GetValue(const char* dirtyValues, WORD& values, WORD& normal);
			static GMath::Vector3DValues<float32> Extract3Vec(std::string line);
			static BOOL ExtractSimpleVertex(const std::list<std::string>& fileLines, std::vector<SimpleVertex>& simpleVerticies, std::vector<WORD>& indicies);
			static BOOL ExtractPosNormalVertex(const std::list<std::string>& fileLines, std::vector<PosNormalVertex>& simpleVerticies, std::vector<WORD>& indicies);

			static const char COMMENT_INDICATOR = '#';
			static const char VERTEX_INDICATOR = 'v';
			static const char NORMAL_INDICATOR = 'n';
			static const char FACE_INDICATOR = 'f';
		};

	}
}
