#ifndef GMATH_HEADER
#define GMATH_HEADER
#include "stdafx.h"
#include <math.h>
#include "GDefs.h"

namespace GUtils {
	// Helper structures/classes/functions for math operations.
	namespace GMath {

		// Pi
		const float PI_F			= 3.141592654f;
		// 2 x Pi
		const float TWOPI_F			= 6.283185307f;
		// 1 / Pi
		const float ONEDIVPI_F		= 0.318309886f;
		// 1 / 2Pi
		const float ONEDIV2PI_F		= 0.159154943f;
		// Pi / 2
		const float PIDIV2_F		= 1.570796327f;
		// Pi / 4
		const float PIDIV4_F		= 0.785398163f;
		// 180 / Pi
		const float RAD_F			= 180/PI_F;

		// 2D Vector.
		// 8 bytes in size. Able to ZeroMemory()
		struct DllExport Vector2D
		{
			float xf;
			float yf;

			Vector2D() { ZeroMemory(this, sizeof Vector2D); };
			Vector2D(float x, float y) : xf(x), yf(y){}
			Vector2D(const Vector2D& otherVector) : xf(otherVector.xf), yf(otherVector.yf){}
			Vector2D& operator= (const Vector2D& otherVector) { xf = xf; yf = otherVector.yf; return *this; }

			Vector2D operator+ (const Vector2D& otherVector) const;
			Vector2D operator- (const Vector2D& otherVector) const;
			Vector2D operator/ (const float scalar) const;
			Vector2D operator* (const float scalar) const;
			const bool operator== (const Vector2D& otherVector) const;

			inline const float Magnitude() const;
			
			Vector2D Normalize() const;
			void NormalizeThis();
			const float DotProduct(const Vector2D& otherVector) const;
			const float AngleRad(const Vector2D& otherVector) const;
			const float AngleDeg(const Vector2D& otherVector) const;
			const bool Orthogonal(const Vector2D& otherVector) const;



			static const float Magnitude(const float x, const float y) { return sqrt((x*x)+(y*y)); }
		};

		// 3D Vector.
		// 12 bytes in size. Able to ZeroMemory()
		struct DllExport Vector3D
		{
			float xf;
			float yf;
			float zf;

			Vector3D() { ZeroMemory(this, sizeof Vector3D); };
			Vector3D(float x, float y, float z) : xf(x), yf(y), zf(z){}
			Vector3D(const Vector3D& otherVector) : xf(otherVector.xf), yf(otherVector.yf), zf(otherVector.zf){}
			Vector3D& operator= (const Vector3D& otherVector) { xf = xf; yf = otherVector.yf; zf = otherVector.zf; return *this; }

			Vector3D operator+ (const Vector3D& otherVector) const;
			Vector3D operator- (const Vector3D& otherVector) const;
			Vector3D operator/ (const float scalar) const;
			Vector3D operator* (const float scalar) const;
			const bool operator== (const Vector3D& otherVector) const;

			inline const float Magnitude() const;
			
			Vector3D Normalize() const;
			void NormalizeThis();
			const float DotProduct(const Vector3D& otherVector) const;
			const float AngleRad(const Vector3D& otherVector) const;
			const float AngleDeg(const Vector3D& otherVector) const;
			const bool Orthogonal(const Vector3D& otherVector) const;
			const Vector3D Cross(const Vector3D& otherVector) const;


			static const float Magnitude(const float x, const float y, const float z) { return sqrt((x*x)+(y*y)+(z*z)); }
		};
	
		// 2D Point.
		// Able to ZeroMemory()
		template <typename T>
		struct DllExport Point2D
		{
			T x;
			T y;
		};

		template<typename T>
		using Vector2DValues = Point2D < T >;

		// 3D Point.
		// Able to ZeroMemory()
		template <typename T>
		struct DllExport Point3D
		{
			Point3D(){}
			Point3D(T x, T y, T z) { this->x = x; this->y = y; this->z = z; }
			T x;
			T y;
			T z;
		};

		template<typename T>
		using Vector3DValues = Point3D < T >;

		template<typename T>
		using ColorRGB = Point3D < T >;

		// 4D Point.
		// Able to ZeroMemory()
		template <typename T>
		struct DllExport Point4D
		{
			Point4D(){}
			Point4D(T x, T y, T z, T w) { this->x = x; this->y = y; this->z = z; this->w = w; }
			T x;
			T y;
			T z;
			T w;
		};

		template<typename T>
		using Vector4DValues = Point4D < T > ;

		template<typename T>
		using ColorRGBA = Point4D < T >;

		template <typename T>
		struct DllExport Values2X2Struct
		{
			union
			{
				struct
				{
					T r1c1, r1c2;
					T r2c1, r2c2;
				} Values;

				T ArrValues[4];
			};
		};

		template <typename T>
		struct DllExport Values3X3Struct
		{
			union
			{
				struct
				{
					T r1c1, r1c2, r1c3;
					T r2c1, r2c2, r2c3;
					T r3c1, r3c2, r3c3;
				} Values;

				T ArrValues[9];
			};
		};

		template <typename T>
		struct DllExport Values4X4Struct
		{
			union
			{
				struct
				{
					T r1c1, r1c2, r1c3, r1c4;
					T r2c1, r2c2, r2c3, r2c4;
					T r3c1, r3c2, r3c3, r3c4;
					T r4c1, r4c2, r4c3, r4c4;
				} Values;

				T ArrValues[16];
			};
		};

		// There might be a faster algorithm than this. I wrote this myself.
		template <typename T>
		Values4X4Struct<T> Multiply4X4(const Values4X4Struct<T>& matrixA, const Values4X4Struct<T> matrixB)
		{
			/*
			|  0,  1,  2, 3  |   |  0,  1,  2, 3  |
			|  4,  5,  6, 7  |   |  4,  5,  6, 7  |
			|  8,  9, 10, 11 | X |  8,  9, 10, 11 |
			| 12, 13, 14, 15 |   | 12, 13, 14, 15 |
			*/

			Values4X4Struct<T> newMatrix;
			ZeroMemory(&newMatrix, sizeof (Values4X4Struct<T>));

			for (register ubyte8 i = 0; i < 16; i++)
			{
				T value;
				ZeroMemory(&value, sizeof T);

				ushort16 aStartIndexRow = (i / 4) * 4;
				ushort16 bStartIndexCol = i % 4;
				for (register ubyte8 j = 0; j < 4; j++)
					value += matrixA.ArrValues[aStartIndexRow + j] * matrixB.ArrValues[bStartIndexCol + (j * 4)];
				
				newMatrix.ArrValues[i] = value;
			}

			return newMatrix;
		}

		template <typename T>
		Values4X4Struct<T> Identity4X4Float()
		{
			Values4X4Struct<T> identityMatrix;
			ZeroMemory(&identityMatrix, sizeof Values4X4Struct<T>);

			identityMatrix.Values.r1c1 = 1;
			identityMatrix.Values.r2c2 = 1;
			identityMatrix.Values.r3c3 = 1;
			identityMatrix.Values.r4c4 = 1;

			return identityMatrix;
		}

		template <typename T>
		Values4X4Struct<T> TranslationStruct(const T x, const T y, const T z)
		{
			Values4X4Struct<T> translationMatrix = Identity4X4Float<T>();
			translationMatrix.Values.r4c1 = x;
			translationMatrix.Values.r4c2 = y;
			translationMatrix.Values.r4c3 = z;

			return translationMatrix;
		}

		template <typename T>
		Values4X4Struct<T> ScaleStruct(const T x, const T y, const T z)
		{
			Values4X4Struct<T> scaleMatrix;
			ZeroMemory(&scaleMatrix, sizeof Values4X4Struct<T>);

			scaleMatrix.Values.r1c1 = x;
			scaleMatrix.Values.r2c2 = y;
			scaleMatrix.Values.r3c3 = z;
			scaleMatrix.Values.r4c4 = 1;

			return scaleMatrix;
		}

		template <typename T>
		Values4X4Struct<T> RotateStructX(const T x)
		{
			Values4X4Struct<T> rotationMatrix = Identity4X4Float<T>();
			rotationMatrix.Values.r2c2 = cos(-x);
			rotationMatrix.Values.r2c3 = -sin(-x);
			rotationMatrix.Values.r3c2 = sin(-x);
			rotationMatrix.Values.r3c3 = cos(-x);

			return rotationMatrix;
		}

		template <typename T>
		Values4X4Struct<T> RotateStructY(const T y)
		{
			Values4X4Struct<T> rotationMatrix = Identity4X4Float<T>();
			rotationMatrix.Values.r1c1 = cos(-y);
			rotationMatrix.Values.r1c3 = sin(-y);
			rotationMatrix.Values.r3c1 = -sin(-y);
			rotationMatrix.Values.r3c3 = cos(-y);

			return rotationMatrix;
		}

		template <typename T>
		Values4X4Struct<T> RotateStructZ(const T z)
		{
			Values4X4Struct<T> rotationMatrix = Identity4X4Float<T>();
			rotationMatrix.Values.r1c1 = cos(-z);
			rotationMatrix.Values.r1c2 = -sin(-z);
			rotationMatrix.Values.r2c1 = sin(-z);
			rotationMatrix.Values.r2c2 = cos(-z);

			return rotationMatrix;
		}

		template <typename T>
		Values4X4Struct<T> TransposeStruct(const Values4X4Struct<T>& structure)
		{
			Values4X4Struct<T> transposeStruct;
			transposeStruct.Values.r1c1 = structure.Values.r1c1;
			transposeStruct.Values.r2c1 = structure.Values.r1c2;
			transposeStruct.Values.r3c1 = structure.Values.r1c3;
			transposeStruct.Values.r4c1 = structure.Values.r1c4;

			transposeStruct.Values.r1c2 = structure.Values.r2c1;
			transposeStruct.Values.r2c2 = structure.Values.r2c2;
			transposeStruct.Values.r3c2 = structure.Values.r2c3;
			transposeStruct.Values.r4c2 = structure.Values.r2c4;

			transposeStruct.Values.r1c3 = structure.Values.r3c1;
			transposeStruct.Values.r2c3 = structure.Values.r3c2;
			transposeStruct.Values.r3c3 = structure.Values.r3c3;
			transposeStruct.Values.r4c3 = structure.Values.r3c4;

			transposeStruct.Values.r1c4 = structure.Values.r4c1;
			transposeStruct.Values.r2c4 = structure.Values.r4c2;
			transposeStruct.Values.r3c4 = structure.Values.r4c3;
			transposeStruct.Values.r4c4 = structure.Values.r4c4;

			return transposeStruct;
		}

		template <typename T>
		T EvaluateDeterminateStruct(const Values3X3Struct<T>& structure)
		{
			T determinate = 0;

			determinate += (structure.Values.r1c1 * (structure.Values.r2c2 * structure.Values.r3c3));
			determinate -= (structure.Values.r1c1 * (structure.Values.r3c2 * structure.Values.r2c3));

			determinate -= (structure.Values.r1c2 * (structure.Values.r2c1 * structure.Values.r3c3));
			determinate += (structure.Values.r1c2 * (structure.Values.r3c1 * structure.Values.r2c3));

			determinate += (structure.Values.r1c3 * (structure.Values.r2c1 * structure.Values.r3c2)); 
			determinate -= (structure.Values.r1c3 * (structure.Values.r3c1 * structure.Values.r2c2));

			return determinate;
		}

		template <typename T>
		T EvaluateDeterminateStruct(const Values4X4Struct<T>& structure)
		{
			Values3X3Struct<T> a;
			a.Values.r1c1 = structure.Values.r2c2;
			a.Values.r1c2 = structure.Values.r2c3;
			a.Values.r1c3 = structure.Values.r2c4;

			a.Values.r2c1 = structure.Values.r3c2;
			a.Values.r2c2 = structure.Values.r3c3;
			a.Values.r2c3 = structure.Values.r3c4;

			a.Values.r3c1 = structure.Values.r4c2;
			a.Values.r3c2 = structure.Values.r4c3;
			a.Values.r3c3 = structure.Values.r4c4;

			Values3X3Struct<T> b;
			b.Values.r1c1 = structure.Values.r2c1;
			b.Values.r1c2 = structure.Values.r2c3;
			b.Values.r1c3 = structure.Values.r2c4;
			
			b.Values.r2c1 = structure.Values.r3c1;
			b.Values.r2c2 = structure.Values.r3c3;
			b.Values.r2c3 = structure.Values.r3c4;

			b.Values.r3c1 = structure.Values.r4c1;
			b.Values.r3c2 = structure.Values.r4c3;
			b.Values.r3c3 = structure.Values.r4c4;

			Values3X3Struct<T> c;
			c.Values.r1c1 = structure.Values.r2c1;
			c.Values.r1c2 = structure.Values.r2c2;
			c.Values.r1c3 = structure.Values.r2c4;

			c.Values.r2c1 = structure.Values.r3c1;
			c.Values.r2c2 = structure.Values.r3c2;
			c.Values.r2c3 = structure.Values.r3c4;

			c.Values.r3c1 = structure.Values.r4c1;
			c.Values.r3c2 = structure.Values.r4c2;
			c.Values.r3c3 = structure.Values.r4c4;

			Values3X3Struct<T> d;
			d.Values.r1c1 = structure.Values.r2c1;
			d.Values.r1c2 = structure.Values.r2c2;
			d.Values.r1c3 = structure.Values.r2c3;

			d.Values.r2c1 = structure.Values.r3c1;
			d.Values.r2c2 = structure.Values.r3c2;
			d.Values.r2c3 = structure.Values.r3c3;

			d.Values.r3c1 = structure.Values.r4c1;
			d.Values.r3c2 = structure.Values.r4c2;
			d.Values.r3c3 = structure.Values.r4c3;

			return (structure.Values.r1c1*EvaluateDeterminateStruct(a)) - (structure.Values.r1c2 * EvaluateDeterminateStruct(b)) + (structure.Values.r1c3 * EvaluateDeterminateStruct(c)) - (structure.Values.r1c4 * EvaluateDeterminateStruct(d));

		}

		template<typename T>
		DllExport T Clamp(const T& x, const T& low, const T& high) {return x < low ? low : (x > high ? high : x); }
		template<typename T>
		DllExport T Minimum(const T& a, const T& b) { return a < b ? a : b; }
		template<typename T>
		DllExport T Max(const T& a, const T& b) { return a > b ? a : b; }

		DllExport const float AngleFromXY(float x, float y);
		DllExport const float RandF();
		DllExport const float RandF(float a, float b);
	}
}
#endif