#pragma once
#include "stdafx.h"

#include <memory>
#include <list>
#include <map>

#include "ResourceLoading.h"
#include "PackageFiles.h"

using namespace GUtils::IO::Resources::Packaging;

namespace GUtils {
	namespace IO	{
		namespace Resources {
			namespace Cache {

				typedef std::list<SharedResourceHandlePtr> ResourceHandleList;
				typedef std::map<std::string, SharedResourceHandlePtr> ResourceHandleMap;
				typedef std::list<SharedIResourceLoaderPtr> IResourceLoaderList;

				using namespace GUtils::IO::Resources;

				class DllExport ResourceCache
				{
				public:
					ResourceCache(const uint32 sizeInBytes, IPackageFile* pPackageFile);
					~ResourceCache();

					BOOL Initialize();
					void RegisterLoader(SharedIResourceLoaderPtr loader);

					SharedResourceHandlePtr GetHandle(const PackageResource& resource);
					void ReleaseHandle(const PackageResource& resource);
					void ReleaseHandle(SharedResourceHandlePtr handle);
					uint32 Preload(const std::string& pattern, void(*progressCallback)(int, bool&));
					uint32 Preload(const std::list<PackageResource>& resources, void(*progressCallback)(int, bool&));
					void Flush();

				protected:

					ResourceHandleList m_LeastUsedResourceHandles;
					ResourceHandleMap m_ResourceHandleMap;
					IResourceLoaderList m_ResourceLoaders;

					IPackageFile* m_pPackageFile;

					uint32 m_TotalCacheSize;
					uint32 m_AllocatedMemory;

					SharedResourceHandlePtr FindCacheResource(const PackageResource& resource);
					void Update(SharedResourceHandlePtr resourceHandle);
					SharedResourceHandlePtr Load(const PackageResource& resource);
					void Free(SharedResourceHandlePtr resourceHandle);

					BOOL MakeRoom(const uint32 size);
					sbyte8* Allocate(const uint32 size);
					void FreeOneResource();
					//void MemoryHasBeenFreed(const uint32 size);



				};

			}
		}
	}
}