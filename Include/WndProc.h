#pragma once
#include "stdafx.h"

#include "Window.h"


LRESULT CALLBACK WndProc(HWND hwnd, UINT umessage, WPARAM wparam, LPARAM lparam)
{
	switch (umessage)
	{
	case WM_DESTROY:
	{
		PostQuitMessage(0);
		return 0;
	}
	case WM_CLOSE:
	{
		PostQuitMessage(0);
		return 0;
	}
	default:
	{
		return GUtils::System::pGameWindowReference->MessageHandler(hwnd, umessage, wparam, lparam);
	}
	}
}