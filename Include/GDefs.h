#pragma once

#define VIRTUAL NULL

#ifdef _WIN32

typedef unsigned __int64 uint64;
typedef signed __int64 int64;
typedef unsigned __int32 uint32;
typedef signed __int32 int32;
typedef unsigned __int16 ushort16;
typedef signed __int16 short16;
typedef __int8 sbyte8;
typedef unsigned __int8 ubyte8;

#else

typedef unsigned long long uint64;
typedef signed long long int64;
typedef unsigned int uint32;
typedef signed int int32;
typedef unsigned short ushort16;
typedef signed short short16;
typedef char sbyte8;
typedef unsigned char ubyte8;

#endif

typedef float float32;
typedef double float64;

typedef uint32 ulong32;
typedef int32 long32;

#ifndef interface
#define interface struct
#endif