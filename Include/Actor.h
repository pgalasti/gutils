#pragma once
#include "stdafx.h"

#include <memory>
#include <map>

#include "XML.h"

namespace GameEngine {
	namespace Logic {
		namespace Actors {

			using namespace GUtils::Utils::XML;

			class DllExport Actor;
			class DllExport ActorComponent;

			typedef uint32 ActorId;
			typedef uint32 ComponentId;
			typedef std::string ActorType;

			typedef std::shared_ptr<Actor> StrongActorPtr;
			typedef std::shared_ptr<ActorComponent> StrongActorComponentPtr;
			typedef std::map<ComponentId, StrongActorComponentPtr> ActorComponentMap;

			class DllExport Actor
			{
				ActorId m_Id;
				ActorComponentMap m_ComponentMaps;
				ActorType m_Type;

			public:
				Actor(const ActorId id);
				~Actor();

				BOOL Init(const ElementPtr pElement);
				void PostInit();
				void Destroy();
				void Update(const uint32 deltaTime);

				ActorId GetId() const { return m_Id; }
				ActorType GetType() const { return m_Type; }

				template <class ComponentType> std::weak_ptr<ComponentType> GetComponent(const char* pszName);
				template <class ComponentType> std::weak_ptr<ComponentType> GetComponent(const ComponentId id);

				const ActorComponentMap* GetComponents() { return &m_ComponentMaps; }
				void AddComponent(StrongActorComponentPtr& pComponent);
			};

			class DllExport ActorComponent
			{

			protected:
				StrongActorPtr m_pOwner;

			public:
				ActorComponent() {}
				~ActorComponent(){}

				virtual void PostInit() {}
				virtual void Update(const uint32 deltaTime) {}

				virtual BOOL Init(ElementPtr pElement) = VIRTUAL;
				virtual ComponentId GetComponentId() const = VIRTUAL;

				void SetOwner(StrongActorPtr pOwner) { m_pOwner = pOwner; }
			};
		}
	}
}