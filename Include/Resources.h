#pragma once
#include "stdafx.h"

#include <string>
#include <memory>

#include "UtilFunctions.h"


namespace GUtils {
	namespace IO	{
		namespace Resources {

			class DllExport PackageResource
			{
			public:

				PackageResource(const std::string& name) { this->SetName(name); }
				~PackageResource() {}
				const std::string& GetName() const { return m_Name; }

				inline void SetName(const std::string& name);

			private:
				std::string m_Name;
			};

			class DllExport ResourceHandle
			{
			public:
				ResourceHandle(const PackageResource& resource, const uint32 size, sbyte8* pBuffer);
				~ResourceHandle();
				uint32 GetSize() const { return m_Size; }
				sbyte8* GetReadableBuffer() const { return m_pBuffer; }
				sbyte8* GetWritableBuffer() { return m_pBuffer; }
				const PackageResource GetPackageResource() const { return m_Resource; }

			protected:
				PackageResource m_Resource;
				sbyte8* m_pBuffer;
				uint32 m_Size;
			};
			
			typedef std::shared_ptr<ResourceHandle> SharedResourceHandlePtr;
		}
	}
}