#pragma once
#include "stdafx.h"

namespace GUtils {
	namespace Scripting {

		interface DllExport IScriptManager
		{
			virtual ~IScriptManager() {}
			virtual BOOL Initialize() = VIRTUAL;
			virtual BOOL Destroy() = VIRTUAL;
			virtual BOOL ExecuteFile(const char* pszPath) = VIRTUAL;
			virtual BOOL ExecuteLine(const char* pszLine) = VIRTUAL;
		};

	}
}