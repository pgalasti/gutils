#pragma once
#include "stdafx.h"

#include <DirectXMath.h>

#include "GMath.h"

using namespace DirectX;

namespace GraphicsEngine {
	namespace Math {
		namespace DirectX {

			using namespace GUtils::GMath;

			DllExport XMMATRIX InverseTranspose(CXMMATRIX M);

			template <typename T>
			DllExport XMMATRIX Convert4X4Structure(const Values4X4Struct<T>& structure)
			{
				// Dirty, but is an optimization for 99% of cases.
				if (typeid(T) == typeid(float))
				{
					XMMATRIX matrix;
					memcpy(&matrix, &structure, sizeof(Values4X4Struct<float>));
					return matrix;
				}

				return XMMATRIX(
					(float)structure.ArrValues[0], (float)structure.ArrValues[1], (float)structure.ArrValues[2], (float)structure.ArrValues[3],
					(float)structure.ArrValues[4], (float)structure.ArrValues[5], (float)structure.ArrValues[6], (float)structure.ArrValues[7],
					(float)structure.ArrValues[8], (float)structure.ArrValues[9], (float)structure.ArrValues[10], (float)structure.ArrValues[11],
					(float)structure.ArrValues[12], (float)structure.ArrValues[13], (float)structure.ArrValues[14], (float)structure.ArrValues[15]
					);
			}

			template <typename T>
			DllExport Values4X4Struct<T> ConvertXMMATRIX(CXMMATRIX matrix)
			{
				Values4X4Struct<T> structure;

				structure.ArrValues[0] = matrix.r[0].m128_f32[0];
				structure.ArrValues[1] = matrix.r[0].m128_f32[1];
				structure.ArrValues[2] = matrix.r[0].m128_f32[2];
				structure.ArrValues[3] = matrix.r[0].m128_f32[3];

				structure.ArrValues[4] = matrix.r[1].m128_f32[0];
				structure.ArrValues[5] = matrix.r[1].m128_f32[1];
				structure.ArrValues[6] = matrix.r[1].m128_f32[2];
				structure.ArrValues[7] = matrix.r[1].m128_f32[3];

				structure.ArrValues[8] = matrix.r[2].m128_f32[0];
				structure.ArrValues[9] = matrix.r[2].m128_f32[1];
				structure.ArrValues[10] = matrix.r[2].m128_f32[2];
				structure.ArrValues[11] = matrix.r[2].m128_f32[3];

				structure.ArrValues[12] = matrix.r[3].m128_f32[0];
				structure.ArrValues[13] = matrix.r[3].m128_f32[1];
				structure.ArrValues[14] = matrix.r[3].m128_f32[2];
				structure.ArrValues[15] = matrix.r[3].m128_f32[3];

				return structure;

			}


		}
	}
}