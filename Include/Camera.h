#pragma once

#include "stdafx.h"
#include <DirectXMath.h>

#include "GMath.h"

namespace GUtils {
	namespace Graphics {
		namespace Camera {

			using namespace DirectX;
			using namespace GUtils::GMath;

			class DllExport Camera
			{
			public:
				Camera();
				Camera(const Vector3D& position, const Vector3D& focus, const Vector3D& up, float aspectRatio, float nearPlane, float farPlane, float fieldOfView = PI_F / 2);
				~Camera();

				// View Functions
				void SetPosition(const Vector3D& position);
				void SetFocus(const Vector3D& focus);
				void SetUp(const Vector3D& up);

				void GetPosition(Vector3D& position) const;
				void GetFocus(Vector3D& focus) const;
				void GetUp(Vector3D& up) const;

				// Projection Functions
				void SetAspectRatio(const float aspectRatio) { m_ScreenAspectRatio = aspectRatio; }
				void SetNearPlane(const float nearPlane) { m_NearPlane = nearPlane; }
				void SetFarPlane(const float farPlane) { m_FarPlane = farPlane; }
				void SetFieldOfViewRadians(const float fieldOfView) { m_FieldOfViewRadians = fieldOfView; }

				float GetAspectRatio() const { return m_ScreenAspectRatio; }
				float GetNearPlane() const { return m_NearPlane; }
				float GetFarPlane() const { return m_FarPlane; }
				float GetFieldOfViewRadians() const { return m_FieldOfViewRadians; }

				// TODO I should create my own 'matrix' like the vector3d class so other APIs could use class generically.
				// or maybe an interface utility class?
				inline XMMATRIX GetViewProjectionMatrix() const;
				inline XMMATRIX GetViewMatrix() const;
				inline XMMATRIX GetProjectionMatrix() const;
				//inline Values4X4Struct<float> GetViewProjectionMatrix() const;
				//inline Values4X4Struct<float> GetViewMatrix() const;
				//inline Values4X4Struct<float> GetProjectionMatrix() const;



			protected:

				Vector3D m_Postion;
				Vector3D m_Focus;
				Vector3D m_Up;

				float m_ScreenAspectRatio;
				float m_NearPlane;
				float m_FarPlane;
				float m_FieldOfViewRadians;

				void Init();
			};

		}
	}
}