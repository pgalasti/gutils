#pragma once

#pragma region deprecated
#ifdef DEPRECATED_ENGINE_CLASS

#include "Engine.h"
#include "VectorDirectx.h"
#include "Model.h"
using namespace GUtils::Graphics;

SimpleEngine::SimpleEngine()
{
	m_pDevice = nullptr;
	m_pDeviceContext = nullptr;
	m_pSwapChain = nullptr;
	m_pRenderTargetView = nullptr;
	m_pDepthStencilBuffer = nullptr;
	m_pDepthStencilState = nullptr;
	m_pDepthStencilView = nullptr;
	m_pRasterState = nullptr;
}

SimpleEngine::~SimpleEngine()
{
	Shutdown();
}

BOOL SimpleEngine::Initialize(HWND hwnd, const EngineOptions& options)
{
	const UINT SCREEN_WIDTH = options.width;
	const UINT SCREEN_HEIGHT = options.height;
	const float SCREEN_NEAR = options.screenNear;
	const float SCREEN_FAR = options.screenDepth;
	const bool V_SYNC_ENABLED = options.VSync;
	const BOOL FULL_SCREEN = options.FullScreen;

	IDXGIFactory* pFactory;
	if (FAILS(CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&pFactory)))
		return FALSE;

	IDXGIAdapter* pAdapter;
	if (FAILS(pFactory->EnumAdapters(0, &pAdapter)))
		return FALSE;

	IDXGIOutput* pAdapterOutput;
	if (FAILS(pAdapter->EnumOutputs(0, &pAdapterOutput)))
		return FALSE;

	unsigned int nModes;
	if (FAILS(pAdapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &nModes, NULL)))
		return FALSE;

	DXGI_MODE_DESC* pDisplayModes;
	pDisplayModes = new DXGI_MODE_DESC[nModes];

	if (FAILS(pAdapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &nModes, pDisplayModes)))
		return FALSE;

	unsigned int numerator, denominator;
	for (unsigned int i = 0; i < nModes; i++)
	{
		if (pDisplayModes[i].Width == SCREEN_WIDTH)
		{
			if (pDisplayModes[i].Height == (unsigned int)SCREEN_HEIGHT)
			{
				numerator = pDisplayModes[i].RefreshRate.Numerator;
				denominator = pDisplayModes[i].RefreshRate.Denominator;
			}
		}
	}

	DXGI_ADAPTER_DESC adapterDesc;
	if (FAILS(pAdapter->GetDesc(&adapterDesc)))
		return FALSE;

	m_nVideoMemory = (int)(adapterDesc.DedicatedVideoMemory / 1024 / 1024);
	std::wstring wStr(adapterDesc.Description);
	m_VideoCardDesc.assign(wStr.begin(), wStr.end());

	SAFE_ARR_DELETE(pDisplayModes);
	COM_RELEASE(pAdapterOutput);
	COM_RELEASE(pAdapter);
	COM_RELEASE(pFactory);
	
	D3D_FEATURE_LEVEL featureLevels[] =
	{
		D3D_FEATURE_LEVEL_11_0,
		D3D_FEATURE_LEVEL_10_1,
		D3D_FEATURE_LEVEL_10_0,
	};
	UINT numFeatureLevels = ARRAYSIZE(featureLevels);

	// Swap chain
	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	ZeroMemory(&swapChainDesc, sizeof DXGI_SWAP_CHAIN_DESC);
	swapChainDesc.BufferCount = 1;
	swapChainDesc.BufferDesc.Width = SCREEN_WIDTH;
	swapChainDesc.BufferDesc.Height = SCREEN_HEIGHT;
	swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.OutputWindow = hwnd;
	swapChainDesc.SampleDesc.Count = 1;		// TODO
	swapChainDesc.SampleDesc.Quality = 0;	// TODO
	swapChainDesc.Windowed = !FULL_SCREEN;
	swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
	//swapChainDesc.Flags

	if (V_SYNC_ENABLED)
	{
		swapChainDesc.BufferDesc.RefreshRate.Numerator = numerator;
		swapChainDesc.BufferDesc.RefreshRate.Denominator = denominator;
	}
	else
	{
		swapChainDesc.BufferDesc.RefreshRate.Numerator = 0;
		swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	}

	D3D_FEATURE_LEVEL wantedFeatureLevel = D3D_FEATURE_LEVEL_11_0;
	UINT createDeviceFlags = 0;
#ifdef _DEBUG
	createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
#endif

	HRESULT result = D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, createDeviceFlags, featureLevels, numFeatureLevels,
		D3D11_SDK_VERSION, &swapChainDesc, &m_pSwapChain, &m_pDevice, &wantedFeatureLevel, &m_pDeviceContext);

	if (FAILED(result))
		return FALSE;

	ID3D11Texture2D* pBackBuffer;
	if (FAILS(m_pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer)))
		return FALSE;

	if (FAILS(m_pDevice->CreateRenderTargetView(pBackBuffer, NULL, &m_pRenderTargetView)))
		return FALSE;

	COM_RELEASE(pBackBuffer);


	D3D11_TEXTURE2D_DESC depthBufferDesc;
	ZeroMemory(&depthBufferDesc, sizeof D3D11_TEXTURE2D_DESC);
	depthBufferDesc.Width = SCREEN_WIDTH;
	depthBufferDesc.Height = SCREEN_HEIGHT;
	depthBufferDesc.MipLevels = 1;
	depthBufferDesc.ArraySize = 1;
	depthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthBufferDesc.SampleDesc.Count = 1;	// TODO
	depthBufferDesc.SampleDesc.Quality = 0; // TODO
	depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	depthBufferDesc.CPUAccessFlags = 0;
	depthBufferDesc.MiscFlags = 0;

	if (FAILS(m_pDevice->CreateTexture2D(&depthBufferDesc, NULL, &m_pDepthStencilBuffer)))
		return FALSE;


	D3D11_DEPTH_STENCIL_DESC depthStencilDesc;
	ZeroMemory(&depthStencilDesc, sizeof D3D11_DEPTH_STENCIL_DESC);
	depthStencilDesc.DepthEnable = true;
	depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
	depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
	depthStencilDesc.StencilEnable = true;
	depthStencilDesc.StencilReadMask = 0xFF;
	depthStencilDesc.StencilWriteMask = 0xFF;
	depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
	depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
	depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
	depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

	if (FAILS(m_pDevice->CreateDepthStencilState(&depthStencilDesc, &m_pDepthStencilState)))
		return FALSE;

	m_pDeviceContext->OMSetDepthStencilState(m_pDepthStencilState, 1);


	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
	ZeroMemory(&depthStencilViewDesc, sizeof D3D11_DEPTH_STENCIL_VIEW_DESC);
	depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
	depthStencilViewDesc.Texture2D.MipSlice = 0;

	if (FAILS(m_pDevice->CreateDepthStencilView(m_pDepthStencilBuffer, &depthStencilViewDesc, &m_pDepthStencilView)))
		return FALSE;

	m_pDeviceContext->OMSetRenderTargets(1, &m_pRenderTargetView, m_pDepthStencilView);


	D3D11_RASTERIZER_DESC rasterDesc;
	ZeroMemory(&rasterDesc, sizeof D3D11_RASTERIZER_DESC);
	rasterDesc.AntialiasedLineEnable = false;
	rasterDesc.CullMode = D3D11_CULL_BACK;
	rasterDesc.DepthBias = 0;
	rasterDesc.DepthBiasClamp = 0.0f;
	rasterDesc.DepthClipEnable = true;
	rasterDesc.FillMode = D3D11_FILL_SOLID;
	rasterDesc.FrontCounterClockwise = false;
	rasterDesc.MultisampleEnable = false;
	rasterDesc.ScissorEnable = false;
	rasterDesc.SlopeScaledDepthBias = 0.0f;

	if (FAILS(m_pDevice->CreateRasterizerState(&rasterDesc, &m_pRasterState)))
		return FALSE;

	m_pDeviceContext->RSSetState(m_pRasterState);


	D3D11_VIEWPORT viewport;
	ZeroMemory(&viewport, sizeof D3D11_VIEWPORT);

	float fieldOfView, screenAspect;
	viewport.Width = (float)SCREEN_WIDTH;
	viewport.Height = (float)SCREEN_HEIGHT;
	viewport.MinDepth = 0.0f;
	viewport.MaxDepth = 1.0f;
	viewport.TopLeftX = 0.0f;
	viewport.TopLeftY = 0.0f;

	m_pDeviceContext->RSSetViewports(1, &viewport);


	fieldOfView = (float)XM_PI / 4.0f;
	screenAspect = (float)SCREEN_WIDTH / (float)SCREEN_HEIGHT;
	m_ProjectionMatrix = XMMatrixPerspectiveFovLH(fieldOfView, screenAspect, SCREEN_NEAR, SCREEN_FAR);

	m_WorldMatrix = XMMatrixIdentity();
	m_OrthoMatrix = XMMatrixOrthographicLH((float)SCREEN_WIDTH, (float)SCREEN_HEIGHT, SCREEN_NEAR, SCREEN_FAR);

	m_Timer.Reset();
	m_Timer.Start();

	return TRUE;
}

void SimpleEngine::Shutdown()
{
	if (m_pSwapChain)
		m_pSwapChain->SetFullscreenState(false, NULL);
	COM_RELEASE(m_pRasterState);
	COM_RELEASE(m_pDepthStencilView);
	COM_RELEASE(m_pDepthStencilState);
	COM_RELEASE(m_pDepthStencilBuffer);
	COM_RELEASE(m_pRenderTargetView);
	COM_RELEASE(m_pSwapChain);
	COM_RELEASE(m_pDeviceContext);
	COM_RELEASE(m_pDevice);
}

BOOL SimpleEngine::Render()
{
	float ClearColor[4] = { 0.0f, 0.125f, 0.3f, 1.0f }; // red,green,blue,alpha

	m_pDeviceContext->ClearRenderTargetView(m_pRenderTargetView, ClearColor);
	m_pDeviceContext->ClearDepthStencilView(m_pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);
	m_pSwapChain->Present(0, 0);

	GUtils::Graphics::SimpleVertex vertex1;
	vertex1.Color.x = 1.0f;
	vertex1.Pos.x = -0.5f;
	
	GUtils::Graphics::SimpleVertex vertex2;
	vertex2.Color.x = 1.0f;
	vertex2.Pos.y = 1.0f;

	GUtils::Graphics::SimpleVertex vertex3;
	vertex3.Color.x = 1.0f;
	vertex3.Pos.y = 0.50f;

	GUtils::Graphics::SimpleVertex verticies[3];
	verticies[0] = vertex1;
	verticies[1] = vertex2;
	verticies[2] = vertex3;
	
	UINT indicies[3];
	indicies[0] = 1;
	indicies[1] = 2;
	indicies[2] = 3;

	SimpleModel model;
	if (!model.InitializeModel(m_pDevice, verticies, 3, indicies))
		return FALSE;
	model.Render(m_pDeviceContext);

	m_Timer.Tick();
	float deltaTime = m_Timer.DeltaTime();

	return TRUE;
}

#endif
#pragma endregion deprecated