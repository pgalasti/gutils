//#pragma once
//#include "DirectX11Engine.h"
//
//#include <memory>
//#include<set>
//
//#include <d3dcompiler.h>
//
//#include "IDGenerator.h"
//#include "ResourceDirectories.h"
//#include "ShapeBuilder.h"
//#include "ObjFormatLoader.h"
//#include "GModelLoader.h"
//#include "Metrics.h"
//#include "DirectXMathConversions.h"
//#include "DDSTextureLoader.h"
//
//using namespace GUtils::Graphics;
//using namespace GUtils::Concurrency;
//
////#define VERBOSE_DIRECTX_LOG
//
//DirectX11Engine::DirectX11Engine() : IGraphicEngine()
//{
//	m_pDevice = nullptr;
//	m_pDeviceContext = nullptr;
//	m_pSwapChain = nullptr;
//	m_pRenderTargetView = nullptr;
//	m_pDepthStencilBuffer = nullptr;
//	m_pDepthStencilState = nullptr;
//	m_pDepthStencilView = nullptr;
//	m_pRenderer = nullptr;
//
//	m_nVideoMemory = 0;
//
//	std::string loggingPath = ResourceDirectories::GetLoggingPath();
//	loggingPath.append("DirectX11Engine_log.txt");
//
//	m_bShutdown = true;
//
//	INITIALIZE_LOG(loggingPath);
//}
//
//DirectX11Engine::~DirectX11Engine()
//{
//	this->Shutdown();
//
//	DELETE_LOG;
//}
//
//BOOL DirectX11Engine::Initialize(HWND hwnd, const EngineOptions& options)
//{
//	DEBUG_LOG(LOG, "DirectX11Engine::Initialize()");
//
//	// Get the adapter and refresh parameters.
//	DXGI_ADAPTER_DESC adapterDescription;
//	DXGI_RATIONAL refreshRate;
//	if (!LoadDisplayAdapter(options, adapterDescription, refreshRate))
//		return FALSE;
//
//	// Load the device, immediate context, swapchain.
//	if (!LoadDeviceAndSwapChain(hwnd, options, refreshRate))
//		return FALSE;
//
//	// Load the depth stencil buffer and state.
//	if (!LoadDepthStencilBufferAndState(options))
//		return FALSE;
//
//	// Set the viewport
//	D3D11_VIEWPORT viewport;
//	if (!LoadViewport(options, viewport))
//		return FALSE;
//
//	// Load our rasterizer states
//	if (!LoadRasterizerStates())
//		return FALSE;
//
//	// Load our shaders
//	if (!LoadShaders())
//		return FALSE;
//
//	// Setup constant buffers to be used
//	if (!LoadConstantBuffers())
//		return FALSE;
//
//	// Set our camera up
//	if (!InitializeCamera(options))
//		return FALSE;
//
//	if (!LoadSamplers())
//		return FALSE;
//
//	// Default to solid rasterizer state
//	ID3D11RasterizerState* pDefaultState = m_RasterizerStateMap["RASTERIZER_SOLID_CULL_BACK"];
//	m_pDeviceContext->RSSetState(pDefaultState);
//
//	// Initialize ID generators for registering resources with the engine.
//	m_pMeshIdGenerator = std::make_unique<IDGenerator>(1);
//	m_pDirectionalLightIdGenerator = std::make_unique<IDGenerator>(1);
//	m_pPointLightIdGenerator = std::make_unique<IDGenerator>(1);
//	m_pSpotLightIdGenerator = std::make_unique<IDGenerator>(1);
//
//	m_RegisteredModelToVSVector.reserve(100);
//	m_RegisteredModelToVSVector.resize(100);
//	m_RegisteredModelToPSVector.reserve(100);
//	m_RegisteredModelToPSVector.resize(100);
//
//	m_EngineTimer.Reset();
//	m_EngineTimer.Start();
//
//	m_bShutdown = false;
//
//	m_bMultithreadingSupport = true; // for now
//	if (m_bMultithreadingSupport)
//	{
//		const ubyte8 NUM_OF_CORES = 4; // Get this dynamically
//		for (ubyte8 i = 0; i < NUM_OF_CORES; i++)
//		{
//			ID3D11DeviceContext* pDeferredContext = nullptr;
//			if (FAILED(m_pDevice->CreateDeferredContext(NULL, &pDeferredContext)))
//			{
//				// Failure message here
//				return FALSE;
//			}
//
//			pDeferredContext->OMSetDepthStencilState(m_pDepthStencilState, 1);
//			pDeferredContext->OMSetRenderTargets(1, &m_pRenderTargetView, m_pDepthStencilView);
//			pDeferredContext->RSSetViewports(1, &viewport);
//			pDeferredContext->RSSetState(pDefaultState);
//			m_DeferredContextList.push_back(pDeferredContext);
//		}
//	}
//
//	return TRUE;
//}
//
//BOOL DirectX11Engine::LoadDisplayAdapter(const EngineOptions& options, DXGI_ADAPTER_DESC& adapterDescription, DXGI_RATIONAL& refreshRate)
//{
//	IDXGIFactory* pFactory;
//	if (FAILS(CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&pFactory)))
//		return FALSE;
//
//	IDXGIAdapter* pAdapter;
//	if (FAILS(pFactory->EnumAdapters(0, &pAdapter)))
//		return FALSE;
//
//	IDXGIOutput* pAdapterOutput;
//	if (FAILS(pAdapter->EnumOutputs(0, &pAdapterOutput)))
//		return FALSE;
//
//	uint32 nModes;
//	if (FAILS(pAdapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &nModes, NULL)))
//		return FALSE;
//
//	DXGI_MODE_DESC* pDisplayModes;
//	pDisplayModes = new DXGI_MODE_DESC[nModes];
//
//	if (FAILS(pAdapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &nModes, pDisplayModes)))
//		return FALSE;
//
//	const UINT SCREEN_WIDTH = options.width;
//	const UINT SCREEN_HEIGHT = options.height;
//
//	for (uint32 i = 0; i < nModes; i++)
//	{
//		if (pDisplayModes[i].Width == SCREEN_WIDTH)
//		{
//			if (pDisplayModes[i].Height == (uint32)SCREEN_HEIGHT)
//				refreshRate = pDisplayModes[i].RefreshRate;
//		}
//	}
//
//	if (FAILS(pAdapter->GetDesc(&adapterDescription)))
//	{
//		// error
//		return FALSE;
//	}
//
//	SAFE_ARR_DELETE(pDisplayModes);
//	COM_RELEASE(pAdapterOutput);
//	COM_RELEASE(pAdapter);
//	COM_RELEASE(pFactory);
//
//	// Set some member parameters from the adapter
//	m_nVideoMemory = (int)(adapterDescription.DedicatedVideoMemory / 1024 / 1024);
//	std::wstring wStr(adapterDescription.Description);
//	m_VideoCardDesc.assign(wStr.begin(), wStr.end());
//	wStr.clear();
//
//	return TRUE;
//}
//
//BOOL DirectX11Engine::LoadDeviceAndSwapChain(HWND hwnd, const EngineOptions& options, const DXGI_RATIONAL& refreshRate)
//{
//	D3D_FEATURE_LEVEL featureLevels[] =
//	{
//		D3D_FEATURE_LEVEL_11_0,
//		D3D_FEATURE_LEVEL_10_1,
//		D3D_FEATURE_LEVEL_10_0,
//	};
//	UINT numFeatureLevels = ARRAYSIZE(featureLevels);
//
//	const UINT SCREEN_WIDTH = options.width;
//	const UINT SCREEN_HEIGHT = options.height;
//	const bool V_SYNC_ENABLED = options.VSync;
//	const BOOL FULL_SCREEN = options.FullScreen;
//
//	// Swap chain
//	DXGI_SWAP_CHAIN_DESC swapChainDesc;
//	ZeroMemory(&swapChainDesc, sizeof DXGI_SWAP_CHAIN_DESC);
//	swapChainDesc.BufferCount = 1;
//	swapChainDesc.BufferDesc.Width = SCREEN_WIDTH;
//	swapChainDesc.BufferDesc.Height = SCREEN_HEIGHT;
//	swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
//	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
//	swapChainDesc.OutputWindow = hwnd;
//	swapChainDesc.SampleDesc.Count = 1;		// TODO
//	swapChainDesc.SampleDesc.Quality = 0;	// TODO
//	swapChainDesc.Windowed = !FULL_SCREEN;
//	swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
//	swapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
//	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
//
//	if (V_SYNC_ENABLED)
//	{
//		swapChainDesc.BufferDesc.RefreshRate.Numerator = refreshRate.Numerator;
//		swapChainDesc.BufferDesc.RefreshRate.Denominator = refreshRate.Denominator;
//	}
//	else
//	{
//		swapChainDesc.BufferDesc.RefreshRate.Numerator = 0;
//		swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
//	}
//
//	D3D_FEATURE_LEVEL wantedFeatureLevel = D3D_FEATURE_LEVEL_11_0;
//	UINT createDeviceFlags = 0;
//#ifdef _DEBUG
//	createDeviceFlags |= D3D11_CREATE_DEVICE_DEBUG;
//#endif
//
//	HRESULT result = D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, createDeviceFlags, featureLevels, numFeatureLevels,
//		D3D11_SDK_VERSION, &swapChainDesc, &m_pSwapChain, &m_pDevice, &wantedFeatureLevel, &m_pDeviceContext);
//
//	if (FAILED(result))
//		return FALSE;
//
//	ID3D11Texture2D* pBackBuffer;
//	if (FAILS(m_pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuffer)))
//		return FALSE;
//
//	if (FAILS(m_pDevice->CreateRenderTargetView(pBackBuffer, NULL, &m_pRenderTargetView)))
//		return FALSE;
//
//	COM_RELEASE(pBackBuffer);
//
//	return TRUE;
//}
//
//BOOL DirectX11Engine::LoadDepthStencilBufferAndState(const EngineOptions& options)
//{
//	const UINT SCREEN_WIDTH = options.width;
//	const UINT SCREEN_HEIGHT = options.height;
//
//	D3D11_TEXTURE2D_DESC depthBufferDesc;
//	ZeroMemory(&depthBufferDesc, sizeof D3D11_TEXTURE2D_DESC);
//	depthBufferDesc.Width = SCREEN_WIDTH;
//	depthBufferDesc.Height = SCREEN_HEIGHT;
//	depthBufferDesc.MipLevels = 1;
//	depthBufferDesc.ArraySize = 1;
//	depthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
//	depthBufferDesc.SampleDesc.Count = 1;	// TODO
//	depthBufferDesc.SampleDesc.Quality = 0; // TODO
//	depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
//	depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
//	depthBufferDesc.CPUAccessFlags = 0;
//	depthBufferDesc.MiscFlags = 0;
//
//	if (FAILS(m_pDevice->CreateTexture2D(&depthBufferDesc, NULL, &m_pDepthStencilBuffer)))
//		return FALSE;
//
//	D3D11_DEPTH_STENCIL_DESC depthStencilDesc;
//	ZeroMemory(&depthStencilDesc, sizeof D3D11_DEPTH_STENCIL_DESC);
//	depthStencilDesc.DepthEnable = true;
//	depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
//	depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
//	depthStencilDesc.StencilEnable = true;
//	depthStencilDesc.StencilReadMask = 0xFF;
//	depthStencilDesc.StencilWriteMask = 0xFF;
//	depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
//	depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
//	depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
//	depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
//	depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
//	depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
//	depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
//	depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
//
//	if (FAILS(m_pDevice->CreateDepthStencilState(&depthStencilDesc, &m_pDepthStencilState)))
//		return FALSE;
//
//	m_pDeviceContext->OMSetDepthStencilState(m_pDepthStencilState, 1);
//
//	D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
//	ZeroMemory(&depthStencilViewDesc, sizeof D3D11_DEPTH_STENCIL_VIEW_DESC);
//	depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
//	depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
//	depthStencilViewDesc.Texture2D.MipSlice = 0;
//
//	if (FAILS(m_pDevice->CreateDepthStencilView(m_pDepthStencilBuffer, &depthStencilViewDesc, &m_pDepthStencilView)))
//		return FALSE;
//
//	m_pDeviceContext->OMSetRenderTargets(1, &m_pRenderTargetView, m_pDepthStencilView);
//
//	return TRUE;
//}
//
//BOOL DirectX11Engine::LoadViewport(const EngineOptions& options, D3D11_VIEWPORT& viewport)
//{
//	const UINT SCREEN_WIDTH = options.width;
//	const UINT SCREEN_HEIGHT = options.height;
//
//	ZeroMemory(&viewport, sizeof D3D11_VIEWPORT);
//	viewport.Width = (float)SCREEN_WIDTH;
//	viewport.Height = (float)SCREEN_HEIGHT;
//	viewport.MinDepth = 0.0f;
//	viewport.MaxDepth = 1.0f;
//	viewport.TopLeftX = 0.0f;
//	viewport.TopLeftY = 0.0f;
//	m_pDeviceContext->RSSetViewports(1, &viewport);
//
//	return TRUE;
//}
//
//// TODO script or options to load available rasterizer states?
//BOOL DirectX11Engine::LoadRasterizerStates()
//{
//	DEBUG_LOG(LOG, "DirectX Engine loading rasterizer states.");
//
//	ID3D11RasterizerState* pRasterizerState = nullptr;
//
//	D3D11_RASTERIZER_DESC rasterDesc;
//	ZeroMemory(&rasterDesc, sizeof D3D11_RASTERIZER_DESC);
//
//	rasterDesc.AntialiasedLineEnable = false;
//	rasterDesc.CullMode = D3D11_CULL_BACK;
//	rasterDesc.DepthBias = 0;
//	rasterDesc.DepthBiasClamp = 0.0f;
//	rasterDesc.DepthClipEnable = true;
//	rasterDesc.FillMode = D3D11_FILL_SOLID;
//	rasterDesc.FrontCounterClockwise = false;
//	rasterDesc.MultisampleEnable = false;
//	rasterDesc.ScissorEnable = false;
//	rasterDesc.SlopeScaledDepthBias = 0.0f;
//	if (FAILS(m_pDevice->CreateRasterizerState(&rasterDesc, &pRasterizerState)))
//	{
//		DEBUG_LOG(LOG, "CRITICAL FAILURE - DirectX Engine failed to create rasterizer state D3D11_CULL_BACK & D3D11_FILL_SOLID.");
//		return FALSE;
//	}
//
//	m_RasterizerStateMap["RASTERIZER_SOLID_CULL_BACK"] = pRasterizerState;
//
//	rasterDesc.FillMode = D3D11_FILL_WIREFRAME;
//	if (FAILS(m_pDevice->CreateRasterizerState(&rasterDesc, &pRasterizerState)))
//	{
//		DEBUG_LOG(LOG, "CRITICAL FAILURE - DirectX Engine failed to create rasterizer state D3D11_CULL_BACK & D3D11_FILL_WIREFRAME.");
//		return FALSE;
//	}
//
//	m_RasterizerStateMap["RASTERIZER_WIREFRAME_CULL_BACK"] = pRasterizerState;
//
//	return TRUE;
//}
//
//// TODO I would like to load shaders using hooks from scripts to make it more dynamic in future.
//// For now I will just load shaders statically in code, but this is something I should implement
//// once I have the basics down.
//BOOL DirectX11Engine::LoadShaders()
//{
//	DEBUG_LOG(LOG, "DirectX Engine loading precompiled shader objects.");
//
//	if (!LoadVertexShaderFiles())
//		return FALSE;
//
//	DEBUG_LOG(LOG, "DirectX Engine loaded vertex shaders.");
//
//	if (!LoadPixelShaderFiles())
//		return TRUE;
//
//	DEBUG_LOG(LOG, "DirectX Engine loaded pixel shaders.");
//
//	return TRUE;
//}
//
//BOOL DirectX11Engine::LoadConstantBuffers()
//{
//	// Create the constant buffers to be used for all objects rendered to screen
//	D3D11_BUFFER_DESC bd;
//	ZeroMemory(&bd, sizeof(bd));
//	bd.Usage = D3D11_USAGE_DYNAMIC;
//	bd.ByteWidth = sizeof(ConstantBuffer);
//	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
//	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
//	HRESULT hr = m_pDevice->CreateBuffer(&bd, NULL, &m_pPerObjectConstantBuffer);
//	if (FAILED(hr))
//		return FALSE;
//
//	bd.ByteWidth = sizeof(PerFrameConstantBuffer);
//	hr = m_pDevice->CreateBuffer(&bd, NULL, &m_pPerFrameConstantBuffer);
//	if (FAILED(hr))
//		return FALSE;
//
//	return TRUE;
//}
//
//BOOL DirectX11Engine::InitializeCamera(const EngineOptions& options)
//{
//	const UINT SCREEN_WIDTH = options.width;
//	const UINT SCREEN_HEIGHT = options.height;
//	const float SCREEN_NEAR = options.screenNear;
//	const float SCREEN_FAR = options.screenDepth;
//
//	// Set our default camera position.
//	float fieldOfView, screenAspect;
//	fieldOfView = (float)XM_PI / 2.0f;
//	screenAspect = (float)SCREEN_WIDTH / (float)SCREEN_HEIGHT;
//
//	m_Camera.SetPosition(Vector3D(0.0f, 0.0f, -10.0f));
//	m_Camera.SetFocus(Vector3D(0.0f, 0.0f, 0.0f));
//	m_Camera.SetUp(Vector3D(0.0f, 1.0f, 0.0f));
//
//	m_Camera.SetAspectRatio(screenAspect);
//	m_Camera.SetFarPlane(SCREEN_FAR);
//	m_Camera.SetNearPlane(SCREEN_NEAR);
//	m_Camera.SetFieldOfViewRadians(fieldOfView);
//
//	return TRUE;
//}
//
//BOOL DirectX11Engine::LoadSamplers()
//{
//	D3D11_SAMPLER_DESC sampDesc;
//	ZeroMemory(&sampDesc, sizeof(sampDesc));
//	sampDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
//	sampDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
//	sampDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
//	sampDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
//	sampDesc.ComparisonFunc = D3D11_COMPARISON_NEVER;
//	sampDesc.MinLOD = 0;
//	sampDesc.MaxLOD = D3D11_FLOAT32_MAX;
//	ID3D11SamplerState* pSamplerLinear;
//	HRESULT hr = m_pDevice->CreateSamplerState(&sampDesc, &pSamplerLinear);
//	if (FAILED(hr))
//		return FALSE;
//
//	// Default sampler
//	m_SamplerStateMap["SAMPLER_LINEAR"] = pSamplerLinear;
//
//	return TRUE;
//}
//
//BOOL DirectX11Engine::LoadVertexShaderFiles()
//{
//	// Get our shader path precompiled shaders will be found.
//	const std::wstring SHADER_PATH = ResourceDirectories::GetBinaryPath();
//	ID3DBlob* pObjectBlob = nullptr;
//
//	// Load basic vertex shader
//	std::wstring vertexShaderPath(SHADER_PATH);
//	vertexShaderPath.append(L"test_VS.cso");
//	if (FAILED(D3DReadFileToBlob(vertexShaderPath.c_str(), &pObjectBlob)))
//	{
//		DEBUG_LOG(LOG, "CRITICAL FAILURE - DirectX Engine D3DReadFileToBlob test_VS.cso ");
//		return FALSE;
//	}
//
//	// Create our vertex shader and input layout interfaces
//	ID3D11VertexShader* pVertexShader = nullptr;
//	ID3D11InputLayout* pInputLayout = nullptr;
//	if (FAILED(m_pDevice->CreateVertexShader(pObjectBlob->GetBufferPointer(), pObjectBlob->GetBufferSize(), NULL, &pVertexShader)))
//	{
//		DEBUG_LOG(LOG, "CRITICAL FAILURE - DirectX Engine CreateVertexShader test_VS.cso failure");
//		return FALSE;
//	}
//
//	D3D11_INPUT_ELEMENT_DESC layout[] =
//	{
//		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
//		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
//	};
//	UINT numElements = ARRAYSIZE(layout);
//
//	if (FAILED(m_pDevice->CreateInputLayout(layout, numElements, pObjectBlob->GetBufferPointer(), pObjectBlob->GetBufferSize(), &pInputLayout)))
//	{
//		DEBUG_LOG(LOG, "CRITICAL FAILURE - DirectX Engine CreateInputLayout test_VS.cso failure");
//		return FALSE;
//	}
//
//	COM_RELEASE(pObjectBlob);
//
//	// Store the interfaces in a map object
//	m_VertexShaderMap["test_VS"].first = pVertexShader;
//	m_VertexShaderMap["test_VS"].second = pInputLayout;
//
//	// Light shader start
//	vertexShaderPath = SHADER_PATH;
//	vertexShaderPath.append(L"MatLightVS.cso");
//	if (FAILED(D3DReadFileToBlob(vertexShaderPath.c_str(), &pObjectBlob)))
//	{
//		DEBUG_LOG(LOG, "CRITICAL FAILURE - DirectX Engine D3DReadFileToBlob MatLightVS.cso ");
//		return FALSE;
//	}
//
//	pVertexShader = nullptr;
//	pInputLayout = nullptr;
//	if (FAILED(m_pDevice->CreateVertexShader(pObjectBlob->GetBufferPointer(), pObjectBlob->GetBufferSize(), NULL, &pVertexShader)))
//	{
//		DEBUG_LOG(LOG, "CRITICAL FAILURE - DirectX Engine CreateVertexShader MatLightVS.cso failure");
//		return FALSE;
//	}
//
//	D3D11_INPUT_ELEMENT_DESC lightLayout[] =
//	{
//		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
//		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
//	};
//	numElements = ARRAYSIZE(lightLayout);
//
//	if (FAILED(m_pDevice->CreateInputLayout(lightLayout, numElements, pObjectBlob->GetBufferPointer(), pObjectBlob->GetBufferSize(), &pInputLayout)))
//	{
//		DEBUG_LOG(LOG, "CRITICAL FAILURE - DirectX Engine CreateInputLayout MatLightVS.cso failure");
//		return FALSE;
//	}
//
//	COM_RELEASE(pObjectBlob);
//
//	m_VertexShaderMap["MatLightVS"].first = pVertexShader;
//	m_VertexShaderMap["MatLightVS"].second = pInputLayout;
//
//	// Texture light shader start
//	vertexShaderPath = SHADER_PATH;
//	vertexShaderPath.append(L"TexLightVS.cso");
//	if (FAILED(D3DReadFileToBlob(vertexShaderPath.c_str(), &pObjectBlob)))
//	{
//		DEBUG_LOG(LOG, "CRITICAL FAILURE - DirectX Engine D3DReadFileToBlob MatLightVS.cso ");
//		return FALSE;
//	}
//
//	pVertexShader = nullptr;
//	pInputLayout = nullptr;
//	if (FAILED(m_pDevice->CreateVertexShader(pObjectBlob->GetBufferPointer(), pObjectBlob->GetBufferSize(), NULL, &pVertexShader)))
//	{
//		DEBUG_LOG(LOG, "CRITICAL FAILURE - DirectX Engine CreateVertexShader TexLightVS.cso failure");
//		return FALSE;
//	}
//
//	D3D11_INPUT_ELEMENT_DESC lightTexLayout[] =
//	{
//		{ "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
//		{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 },
//		{ "TEXTCOORD", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 24, D3D11_INPUT_PER_VERTEX_DATA, 0 },
//	};
//	numElements = ARRAYSIZE(lightTexLayout);
//
//	if (FAILED(m_pDevice->CreateInputLayout(lightTexLayout, numElements, pObjectBlob->GetBufferPointer(), pObjectBlob->GetBufferSize(), &pInputLayout)))
//	{
//		DEBUG_LOG(LOG, "CRITICAL FAILURE - DirectX Engine CreateInputLayout TexLightVS.cso failure");
//		return FALSE;
//	}
//
//	COM_RELEASE(pObjectBlob);
//
//	m_VertexShaderMap["TexLightVS"].first = pVertexShader;
//	m_VertexShaderMap["TexLightVS"].second = pInputLayout;
//
//	return TRUE;
//}
//
//BOOL DirectX11Engine::LoadPixelShaderFiles()
//{
//	// Get our shader path precompiled shaders will be found.
//	const std::wstring SHADER_PATH = ResourceDirectories::GetBinaryPath();
//	ID3DBlob* pObjectBlob = nullptr;
//
//	// Load basic pixel shader
//	std::wstring pixelShaderPath(SHADER_PATH);
//	pixelShaderPath.append(L"test_PS.cso");
//	if (FAILED(D3DReadFileToBlob(pixelShaderPath.c_str(), &pObjectBlob)))
//	{
//		DEBUG_LOG(LOG, "CRITICAL FAILURE - DirectX Engine D3DReadFileToBlob test_PS.cso failure");
//		return FALSE;
//	}
//
//	// Create our pixel shader interface
//	ID3D11PixelShader* pPixelShader = nullptr;
//	if (FAILED(m_pDevice->CreatePixelShader(pObjectBlob->GetBufferPointer(), pObjectBlob->GetBufferSize(), NULL, &pPixelShader)))
//	{
//		DEBUG_LOG(LOG, "CRITICAL FAILURE - DirectX Engine CreatePixelShader test_PS.cso ");
//		return FALSE;
//	}
//
//	// Store pixel interface in map
//	m_PixelShaderMap["test_PS"] = pPixelShader;
//
//	COM_RELEASE(pObjectBlob);
//	pixelShaderPath = SHADER_PATH;
//	pixelShaderPath.append(L"MatLightPS.cso");
//	if (FAILED(D3DReadFileToBlob(pixelShaderPath.c_str(), &pObjectBlob)))
//	{
//		DEBUG_LOG(LOG, "CRITICAL FAILURE - DirectX Engine D3DReadFileToBlob MatLightPS.cso failure");
//		return FALSE;
//	}
//
//	// Create our pixel shader interface
//	pPixelShader = nullptr;
//	if (FAILED(m_pDevice->CreatePixelShader(pObjectBlob->GetBufferPointer(), pObjectBlob->GetBufferSize(), NULL, &pPixelShader)))
//	{
//		DEBUG_LOG(LOG, "CRITICAL FAILURE - DirectX Engine CreatePixelShader MatLightPS.cso ");
//		return FALSE;
//	}
//
//	m_PixelShaderMap["MatLightPS"] = pPixelShader;
//
//	COM_RELEASE(pObjectBlob);
//
//	// Light texture
//	pixelShaderPath = SHADER_PATH;
//	pixelShaderPath.append(L"TexLightPS.cso");
//	if (FAILED(D3DReadFileToBlob(pixelShaderPath.c_str(), &pObjectBlob)))
//	{
//		DEBUG_LOG(LOG, "CRITICAL FAILURE - DirectX Engine D3DReadFileToBlob TexLightPS.cso failure");
//		return FALSE;
//	}
//
//	// Create our pixel shader interface
//	pPixelShader = nullptr;
//	if (FAILED(m_pDevice->CreatePixelShader(pObjectBlob->GetBufferPointer(), pObjectBlob->GetBufferSize(), NULL, &pPixelShader)))
//	{
//		DEBUG_LOG(LOG, "CRITICAL FAILURE - DirectX Engine CreatePixelShader TexLightPS.cso ");
//		return FALSE;
//	}
//
//	m_PixelShaderMap["TexLightPS"] = pPixelShader;
//
//	COM_RELEASE(pObjectBlob);
//
//	return TRUE;
//}
//
//BOOL DirectX11Engine::Shutdown()
//{
//	if (m_bShutdown)
//		return TRUE;
//
//	DEBUG_LOG(LOG, "DirectX11Engine::Shutdown()");
//
//	if (m_pDeviceContext)
//	{
//		m_pDeviceContext->ClearState();
//		m_pDeviceContext->Flush();
//	}
//
//	// Clean up our loaded vertex shaders
//	for (auto vertexShader : m_VertexShaderMap)
//	{
//		if (vertexShader.second.first == nullptr)
//			continue;
//
//		// Release VertexShader
//		COM_RELEASE(vertexShader.second.first);
//
//		if (vertexShader.second.second == nullptr)
//			continue;
//
//		// Release InputLayout
//		COM_RELEASE(vertexShader.second.second);
//	}
//	m_VertexShaderMap.clear();
//
//	// Clean up our loaded pixel shaders
//	for (auto pixelShader : m_PixelShaderMap)
//	{
//		COM_RELEASE(pixelShader.second);
//	}
//	m_PixelShaderMap.clear();
//
//	for (auto rasterizerState : m_RasterizerStateMap)
//	{
//		COM_RELEASE(rasterizerState.second);
//	}
//	m_RasterizerStateMap.clear();
//
//	for (auto sampler : m_SamplerStateMap)
//	{
//		COM_RELEASE(sampler.second);
//	}
//	m_SamplerStateMap.clear();
//
//	// Cleanup models
//	std::set<int> idsToRemove = m_RegisteredIds;
//	for (int id : idsToRemove)
//		this->DeregisterModel(id);
//
//	// Clean up lights
//	m_LoadedDirectionalLightVector.clear();
//	m_LoadedPointLightVector.clear();
//	m_LoadedSpotLightVector.clear();
//
//	m_pDirectionalLightIdGenerator->Reset(1);
//	m_pPointLightIdGenerator->Reset(1);
//	m_pSpotLightIdGenerator->Reset(1);
//
//	// Clean up materials
//	m_MaterialToRegisteredModelMap.clear();
//
//	// Clean up textures
//	std::set<uint32> textureIdsToRemove;
//	for (auto mapItem : m_TextureToRegisteredModelMap)
//		textureIdsToRemove.insert(mapItem.first);
//	for (auto id : textureIdsToRemove)
//		this->DeregisterTextureToModel(id);
//
//	if (m_bMultithreadingSupport)
//	{
//		for (ubyte8 i = 0; i < m_DeferredContextList.size(); i++)
//		{
//			COM_RELEASE(m_DeferredContextList[i]);
//		}
//		m_DeferredContextList.clear();
//	}
//
//	m_TextureToRegisteredModelMap.clear();
//
//	if (m_pSwapChain)
//		m_pSwapChain->SetFullscreenState(false, NULL);
//
//	SAFE_DELETE(m_pRenderer);
//	COM_RELEASE(m_pPerObjectConstantBuffer);
//	COM_RELEASE(m_pPerFrameConstantBuffer);
//	COM_RELEASE(m_pDepthStencilView);
//	COM_RELEASE(m_pDepthStencilState);
//	COM_RELEASE(m_pDepthStencilBuffer);
//	COM_RELEASE(m_pRenderTargetView);
//	COM_RELEASE(m_pSwapChain);
//	COM_RELEASE(m_pDeviceContext);
//
//#ifdef VERBOSE_DIRECTX_LOG
//	ID3D11Debug* pDebug = nullptr;
//	m_pDevice->QueryInterface(__uuidof(ID3D11Debug), reinterpret_cast<void**>(&pDebug));
//	pDebug->ReportLiveDeviceObjects(D3D11_RLDO_DETAIL);
//	COM_RELEASE(pDebug);
//#endif
//
//	COM_RELEASE(m_pDevice);
//
//	m_bShutdown = true;
//
//	return TRUE;
//}
//
//BOOL DirectX11Engine::Render(const float dt)
//{
//	// Clear the screen
//	ClearScreenToDefaultColor(m_pDeviceContext);
//
//	// Copy over lights
//	if (!RenderRegisteredLights())
//		return FALSE;
//
//	// Draw all loaded models, transformations, effects
//	if (!RenderRegisteredResources())
//		return FALSE;
//
//	// Render to screen
//	m_pSwapChain->Present(0, 0);
//	m_EngineTimer.Tick();
//	return TRUE;
//}
//
//BOOL DirectX11Engine::LoadPendingModel(const int resourceID)
//{
//	static uint32 totalVerticies = 0;
//	static uint32 totalIndicies = 0;
//
//	auto iter = m_LoadingMeshMap.find(resourceID);
//	m_LoadedMeshMap[resourceID] = std::move(iter->second.get());
//	m_MeshIds.insert(resourceID);
//
//	// Remove the pending mesh that have now been loaded
//	m_LoadingMeshMap.erase(resourceID);
//
//	auto pathIter = m_LoadingModelPath.find(resourceID);
//	m_LoadingModelPath.erase(pathIter);
//
//	return TRUE;
//}
//
//BOOL DirectX11Engine::RenderRegisteredResources()
//{
//	const XMMATRIX viewProjection = m_Camera.GetViewProjectionMatrix();
//
//	ConstantBuffer perModelBuffer;
//	IMesh* pMesh = nullptr;
//
//	m_nLoadedVerticies = 0;
//	m_nLoadedIndicies = 0;
//
//	ID3D11Buffer* constantBuffers[2];
//	constantBuffers[1] = m_pPerFrameConstantBuffer;
//
//	// TODO make this member
//	//ubyte8 coreCount = 8;
//	//std::vector<ID3D11DeviceContext*> m_DeferredContextList(coreCount);
//	std::vector<std::future<BOOL>> m_AsyncRenderOperations;
//	std::vector<ID3D11CommandList*> commandListList;
//	ubyte8 deferredContextIndex = 0;
//
//	// Per object, lighting has already been set
//
//	std::vector<ConstantBuffer> perModelConstantBufferPointers(m_ModelTransformations.size());
//
//	for (register uint32 i = 0; i < m_ModelTransformations.size(); i++)
//	{
//		ID3D11DeviceContext* pContextToUse = m_DeferredContextList[deferredContextIndex];
//
//		if (m_AsyncRenderOperations.size() == 4) // Need to get list
//		{
//			if (!m_AsyncRenderOperations[deferredContextIndex].get())
//				return FALSE;
//
//			ID3D11CommandList* pCommandList = nullptr;
//			HRESULT debug = pContextToUse->FinishCommandList(TRUE, &pCommandList);
//			commandListList.push_back(pCommandList);
//		}
//
//		TransformOnModelKey_t transformation = m_ModelTransformations[i];
//		const int key = transformation.first;
//
//		// Set the vertex shader and input layout for the registered model
//		if (!SetRegisteredResourceVertexShaderAndLayout(key, pContextToUse))
//			return FALSE;
//
//		// Set the pixel shader for the registered model
//		if (!SetRegisteredResourcePixelShader(key, pContextToUse))
//			return FALSE;
//
//		// Load the material to the perModelBuffer if a material is registered for the model
//		Material material;
//		if (GetRegisteredResourceMaterial(key, material))
//			perModelBuffer.material = material;
//
//		// Load the texture resource and the sampler specified.
//		GetRegisteredResourceTexture(key, pContextToUse);
//
//		// Get the transformation
//		const ConstantBuffer modelTransformation = transformation.second;
//		perModelBuffer.world = modelTransformation.world;
//		perModelBuffer.worldViewProjection = modelTransformation.worldViewProjection;
//		perModelBuffer.worldInverseTranspose = modelTransformation.worldInverseTranspose;
//		//pPerModelBuffer->TextureTransformation = modelTransformation.TextureTransformation;
//
//		perModelBuffer.world = XMMatrixTranspose(perModelBuffer.worldViewProjection); // world is loaded into worldViewProjection at first.
//		perModelBuffer.worldViewProjection = XMMatrixTranspose(perModelBuffer.worldViewProjection * viewProjection);
//		perModelBuffer.worldInverseTranspose = InverseTranspose(perModelBuffer.world);
//		//pPerModelBuffer->TextureTransformation = XMMatrixTranspose(pPerModelBuffer->TextureTransformation);
//		// Find the mesh the transformation is trying to be placed on
//		pMesh = nullptr;
//		if (!GetRegisteredMesh(key, &pMesh))
//			return FALSE;
//
//		m_nLoadedVerticies += pMesh->GetNumberVerticies();
//		m_nLoadedIndicies += pMesh->GetNumberIndicies();
//
//		// TESTING MULTITHREADED LOGIC START
//		// ================================================================================================
//
//		perModelConstantBufferPointers[i] = perModelBuffer;
//		if (m_AsyncRenderOperations.size() < 4) // Will only hit this the first time it attempts to render
//		{
//			m_AsyncRenderOperations.push_back(std::async(&DirectX11Engine::AsyncMeshRender, this, pContextToUse, pMesh, perModelConstantBufferPointers[i]));
//		}
//		else
//		{
//			// We hit our core limit, we need to reuse our pool of deferred contexts. Force the get in case it's still running
//			m_AsyncRenderOperations[deferredContextIndex] = std::async(&DirectX11Engine::AsyncMeshRender, this, pContextToUse, pMesh, perModelConstantBufferPointers[i]);
//		}
//
//		++deferredContextIndex;
//		if (deferredContextIndex == 4)
//			deferredContextIndex = 0;
//
//
//		// ================================================================================================
//
//		// Map to GPU
//		//D3D11_MAPPED_SUBRESOURCE constantBufferResource;
//		//m_pDeviceContext->Map(m_pPerObjectConstantBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &constantBufferResource);
//		//memcpy(constantBufferResource.pData, &perModelBuffer, sizeof(ConstantBuffer));
//		//m_pDeviceContext->Unmap(m_pPerObjectConstantBuffer, 0);
//
//		//constantBuffers[0] = m_pPerObjectConstantBuffer;
//		//m_pDeviceContext->VSSetConstantBuffers(0, 1, &m_pPerObjectConstantBuffer);
//		//m_pDeviceContext->PSSetConstantBuffers(0, 2, constantBuffers);
//
//		// Render the loaded mesh
//		//pMesh->Render(m_pDeviceContext);
//	}
//
//	//Get the remaining operations
//	for (ubyte8 i = 0; i < 4; i++)
//	{
//		if (!m_AsyncRenderOperations[i].get())
//			return FALSE;
//
//		ID3D11DeviceContext* pContextToUse = m_DeferredContextList[i];
//		ID3D11CommandList* pCommandList = nullptr;
//		HRESULT debug = pContextToUse->FinishCommandList(TRUE, &pCommandList);
//		commandListList.push_back(pCommandList);
//	}
//	// Defered batch list can be finished here TODO
//	for (auto pCommandList : commandListList)
//	{
//		m_pDeviceContext->ExecuteCommandList(pCommandList, TRUE);
//		COM_RELEASE(pCommandList);
//	}
//
//	perModelConstantBufferPointers.clear();
//	// Transformations have been made
//	m_ModelTransformations.clear();
//
//	return TRUE;
//}
//
//BOOL DirectX11Engine::AsyncMeshRender(ID3D11DeviceContext* pContext, IMesh* pMesh, const ConstantBuffer& perModelBuffer)
//{
//	ID3D11Buffer* perObjectConstantBuffer = nullptr;
//	D3D11_BUFFER_DESC bd;
//	ZeroMemory(&bd, sizeof(bd));
//	bd.Usage = D3D11_USAGE_DYNAMIC;
//	bd.ByteWidth = sizeof(ConstantBuffer);
//	bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
//	bd.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
//	HRESULT hr = m_pDevice->CreateBuffer(&bd, NULL, &perObjectConstantBuffer);
//	if (FAILED(hr))
//		return FALSE;
//
//	D3D11_MAPPED_SUBRESOURCE constantBufferResource;
//	pContext->Map(perObjectConstantBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &constantBufferResource);
//	memcpy(constantBufferResource.pData, &perModelBuffer, sizeof(ConstantBuffer));
//	pContext->Unmap(perObjectConstantBuffer, 0);
//
//	ID3D11Buffer* constantBuffers[2];
//	constantBuffers[0] = perObjectConstantBuffer;
//	constantBuffers[1] = m_pPerFrameConstantBuffer;
//	pContext->VSSetConstantBuffers(0, 1, &perObjectConstantBuffer);
//	pContext->PSSetConstantBuffers(0, 2, constantBuffers);
//
//
//	pMesh->Render(pContext);
//
//	COM_RELEASE(perObjectConstantBuffer);
//
//	return TRUE;
//}
//
//BOOL DirectX11Engine::RenderRegisteredLights()
//{
//	PerFrameConstantBuffer perFrameBuffer;
//	ZeroMemory(&perFrameBuffer, sizeof PerFrameConstantBuffer);
//
//	// Load the direction lights to the constant buffer
//	int lightIndex = 0;
//	for (auto light : m_LoadedDirectionalLightVector)
//		perFrameBuffer.directionalLight[lightIndex++] = light;
//
//	perFrameBuffer.numDirectionalLights = m_LoadedDirectionalLightVector.size();
//
//	// Load the point lights to the constant buffer
//	lightIndex = 0;
//	for (auto light : m_LoadedPointLightVector)
//		perFrameBuffer.pointLight[lightIndex++] = light;
//
//	perFrameBuffer.numPointLights = m_LoadedPointLightVector.size();
//
//	// Load the spotlights to the constant buffer
//	lightIndex = 0;
//	for (auto light : m_LoadedSpotLightVector)
//		perFrameBuffer.spotLight[lightIndex++] = light;
//
//	perFrameBuffer.numSpotLights = m_LoadedSpotLightVector.size();
//
//	Vector3D position;
//	m_Camera.GetPosition(position);
//	perFrameBuffer.EyePosition.x = position.xf;
//	perFrameBuffer.EyePosition.y = position.yf;
//	perFrameBuffer.EyePosition.z = position.zf;
//
//	D3D11_MAPPED_SUBRESOURCE constantBufferResource2;
//	m_pDeviceContext->Map(m_pPerFrameConstantBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &constantBufferResource2);
//	memcpy(constantBufferResource2.pData, &perFrameBuffer, sizeof(PerFrameConstantBuffer));
//	m_pDeviceContext->Unmap(m_pPerFrameConstantBuffer, 0);
//
//	return TRUE;
//}
//
//std::string DirectX11Engine::GetVideoCardDesc() const
//{
//	return m_VideoCardDesc;
//}
//
//uint32 DirectX11Engine::GetVideoMemory() const
//{
//	return m_nVideoMemory;
//}
//
//float DirectX11Engine::FPS() const
//{
//	static float lastFPS = 0.0f;
//	static int frameCount = 0;
//	static float timeElapsed = 0.0f;
//	static float sleepTimeToTarget = 0.0f;
//	static float lastTime = 0.0f;
//
//	float testFPS = (m_EngineTimer.TotalTime() - lastTime) * 1000;
//	frameCount++;
//
//	lastTime = m_EngineTimer.TotalTime();
//	if ((m_EngineTimer.TotalTime() - timeElapsed) >= 1.0f)
//	{
//		float fps = (float)frameCount;
//		float mspf = 1000.0f / fps;
//
//		frameCount = 0;
//		timeElapsed += 1.0f;
//		lastFPS = fps;
//		sleepTimeToTarget = 0.0f;
//		if (mspf < 1.0f)
//			sleepTimeToTarget = (mspf * 1000) / 60;
//	}
//	//Sleep(sleepTimeToTarget);
//	return lastFPS;
//}
//
//BOOL DirectX11Engine::RegisterModel(const char* pszModelResourcePath, uint32& resourceID)
//{
//	// Get the next id, and assign the pending mesh map with the id to be loaded.
//	resourceID = m_pMeshIdGenerator->GetNextId();
//
//	// Setup an async task to load the mesh
//	m_LoadingModelPath[resourceID] = std::string(pszModelResourcePath);
//	std::future<IMesh*> loadingMeshFuture = std::async(&DirectX11Engine::LoadModelAsync, this, m_LoadingModelPath[resourceID].c_str());
//
//	m_LoadingMeshMap[resourceID] = std::move(loadingMeshFuture);
//	m_RegisteredIds.insert(resourceID);
//	return TRUE;
//}
//
//IMesh* DirectX11Engine::LoadModelAsync(const char* pszModelResourcePath)
//{
//	IMesh* pMesh = nullptr;
//	if (std::string(pszModelResourcePath).find(".gmodel") != std::string::npos)
//	{
//		if (!GUtils::Graphics::Model::GModelLoader::LoadFile(pszModelResourcePath, &pMesh))
//		{
//			char szFileName[MAX_PATH]; ZeroMemory(szFileName, MAX_PATH);
//			_splitpath(pszModelResourcePath, nullptr, nullptr, szFileName, nullptr);
//			std::string failureMessage = "CRITICAL FAILURE - DirectX11Engine::LoadModelAsync() - Failure to load model " + std::string(szFileName);
//			DEBUG_LOG(LOG, failureMessage);
//			assert(false);
//		}
//	}
//	else
//	{
//		if (!GUtils::Graphics::Model::ObjFormatLoader::LoadFile(pszModelResourcePath, &pMesh))
//		{
//			char szFileName[MAX_PATH]; ZeroMemory(szFileName, MAX_PATH);
//			_splitpath(pszModelResourcePath, nullptr, nullptr, szFileName, nullptr);
//			std::string failureMessage = "CRITICAL FAILURE - DirectX11Engine::LoadModelAsync() - Failure to load model " + std::string(szFileName);
//			DEBUG_LOG(LOG, failureMessage);
//			assert(false);
//		}
//	}
//
//	if (!pMesh->BuildBuffers(m_pDevice))
//	{
//		std::string failureMessage = "CRITICAL FAILURE - DirectX11Engine::LoadModelAsync() - Failure to build buffers ";
//		DEBUG_LOG(LOG, failureMessage);
//		assert(false);
//	}
//
//	return pMesh;
//}
//
//void DirectX11Engine::DeregisterModel(const uint32 resourceID)
//{
//	// Remove from known IDs in use.
//	m_MeshIds.erase(resourceID);
//
//	// Remove any shaders associated with the resource
//	m_RegisteredModelToVSVector[resourceID].first = nullptr;
//	m_RegisteredModelToVSVector[resourceID].second = nullptr;
//	m_RegisteredModelToPSVector[resourceID] = nullptr;
//
//	m_pMeshIdGenerator->Release(resourceID);
//	m_RegisteredIds.erase(resourceID);
//
//	// Delete it from the pending loading meshes in case it hasn't been loaded yet
//	auto loadingIter = m_LoadingMeshMap.find(resourceID);
//	if (loadingIter != m_LoadingMeshMap.end())
//	{
//		// Remove the loading path from it's map
//		auto loadingPathIter = m_LoadingModelPath.find(resourceID);
//		if (loadingPathIter != m_LoadingModelPath.end())
//			m_LoadingModelPath.erase(loadingPathIter);
//
//		if (loadingIter->second.valid())
//		{
//			IMesh* pMesh = m_LoadingMeshMap[resourceID].get();
//			m_LoadingMeshMap.erase(loadingIter);
//			SAFE_DELETE(pMesh);
//			return;
//		}
//	}
//
//	// Remove from the loaded meshes
//	auto loadedIter = m_LoadedMeshMap.find(resourceID);
//	if (loadedIter != m_LoadedMeshMap.end())
//	{
//		if (loadedIter->second)
//		{
//			IMesh* pMesh = loadedIter->second;
//			m_LoadedMeshMap.erase(loadedIter);
//			SAFE_DELETE(pMesh);
//			return;
//		}
//	}
//}
//
//BOOL DirectX11Engine::RegisterDirectionalLight(const DirectionalLight& directionalLight, uint32& resourceID)
//{
//	if (m_LoadedDirectionalLightVector.size() >= 3)
//	{
//		resourceID = -1;
//		DEBUG_LOG(LOG, "FAILURE - DirectX11Engine::RegisterDirectionalLight() already has 3 directional lights registered. Registration rejected.");
//		return FALSE;
//	}
//
//	resourceID = m_pDirectionalLightIdGenerator->GetNextId();
//	m_LoadedDirectionalLightVector.push_back(directionalLight);
//
//	return TRUE;
//}
//
//BOOL DirectX11Engine::DergisterDirectionalLight(uint32 resourceID)
//{
//	m_pDirectionalLightIdGenerator->Release(resourceID);
//	m_LoadedDirectionalLightVector.erase(m_LoadedDirectionalLightVector.begin() + resourceID);
//
//	return TRUE;
//}
//
//BOOL DirectX11Engine::ApplyDirectionalLight(const DirectionalLight& directionalLight, const uint32 resourceID)
//{
//	if (resourceID > m_LoadedDirectionalLightVector.size())
//	{
//		DEBUG_LOG(LOG, std::string("FAILURE - DirectX11Engine::ApplyDirectionalLight() doesn't have a directional light resource ID: ").append(std::to_string(resourceID)));
//		return FALSE;
//	}
//
//	m_LoadedDirectionalLightVector[resourceID - 1] = directionalLight;
//
//	return TRUE;
//}
//
//BOOL DirectX11Engine::RegisterPointLight(const PointLight& pointLight, uint32& resourceID)
//{
//	if (m_LoadedPointLightVector.size() >= 8)
//	{
//		resourceID = -1;
//		DEBUG_LOG(LOG, "FAILURE - DirectX11Engine::RegisterPointLight() already has 8 point lights registered. Registration rejected.");
//		return FALSE;
//	}
//
//	resourceID = m_pPointLightIdGenerator->GetNextId();
//	m_LoadedPointLightVector.push_back(pointLight);
//
//	return TRUE;
//}
//
//BOOL DirectX11Engine::DergisterPointLight(uint32 resourceID)
//{
//	m_pPointLightIdGenerator->Release(resourceID);
//	m_LoadedPointLightVector.erase(m_LoadedPointLightVector.begin() + resourceID);
//
//	return TRUE;
//}
//
//BOOL DirectX11Engine::ApplyPointLight(const PointLight& pointLight, const uint32 resourceID)
//{
//	if (resourceID > m_LoadedPointLightVector.size())
//	{
//		DEBUG_LOG(LOG, std::string("FAILURE - DirectX11Engine::ApplyPointLight() doesn't have a point light resource ID: ").append(std::to_string(resourceID)));
//		return FALSE;
//	}
//
//	m_LoadedPointLightVector[resourceID - 1] = pointLight;
//
//	return TRUE;
//}
//
//BOOL DirectX11Engine::RegisterSpotLight(const SpotLight& spotLight, uint32& resourceID)
//{
//	if (m_LoadedSpotLightVector.size() >= 8)
//	{
//		resourceID = -1;
//		DEBUG_LOG(LOG, "FAILURE - DirectX11Engine::RegisterSpotLight() already has 8 spot lights registered. Registration rejected.");
//		return FALSE;
//	}
//
//	resourceID = m_pSpotLightIdGenerator->GetNextId();
//	m_LoadedSpotLightVector.push_back(spotLight);
//
//	return TRUE;
//}
//
//BOOL DirectX11Engine::DergisterSpotLight(const uint32 resourceID)
//{
//	m_pSpotLightIdGenerator->Release(resourceID);
//	m_LoadedSpotLightVector.erase(m_LoadedSpotLightVector.begin() + resourceID);
//
//	return TRUE;
//}
//
//BOOL DirectX11Engine::ApplySpotLight(const SpotLight& spotLight, const uint32 resourceID)
//{
//	if (resourceID > m_LoadedSpotLightVector.size())
//	{
//		DEBUG_LOG(LOG, std::string("FAILURE - DirectX11Engine::ApplySpotLight() doesn't have a spot light resource ID: ").append(std::to_string(resourceID)));
//		return FALSE;
//	}
//
//	m_LoadedSpotLightVector[resourceID - 1] = spotLight;
//
//	return TRUE;
//}
//
//void DirectX11Engine::TransformModel(const int resourceID, const ConstantBuffer& constantBuffer)
//{
//	TransformOnModelKey_t key;
//	key.first = resourceID;
//	key.second = constantBuffer;
//	m_ModelTransformations.push_back(key);
//	//m_ModelTransformationMap[resourceID] = constantBuffer;
//}
//
//BOOL DirectX11Engine::AssignVertexShaderToModel(const int resourceID, std::string shaderID)
//{
//	if (m_LoadingMeshMap.find(resourceID) == m_LoadingMeshMap.end())
//	{
//		if (m_LoadedMeshMap.find(resourceID) == m_LoadedMeshMap.end())
//		{
//			std::string errorMessage = "CRITICAL FAILURE - DirectX Engine failed bind resourceID ID: " + std::to_string(resourceID) + " to a shader because it can't be found.";
//			DEBUG_LOG(LOG, errorMessage);
//			return FALSE;
//		}
//	}
//
//	if (m_VertexShaderMap.find(shaderID) == m_VertexShaderMap.end())
//	{
//		std::string errorMessage = "CRITICAL FAILURE - DirectX Engine failed bind shader ID: " + shaderID + " because it has not been loaded.";
//		DEBUG_LOG(LOG, errorMessage);
//		return FALSE;
//	}
//
//	VertexShaderAndInputPair_t vertexShaderInputPair = m_VertexShaderMap[shaderID];
//	//m_RegisteredModelToVSMap[resourceID] = vertexShaderInputPair;
//	m_RegisteredModelToVSVector[resourceID] = vertexShaderInputPair;
//	return TRUE;
//}
//
//BOOL DirectX11Engine::AssignPixelShaderToModel(const int resourceID, std::string shaderID)
//{
//	if (m_LoadingMeshMap.find(resourceID) == m_LoadingMeshMap.end())
//	{
//		if (m_LoadedMeshMap.find(resourceID) == m_LoadedMeshMap.end())
//		{
//			std::string errorMessage = "CRITICAL FAILURE - DirectX Engine failed bind resourceID ID: " + std::to_string(resourceID) + " to a shader because it can't be found.";
//			DEBUG_LOG(LOG, errorMessage);
//			return FALSE;
//		}
//	}
//
//	if (m_PixelShaderMap.find(shaderID) == m_PixelShaderMap.end())
//	{
//		std::string errorMessage = "CRITICAL FAILURE - DirectX Engine failed bind shader ID: " + shaderID + " because it has not been loaded.";
//		DEBUG_LOG(LOG, errorMessage);
//		return FALSE;
//	}
//
//	m_RegisteredModelToPSVector[resourceID] = m_PixelShaderMap[shaderID];
//	//m_RegisteredModelToPSMap[resourceID] = m_PixelShaderMap[shaderID];
//
//	return TRUE;
//}
//
//BOOL DirectX11Engine::SetRegisteredResourceVertexShaderAndLayout(const int resourceID, ID3D11DeviceContext* pContext)
//{
//	if (m_RegisteredModelToVSVector[resourceID].first == nullptr/* || m_RegisteredModelToVSVector[resourceID].second == nullptr*/)
//	{
//		std::string errorMessage = "CRITICAL FAILURE - DirectX Engine Unable to find loaded vertex shader with model ID: " + resourceID;
//		DEBUG_LOG(LOG, errorMessage);
//		return FALSE;
//	}
//
//	ID3D11VertexShader* pVertexShader = m_RegisteredModelToVSVector[resourceID].first;
//	ID3D11InputLayout* pInputLayout = m_RegisteredModelToVSVector[resourceID].second;
//
//	pContext->IASetInputLayout(pInputLayout);
//	pContext->VSSetShader(pVertexShader, NULL, 0);
//
//	return TRUE;
//}
//
//BOOL DirectX11Engine::SetRegisteredResourcePixelShader(const int resourceID, ID3D11DeviceContext* pContext)
//{
//	if (m_RegisteredModelToPSVector[resourceID] == nullptr)
//	{
//		std::string errorMessage = "CRITICAL FAILURE - DirectX Engine Unable to find loaded pixel shader with model ID: " + resourceID;
//		DEBUG_LOG(LOG, errorMessage);
//		return FALSE;
//	}
//
//	ID3D11PixelShader* pPixelShader = m_RegisteredModelToPSVector[resourceID];
//	pContext->PSSetShader(pPixelShader, NULL, 0);
//
//	return TRUE;
//}
//
//bool DirectX11Engine::GetRegisteredResourceMaterial(const int resourceID, Material& material)
//{
//	auto iter = m_MaterialToRegisteredModelMap.find(resourceID);
//	if (iter == m_MaterialToRegisteredModelMap.end())
//		return false; // No material loaded for the resource.
//
//	material = iter->second;
//
//	return true;
//}
//
//bool DirectX11Engine::GetRegisteredResourceTexture(const uint32 resourceID, ID3D11DeviceContext* pContext)
//{
//	auto iter = m_TextureToRegisteredModelMap.find(resourceID);
//	if (iter == m_TextureToRegisteredModelMap.end())
//		return false; // No texture loaded for the resource.
//
//	ID3D11ShaderResourceView* pResourceView = iter->second.first;
//	pContext->PSSetShaderResources(0, 1, &pResourceView);
//	ID3D11SamplerState* pSamplerState = iter->second.second;
//	pContext->PSSetSamplers(0, 1, &pSamplerState);
//
//	return true;
//}
//
//BOOL DirectX11Engine::GetRegisteredMesh(const int resourceID, IMesh** pMesh)
//{
//	auto meshIter = m_LoadedMeshMap.find(resourceID);
//	if (meshIter == m_LoadedMeshMap.end())
//	{
//		// If it's not found in the loaded mesh map, it hasn't been moved to loaded yet.
//		if (m_LoadingMeshMap.find(resourceID) == m_LoadingMeshMap.end())
//		{
//			// If it hasn't been found in loaded or loading, there's a problem and it's not going to be rendered
//			// debug message
//			return FALSE;
//		}
//
//		// Move the loading model into loaded
//		if (!LoadPendingModel(resourceID))
//			return FALSE;
//
//		// It should now be loaded
//		meshIter = m_LoadedMeshMap.find(resourceID);
//	}
//
//	*pMesh = meshIter->second;
//	return TRUE;
//}
//
//void DirectX11Engine::ClearScreenToDefaultColor(ID3D11DeviceContext* pDeviceContext)
//{
//	// Clear the screen
//	float ClearColor[4] = { 0.0f, 0.125f, 0.3f, 1.0f }; // red,green,blue,alpha
//	pDeviceContext->ClearRenderTargetView(m_pRenderTargetView, ClearColor);
//	pDeviceContext->ClearDepthStencilView(m_pDepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);
//}
//
//void DirectX11Engine::ApplyCamera(const Camera& camera)
//{
//	memcpy(&m_Camera, &camera, sizeof Camera);
//}
//
//void DirectX11Engine::GetCamera(Camera& camera)
//{
//	memcpy(&camera, &m_Camera, sizeof Camera);
//}
//
//void DirectX11Engine::SetRasterizerState(const char* pszRasterizerStateName)
//{
//	ID3D11RasterizerState* pDefaultState = m_RasterizerStateMap[pszRasterizerStateName];
//	if (pDefaultState == nullptr)
//	{
//		DEBUG_LOG(LOG, std::string("FAILURE - Rasterizer state defined hasn't been loaded: ")
//			.append(pszRasterizerStateName)
//			.append("Rasterizer state will remain as is."));
//		return;
//	}
//
//	m_pDeviceContext->RSSetState(pDefaultState);
//}
//
//BOOL DirectX11Engine::RegisterMaterialToModel(const int resourceIDOfModel, const Material& material)
//{
//	// Make sure there's a registered resource
//	auto meshIter = m_LoadedMeshMap.find(resourceIDOfModel);
//	if (meshIter == m_LoadedMeshMap.end())
//	{
//		// If it's not found in the loaded mesh map, it hasn't been moved to loaded yet.
//		if (m_LoadingMeshMap.find(resourceIDOfModel) == m_LoadingMeshMap.end())
//		{
//			DEBUG_LOG(LOG, std::string("FAILURE - Cannot find model with resource ID: ")
//				.append(std::to_string(resourceIDOfModel)));
//			return FALSE;
//		}
//	}
//
//	m_MaterialToRegisteredModelMap[resourceIDOfModel] = material;
//
//	return TRUE;
//}
//
//BOOL DirectX11Engine::DeregisterMaterialToModel(const int resourceIDOfModel)
//{
//	auto iter = m_MaterialToRegisteredModelMap.find(resourceIDOfModel);
//
//	if (iter == m_MaterialToRegisteredModelMap.end())
//	{
//		DEBUG_LOG(LOG, std::string("FAILURE - Cannot find a loaded material assigned to resource ID: ")
//			.append(std::to_string(resourceIDOfModel)));
//		return FALSE;
//	}
//
//	m_MaterialToRegisteredModelMap.erase(iter);
//
//	return TRUE;
//}
//
//BOOL DirectX11Engine::RegisterTextureToModel(const uint32 resourceID, const char* pszTexturePath, const char* pszSamplerName)
//{
//	// Make sure there's a registered resource
//	auto meshIter = m_LoadedMeshMap.find(resourceID);
//	if (meshIter == m_LoadedMeshMap.end())
//	{
//		// If it's not found in the loaded mesh map, it hasn't been moved to loaded yet.
//		if (m_LoadingMeshMap.find(resourceID) == m_LoadingMeshMap.end())
//		{
//			DEBUG_LOG(LOG, std::string("FAILURE - Cannot find model with resource ID: ")
//				.append(std::to_string(resourceID)));
//			return FALSE;
//		}
//	}
//
//	// Convert to wide character
//	wchar_t szwPath[_MAX_PATH]; ZeroMemory(szwPath, _MAX_PATH);
//	MultiByteToWideChar(0, 0, std::string(pszTexturePath).c_str(), std::string(pszTexturePath).length(), szwPath, std::string(pszTexturePath).length());
//
//	// Load texture file
//	ID3D11Resource* pTex = nullptr; // We don't need to keep this.
//	ID3D11ShaderResourceView* pShaderResourceView = nullptr;
//	if (FAILED(CreateDDSTextureFromFile(m_pDevice, std::wstring(szwPath).c_str(), &pTex, &pShaderResourceView)))
//	{
//		DEBUG_LOG(LOG, std::string("FAILURE - Cannot load DDS texture from file path: ")
//			.append(std::string(pszTexturePath)));
//		return FALSE;
//	}
//
//	COM_RELEASE(pTex);
//	m_TextureToRegisteredModelMap[resourceID].first = pShaderResourceView;
//	m_TextureToRegisteredModelMap[resourceID].second = m_SamplerStateMap[pszSamplerName];
//
//	return TRUE;
//}
//
//BOOL DirectX11Engine::DeregisterTextureToModel(const uint32 resourceID)
//{
//	auto iter = m_TextureToRegisteredModelMap.find(resourceID);
//	if (iter == m_TextureToRegisteredModelMap.end())
//	{
//		DEBUG_LOG(LOG, std::string("FAILURE - Cannot find a loaded texture assigned to resource ID: ")
//			.append(std::to_string(resourceID)));
//		return FALSE;
//	}
//
//	ID3D11ShaderResourceView* pResourceView = iter->second.first;
//	ID3D11SamplerState* pSamplerState = iter->second.second;
//	COM_RELEASE(pResourceView);
//	//COM_RELEASE(pSamplerState); Already cleaned up from its map
//
//	m_TextureToRegisteredModelMap.erase(iter);
//
//	return TRUE;
//}