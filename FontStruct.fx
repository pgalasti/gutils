#ifndef FONT_STRUCTURES_FXH
#define FONT_STRUCTURES_FXH

struct VertexFontInput
{
	float4 position : POSITION;
	float2 tex : TEXTCOORD0;
};

struct PixelFontInput
{
	float4 position : SV_POSITION;
	float2 tex : TEXTCOORD0;
};

#endif
