#pragma once
#include "GMath.h"
#include "stdafx.h" 
#include <math.h>

// Vector2D
using GUtils::GMath::Vector2D;


Vector2D Vector2D::operator+ (const Vector2D& otherVector)  const
{
	return Vector2D (this->xf + otherVector.xf, this->yf + otherVector.yf);
}

Vector2D Vector2D::operator- (const Vector2D& otherVector) const
{
	return Vector2D (this->xf - otherVector.xf, this->yf - otherVector.yf);
}

Vector2D Vector2D::operator/ (const float scalar) const
{
	return Vector2D(this->xf/scalar, this->yf/scalar);
}

Vector2D Vector2D::operator* (const float scalar) const
{
	return Vector2D(this->xf*scalar, this->yf*scalar);
}

const bool Vector2D::operator== (const Vector2D& otherVector) const
{
	return (this->xf == otherVector.xf && this->yf == otherVector.yf);
}

const float Vector2D::Magnitude() const
{
	return sqrt((xf*xf)+(yf*yf));
}

Vector2D Vector2D::Normalize() const
{
	return Vector2D(xf/Magnitude(), yf/Magnitude());
}

void Vector2D::NormalizeThis()
{
	const float magnitude = Magnitude();
	xf /= magnitude;
	yf /= magnitude;
}

const float Vector2D::DotProduct(const Vector2D& otherVector) const
{
	return (this->xf * otherVector.xf) + (this->yf * otherVector.yf);
}

const float Vector2D::AngleRad(const Vector2D& otherVector) const
{
	return acos(DotProduct(otherVector)/ (this->Magnitude() * otherVector.Magnitude()));
}

const float Vector2D::AngleDeg(const Vector2D& otherVector) const
{
	return acos(DotProduct(otherVector)/ (this->Magnitude() * otherVector.Magnitude()))
		* RAD_F;
}

const bool Vector2D::Orthogonal(const Vector2D& otherVector) const
{
	return DotProduct(otherVector) == 0.0;
}

// Vector3D
using GUtils::GMath::Vector3D;


Vector3D Vector3D::operator+ (const Vector3D& otherVector) const
{
	return Vector3D (this->xf + otherVector.xf, this->yf + otherVector.yf, this->zf + otherVector.zf);
}

Vector3D Vector3D::operator- (const Vector3D& otherVector) const
{
	return Vector3D (this->xf - otherVector.xf, this->yf - otherVector.yf, this->zf - otherVector.zf);
}

Vector3D Vector3D::operator/ (const float scalar) const
{
	return Vector3D(this->xf/scalar, this->yf/scalar, this->zf/scalar);
}

Vector3D Vector3D::operator* (const float scalar) const
{
	return Vector3D(this->xf*scalar, this->yf*scalar, this->zf*scalar);
}

const bool Vector3D::operator== (const Vector3D& otherVector) const
{
	return (this->xf == otherVector.xf && this->yf == otherVector.yf && this->zf == otherVector.zf);
}

inline const float Vector3D::Magnitude() const
{
	return sqrt((xf*xf)+(yf*yf)+(zf*zf));
}

Vector3D Vector3D::Normalize() const
{
	const float magnitude = Magnitude();
	if (magnitude == 0.0f)
		return Vector3D(0, 0, 0);

	return Vector3D(xf/magnitude, yf/magnitude, zf/magnitude);
}

void Vector3D::NormalizeThis()
{
	const float magnitude = Magnitude();
	if (magnitude == 0.0f)
	{
		xf = 0.0f;
		yf = 0.0f;
		zf = 0.0f;
		return;
	}

	xf /= magnitude;
	yf /= magnitude;
	zf /= magnitude;
}

const float Vector3D::DotProduct(const Vector3D& otherVector) const
{
	return (this->xf * otherVector.xf) + (this->yf * otherVector.yf) + (this->zf * otherVector.zf);
}

const float Vector3D::AngleRad(const Vector3D& otherVector) const
{
	return acos( DotProduct(otherVector)/(this->Magnitude()*otherVector.Magnitude()) );
}

const float Vector3D::AngleDeg(const Vector3D& otherVector) const
{
	return acos(DotProduct(otherVector)/(this->Magnitude()*otherVector.Magnitude()))
		*RAD_F;
}

const bool Vector3D::Orthogonal(const Vector3D& otherVector) const
{
	return DotProduct(otherVector) == 0.0;
}

const Vector3D Vector3D::Cross(const Vector3D& otherVector) const
{
	return Vector3D(
		((this->yf*otherVector.zf) - (this->zf*otherVector.yf)),
		((this->zf*otherVector.xf) - (this->xf*otherVector.zf)),
		((this->xf*otherVector.yf) - (this->yf*otherVector.xf))
		);
}

namespace GUtils
{
	namespace GMath
	{
		const float AngleFromXY(float x, float y)
		{
			float theta = 0.0f;

			// Quadrant I or IV
			if(x >= 0.0f) 
			{
				// If x = 0, then atanf(y/x) = +pi/2 if y > 0
				//                atanf(y/x) = -pi/2 if y < 0
				theta = atanf(y / x); // in [-pi/2, +pi/2]

				if(theta < 0.0f)
					theta += 2.0f*PI_F; // in [0, 2*pi).
			}

			// Quadrant II or III
			else      
				theta = atanf(y/x) + PI_F; // in [0, 2*pi).

			return theta;
		}

		const float RandF()
		{
			return (float)(rand()) / (float)RAND_MAX;
		}

		// Returns random float in [a, b).
		const float RandF(float a, float b)
		{
			return a + RandF()*(b-a);
		}
	}
}