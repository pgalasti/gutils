#pragma once
#include "stdafx.h"

#include "FontCoordinateLoader.h"

#include <list>
#include "FileLoader.h"
#include "UtilFunctions.h"
#include "Parser.h"
#include "Font.h"

using namespace GraphicsEngine::Font::DirectX;
using namespace GraphicsEngine::Font;
using namespace GUtils::IO;
using namespace GUtils::Utils::Functions;


FontCoordinateLoader::FontCoordinateLoader(LoadType loadType /*= Normal*/)
{
	m_LoadType = loadType;
}

FontCoordinateLoader::~FontCoordinateLoader()
{

}

BOOL FontCoordinateLoader::LoadCharactersFromData(const ubyte8* pData, CoordinateMap& map)
{

	return TRUE;
}

BOOL FontCoordinateLoader::LoadCharactersFromPath(const char* pszFilePath, CoordinateMap& map)
{
	FileLoader fileLoader(pszFilePath);
	if (!fileLoader.Open())
		return FALSE;

	std::list<std::string> fileLines;
	if (!fileLoader.GetFileLines(fileLines))
		return FALSE;

	fileLoader.Close();

	for (std::string line : fileLines)
	{
		CleanString(line, "\r\t");
		RemoveTrailingAndLeadingSpaces(line);
		if (line[0] == '#')
			continue;

		AsciiValue asciiValue = 0;
		FontCoordinateStructure character;
		ZeroMemory(&character, sizeof FontCoordinateStructure);

		// ASCII Value
		std::string asciiValueStr;
		asciiValueStr = line[0];
		asciiValueStr += line[1];
		asciiValueStr += line[2];
		asciiValue = atoi(asciiValueStr.c_str());

		ubyte8 additionalSpace = 0;
		if (line[2] != ' ')
			++additionalSpace;

		// The character
		character.Character = line[3 + additionalSpace];

		// Left U Coordinate
		std::string leftUCoordinateStr = line.substr(5 + additionalSpace, 10);
		character.LeftU = atof(leftUCoordinateStr.c_str());

		// Right U Coordinate
		std::string rightUCoordinateStr = line.substr(16 + additionalSpace, 10);
		character.RightU = atof(rightUCoordinateStr.c_str());
		
		// Width of character
		std::string widthStr = line.substr(28, line.length() - 28 + additionalSpace);
		character.Width = atoi(widthStr.c_str());
		
		map[asciiValue] = character;
	}


	return TRUE;
}