#pragma once
#include "stdafx.h"
#include "PollInput.h"

using namespace GUtils::System;

void PollInput::SetCharacter(const char character)
{
	// Return if it's not an alpha character
	if (!isalpha(character))
		return;

	char upperChar = toupper(character);
	unsigned shiftValue = upperChar - 65;

	m_InputState.lCharacterKeys |= (1 << shiftValue);
}

void PollInput::UnsetCharacter(const char character)
{
	// Return if it's not an alpha character
	if (!isalpha(character))
		return;

	if (character == ' ')
	{
		m_InputState.lCharacterKeys |= InputState::KEY_DOWN_SPACE;
		return;
	}

	char upperChar = toupper(character);
	unsigned shiftValue = upperChar - 65;

	m_InputState.lCharacterKeys &= ~(1 << shiftValue);
}

void PollInput::SetFunctionKey(FunctionKey key)
{
	switch (key)
	{
	case FunctionKey::F1:
		m_InputState.lFunctionKeys |= InputState::FUNCTION_KEY_DOWN_1;
		break;
	case FunctionKey::F2:
		m_InputState.lFunctionKeys |= InputState::FUNCTION_KEY_DOWN_2;
		break;
	case FunctionKey::F3:
		m_InputState.lFunctionKeys |= InputState::FUNCTION_KEY_DOWN_3;
		break;
	case FunctionKey::F4:
		m_InputState.lFunctionKeys |= InputState::FUNCTION_KEY_DOWN_4;
		break;
	case FunctionKey::F5:
		m_InputState.lFunctionKeys |= InputState::FUNCTION_KEY_DOWN_5;
		break;
	case FunctionKey::F6:
		m_InputState.lFunctionKeys |= InputState::FUNCTION_KEY_DOWN_6;
		break;
	case FunctionKey::F7:
		m_InputState.lFunctionKeys |= InputState::FUNCTION_KEY_DOWN_7;
		break;
	case FunctionKey::F8:
		m_InputState.lFunctionKeys |= InputState::FUNCTION_KEY_DOWN_8;
		break;
	case FunctionKey::F9:
		m_InputState.lFunctionKeys |= InputState::FUNCTION_KEY_DOWN_9;
		break;
	case FunctionKey::F10:
		m_InputState.lFunctionKeys |= InputState::FUNCTION_KEY_DOWN_10;
		break;
	case FunctionKey::F11:
		m_InputState.lFunctionKeys |= InputState::FUNCTION_KEY_DOWN_11;
		break;
	case FunctionKey::F12:
		m_InputState.lFunctionKeys |= InputState::FUNCTION_KEY_DOWN_12;
		break;
	}
}

void PollInput::UnsetFunctionKey(FunctionKey key)
{
	switch (key)
	{
	case FunctionKey::F1:
		m_InputState.lFunctionKeys &= ~InputState::FUNCTION_KEY_DOWN_1;
		break;
	case FunctionKey::F2:
		m_InputState.lFunctionKeys &= ~InputState::FUNCTION_KEY_DOWN_2;
		break;
	case FunctionKey::F3:
		m_InputState.lFunctionKeys &= ~InputState::FUNCTION_KEY_DOWN_3;
		break;
	case FunctionKey::F4:
		m_InputState.lFunctionKeys &= ~InputState::FUNCTION_KEY_DOWN_4;
		break;
	case FunctionKey::F5:
		m_InputState.lFunctionKeys &= ~InputState::FUNCTION_KEY_DOWN_5;
		break;
	case FunctionKey::F6:
		m_InputState.lFunctionKeys &= ~InputState::FUNCTION_KEY_DOWN_6;
		break;
	case FunctionKey::F7:
		m_InputState.lFunctionKeys &= ~InputState::FUNCTION_KEY_DOWN_7;
		break;
	case FunctionKey::F8:
		m_InputState.lFunctionKeys &= ~InputState::FUNCTION_KEY_DOWN_8;
		break;
	case FunctionKey::F9:
		m_InputState.lFunctionKeys &= ~InputState::FUNCTION_KEY_DOWN_9;
		break;
	case FunctionKey::F10:
		m_InputState.lFunctionKeys &= ~InputState::FUNCTION_KEY_DOWN_10;
		break;
	case FunctionKey::F11:
		m_InputState.lFunctionKeys &= ~InputState::FUNCTION_KEY_DOWN_11;
		break;
	case FunctionKey::F12:
		m_InputState.lFunctionKeys &= ~InputState::FUNCTION_KEY_DOWN_12;
		break;
	}
}

void PollInput::SetNumberCharacter(ubyte8 number, NumberOrigin origin)
{
	if (number < 0 || number > 9)
		return;

	if (origin == NumberOrigin::Row)
	{
		m_InputState.lNumberKeys |= (1 << number);
	}
	else if (origin == NumberOrigin::KeyPad)
	{
		unsigned long bitStart = InputState::NUMBERPAD_KEY_DOWN_0;
		m_InputState.lNumberKeys |= (bitStart << number);
	}
}

void PollInput::UnsetNumberCharacter(ubyte8 number, NumberOrigin origin)
{
	if (number < 0 || number > 9)
		return;

	if (origin == NumberOrigin::Row)
	{
		m_InputState.lNumberKeys &= ~(1 << number);
	}
	else if (origin == NumberOrigin::KeyPad)
	{
		unsigned long bitStart = InputState::NUMBERPAD_KEY_DOWN_0;
		m_InputState.lNumberKeys &= ~(bitStart << number);
	}
}

void PollInput::SetSpecialKey(SpecialKey key)
{
	switch (key)
	{
	case SpecialKey::Alt:
		m_InputState.lSpecialKeys |= InputState::SPECIAL_KEY_DOWN_ALT;
		break;
	case SpecialKey::Backspace:
		m_InputState.lSpecialKeys |= InputState::SPECIAL_KEY_DOWN_BACKSPACE;
		break;
	case SpecialKey::CapsLock:
		m_InputState.lSpecialKeys |= InputState::SPECIAL_KEY_DOWN_CAPS_LOCK;
		break;
	case SpecialKey::Control:
		m_InputState.lSpecialKeys |= InputState::SPECIAL_KEY_DOWN_CTRL;
		break;
	case SpecialKey::Delete:
		m_InputState.lSpecialKeys |= InputState::SPECIAL_KEY_DOWN_DELETE;
		break;
	case SpecialKey::DownArrow:
		m_InputState.lSpecialKeys |= InputState::SPECIAL_KEY_DOWN_DOWN_ARROW;
		break;
	case SpecialKey::End:
		m_InputState.lSpecialKeys |= InputState::SPECIAL_KEY_DOWN_END;
		break;
	case SpecialKey::Enter:
		m_InputState.lSpecialKeys |= InputState::SPECIAL_KEY_DOWN_ENTER;
		break;
	case SpecialKey::Escape:
		m_InputState.lSpecialKeys |= InputState::SPECIAL_KEY_DOWN_ESCAPE;
		break;
	case SpecialKey::Home:
		m_InputState.lSpecialKeys |= InputState::SPECIAL_KEY_DOWN_HOME;
		break;
	case SpecialKey::Insert:
		m_InputState.lSpecialKeys |= InputState::SPECIAL_KEY_DOWN_INSERT;
		break;
	case SpecialKey::LeftArrow:
		m_InputState.lSpecialKeys |= InputState::SPECIAL_KEY_DOWN_LEFT_ARROW;
		break;
	case SpecialKey::NumberLock:
		m_InputState.lSpecialKeys |= InputState::SPECIAL_KEY_DOWN_NUMBER_LOCK;
		break;
	case SpecialKey::PageDown:
		m_InputState.lSpecialKeys |= InputState::SPECIAL_KEY_DOWN_PAGE_DOWN;
		break;
	case SpecialKey::PageUp:
		m_InputState.lSpecialKeys |= InputState::SPECIAL_KEY_DOWN_PAGE_UP;
		break;
	case SpecialKey::PauseBreak:
		m_InputState.lSpecialKeys |= InputState::SPECIAL_KEY_DOWN_PAUSE_BREAK;
		break;
	case SpecialKey::RightArrow:
		m_InputState.lSpecialKeys |= InputState::SPECIAL_KEY_DOWN_RIGHT_ARROW;
		break;
	case SpecialKey::ScreenLock:
		m_InputState.lSpecialKeys |= InputState::SPECIAL_KEY_DOWN_SCREEN_LOCK;
		break;
	case SpecialKey::Shift:
		m_InputState.lSpecialKeys |= InputState::SPECIAL_KEY_DOWN_SHIFT;
		break;
	case SpecialKey::Tab:
		m_InputState.lSpecialKeys |= InputState::SPECIAL_KEY_DOWN_TAB;
		break;
	case SpecialKey::UpArrow:
		m_InputState.lSpecialKeys |= InputState::SPECIAL_KEY_DOWN_UP_ARROW;
		break;
	case SpecialKey::Windows:
		m_InputState.lSpecialKeys |= InputState::SPECIAL_KEY_DOWN_WINDOWS;
		break;
	}
}

void PollInput::UnsetSpecialKey(SpecialKey key)
{
	switch (key)
	{
	case SpecialKey::Alt:
		m_InputState.lSpecialKeys &= ~InputState::SPECIAL_KEY_DOWN_ALT;
		break;
	case SpecialKey::Backspace:
		m_InputState.lSpecialKeys &= ~InputState::SPECIAL_KEY_DOWN_BACKSPACE;
		break;
	case SpecialKey::CapsLock:
		m_InputState.lSpecialKeys &= ~InputState::SPECIAL_KEY_DOWN_CAPS_LOCK;
		break;
	case SpecialKey::Control:
		m_InputState.lSpecialKeys &= ~InputState::SPECIAL_KEY_DOWN_CTRL;
		break;
	case SpecialKey::Delete:
		m_InputState.lSpecialKeys &= ~InputState::SPECIAL_KEY_DOWN_DELETE;
		break;
	case SpecialKey::DownArrow:
		m_InputState.lSpecialKeys &= ~InputState::SPECIAL_KEY_DOWN_DOWN_ARROW;
		break;
	case SpecialKey::End:
		m_InputState.lSpecialKeys &= ~InputState::SPECIAL_KEY_DOWN_END;
		break;
	case SpecialKey::Enter:
		m_InputState.lSpecialKeys &= ~InputState::SPECIAL_KEY_DOWN_ENTER;
		break;
	case SpecialKey::Escape:
		m_InputState.lSpecialKeys &= ~InputState::SPECIAL_KEY_DOWN_ESCAPE;
		break;
	case SpecialKey::Home:
		m_InputState.lSpecialKeys &= ~InputState::SPECIAL_KEY_DOWN_HOME;
		break;
	case SpecialKey::Insert:
		m_InputState.lSpecialKeys &= ~InputState::SPECIAL_KEY_DOWN_INSERT;
		break;
	case SpecialKey::LeftArrow:
		m_InputState.lSpecialKeys &= ~InputState::SPECIAL_KEY_DOWN_LEFT_ARROW;
		break;
	case SpecialKey::NumberLock:
		m_InputState.lSpecialKeys &= ~InputState::SPECIAL_KEY_DOWN_NUMBER_LOCK;
		break;
	case SpecialKey::PageDown:
		m_InputState.lSpecialKeys &= ~InputState::SPECIAL_KEY_DOWN_PAGE_DOWN;
		break;
	case SpecialKey::PageUp:
		m_InputState.lSpecialKeys &= ~InputState::SPECIAL_KEY_DOWN_PAGE_UP;
		break;
	case SpecialKey::PauseBreak:
		m_InputState.lSpecialKeys &= ~InputState::SPECIAL_KEY_DOWN_PAUSE_BREAK;
		break;
	case SpecialKey::RightArrow:
		m_InputState.lSpecialKeys &= ~InputState::SPECIAL_KEY_DOWN_RIGHT_ARROW;
		break;
	case SpecialKey::ScreenLock:
		m_InputState.lSpecialKeys &= ~InputState::SPECIAL_KEY_DOWN_SCREEN_LOCK;
		break;
	case SpecialKey::Shift:
		m_InputState.lSpecialKeys &= ~InputState::SPECIAL_KEY_DOWN_SHIFT;
		break;
	case SpecialKey::Tab:
		m_InputState.lSpecialKeys &= ~InputState::SPECIAL_KEY_DOWN_TAB;
		break;
	case SpecialKey::UpArrow:
		m_InputState.lSpecialKeys &= ~InputState::SPECIAL_KEY_DOWN_UP_ARROW;
		break;
	case SpecialKey::Windows:
		m_InputState.lSpecialKeys &= ~InputState::SPECIAL_KEY_DOWN_WINDOWS;
		break;
	}
}

void PollInput::SetMouseButtonDown(MouseButton button)
{
	switch (button)
	{
	case MouseButton::Left:
		m_InputState.lMouseButtons |= InputState::MOUSE_BUTTON_LEFT_DOWN;
		break;
	case MouseButton::Middle:
		m_InputState.lMouseButtons |= InputState::MOUSE_BUTTON_MIDDLE_DOWN;
		break;
	case MouseButton::Right:
		m_InputState.lMouseButtons |= InputState::MOUSE_BUTTON_RIGHT_DOWN;
		break;
	}
}

void PollInput::UnsetMouseButtonDown(MouseButton button)
{
	switch (button)
	{
	case MouseButton::Left:
		m_InputState.lMouseButtons &= ~InputState::MOUSE_BUTTON_LEFT_DOWN;
		break;
	case MouseButton::Middle:
		m_InputState.lMouseButtons &= ~InputState::MOUSE_BUTTON_MIDDLE_DOWN;
		break;
	case MouseButton::Right:
		m_InputState.lMouseButtons &= ~InputState::MOUSE_BUTTON_RIGHT_DOWN;
		break;
	}
}
