#include "StructMatLight.fx"
#include "LightStruct.fx"

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
PSInput VS(VSInput input)
{
	PSInput output = (PSInput)0;
	output.PosW = mul(float4(input.PosL, 1.0), World).xyz;
	output.NormalW = mul(input.Normal, (float3x3)WorldInvTranspose);
	output.PosH = mul(float4(input.PosL, 1.0f), WorldViewProjection);

	return output;
}
