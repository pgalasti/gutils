#pragma once
#include "stdafx.h"

#include "Actor.h"

using namespace GameEngine::Logic::Actors;

Actor::Actor(const ActorId id)
{
	m_Id = id;
	m_Type = "NOT_SET";
}

Actor::~Actor()
{
	
}

BOOL Actor::Init(const ElementPtr pElement)
{
	if (pElement == nullptr || pElement->m_Name.empty())
		return FALSE;

	m_Type = pElement->m_Name;

	return TRUE;
}

void Actor::PostInit()
{
	for (auto compIter = m_ComponentMaps.begin(); compIter != m_ComponentMaps.end(); compIter++)
		compIter->second->PostInit();
}

void Actor::Destroy()
{
	m_ComponentMaps.clear();
}

void Actor::Update(const uint32 deltaTime)
{
	for (auto compIter = m_ComponentMaps.begin(); compIter != m_ComponentMaps.end(); compIter++)
		compIter->second->Update(deltaTime);
}

void Actor::AddComponent(StrongActorComponentPtr& pComponent)
{
	m_ComponentMaps[pComponent->GetComponentId()] = pComponent;
}