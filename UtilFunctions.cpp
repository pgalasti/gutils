#pragma once
#include "stdafx.h"

#include "UtilFunctions.h"

#include <algorithm>
#include <string>

namespace GUtils {
	namespace Utils {
		namespace Functions {

			DllExport void TrimTrailingSpaces(char * pszString)
			{
				if (!pszString)
					return;

				size_t length = strlen(pszString);
				char *pChar = &pszString[length - 1];
				while (*pChar == ' ' && pChar >= pszString)
					*pChar-- = '\0';
			}

			DllExport void RemoveLeadingSpaces(char* pszString)
			{
				char	ZB[1024];
				memset(ZB, 0, 1024);
				strncpy(ZB, pszString, 1023);
				size_t len = strlen(pszString);
				memset(pszString, 0, len);

				char	*pSource = ZB;
				while (*pSource == ' ')
					pSource++;
				strcpy(pszString, pSource);
			}

			DllExport void RemoveTrailingAndLeadingSpaces(char * pszString)
			{
				TrimTrailingSpaces(pszString);
				RemoveLeadingSpaces(pszString);
			}

			DllExport void TrimTrailingSpaces(std::string& string)
			{
				if (string.empty())
					return;

				size_t lastNonWhitespace = string.find_last_not_of(' ');
				string = string.substr(0, lastNonWhitespace + 1);
			}

			DllExport void RemoveLeadingSpaces(std::string& string)
			{
				if (string.empty())
					return;

				size_t firstNonWhitespace = string.find_first_not_of(' ');
				string = string.substr(firstNonWhitespace, string.size());
			}

			DllExport void RemoveTrailingAndLeadingSpaces(std::string& string)
			{
				TrimTrailingSpaces(string);
				RemoveLeadingSpaces(string);
			}

			DllExport void CleanString(std::string& string, const std::string& characters)
			{
				if (characters.empty())
					return;

				for (unsigned short i = 0; i < characters.size(); i++)
				{
					const char charToRemove = characters[i];
					string.erase(std::remove(string.begin(), string.end(), charToRemove), string.end());
				}
			}

			DllExport void ToUpper(std::string& string)
			{
				std::transform(string.begin(), string.end(), string.begin(), ::toupper);
			}

			DllExport void ToLower(std::string& string)
			{
				std::transform(string.begin(), string.end(), string.begin(), ::tolower);
			}

			// The following function was found on http://xoomer.virgilio.it/acantato/dev/wildcard/wildmatch.html, where it was attributed to 
			// the C/C++ Users Journal, written by Mike Cornelison. It is a little ugly, but it is FAST. 
			DllExport BOOL WildcardMatch(const char* pattern, const char* string)
			{
				int i, star;

			new_segment:

				star = 0;
				if (*pattern == '*') {
					star = 1;
					do { pattern++; } while (*pattern == '*'); /* enddo */
				} /* endif */

			test_match:

				for (i = 0; pattern[i] && (pattern[i] != '*'); i++) {
					//if (mapCaseTable[str[i]] != mapCaseTable[pat[i]]) {
					if (string[i] != pattern[i]) {
						if (!string[i]) return 0;
						if ((pattern[i] == '?') && (string[i] != '.')) continue;
						if (!star) return 0;
						string++;
						goto test_match;
					}
				}
				if (pattern[i] == '*') {
					string += i;
					pattern += i;
					goto new_segment;
				}
				if (!string[i]) return 1;
				if (i && pattern[i - 1] == '*') return 1;
				if (!star) return 0;
				string++;
				goto test_match;
			}
		}
	}
}