#pragma once
#include "stdafx.h"

#include "TestMesh.h"
#include "IRenderer.h"

using namespace GraphicsEngine::Model;

namespace GraphicsEngine {
	namespace Model {

		PosNormalVertex TruncateTex(const PosNormalTexVertex& vertex)
		{
			PosNormalVertex newVertex;
			newVertex.Normal = vertex.Normal;
			newVertex.Pos = vertex.Pos;
			return newVertex;
		}

		SimpleVertex TruncateNormalTex(const PosNormalTexVertex& vertex)
		{
			SimpleVertex newVertex;
			newVertex.Pos.x = vertex.Pos.x;
			newVertex.Pos.y = vertex.Pos.y;
			newVertex.Pos.z = vertex.Pos.z;

			// Hardcode as red
			newVertex.Color.x = 1.0f;
			newVertex.Color.w = 1.0f;

			return newVertex;
		}
	}
}
// TEST MESH
// ==============================================================================================================

TestMesh& TestMesh::operator = (TestMesh&& other)
{

	return *this;
}

void TestMesh::SetVerticies(const SimpleVertex* pVerticies, UINT numVerticies, WORD* pIndicies, UINT numIndicies)
{
	m_Verticies = std::vector < SimpleVertex >() ;
	m_Indicies = std::vector<WORD>();

	m_Verticies.clear();
	m_Indicies.clear();
	m_Verticies.reserve(numVerticies);
	m_Indicies.reserve(numIndicies);

	SimpleVertex vertex;
	for (uint32 i = 0; i < numVerticies; i++)
	{
		memcpy(&vertex, pVerticies + i, sizeof SimpleVertex);
		m_Verticies.push_back(vertex);
	}
	
	for (uint32 i = 0; i < numIndicies; i++)
	{
		m_Indicies.push_back(*(pIndicies + i));
	}
}

BOOL TestMesh::Render(ID3D11DeviceContext* pDeviceContext)
{
	UINT stride = sizeof(SimpleVertex);
	UINT offset = 0;
	pDeviceContext->IASetVertexBuffers(0, 1, &m_pVertexBuffer, &stride, &offset);
	pDeviceContext->IASetIndexBuffer(m_pIndexBuffer, DXGI_FORMAT_R16_UINT, 0);
	pDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	pDeviceContext->DrawIndexed(m_Indicies.size(), 0, 0);

	return TRUE;
}

BOOL TestMesh::BuildBuffers(ID3D11Device* pDevice)
{
	COM_RELEASE(m_pVertexBuffer);
	COM_RELEASE(m_pIndexBuffer);

	D3D11_BUFFER_DESC bd;
	D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&bd, sizeof(bd));
	ZeroMemory(&InitData, sizeof D3D11_SUBRESOURCE_DATA);

	// Vertex Buffer
	bd.Usage = D3D11_USAGE_IMMUTABLE;
	bd.ByteWidth = sizeof(SimpleVertex) * m_Verticies.size();
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;
	InitData.pSysMem = &m_Verticies[0];
	if (FAILED(pDevice->CreateBuffer(&bd, &InitData, &m_pVertexBuffer)))
		return FALSE;

	// Index Buffer
	bd.Usage = D3D11_USAGE_IMMUTABLE;
	bd.ByteWidth = sizeof(WORD) * m_Indicies.size();
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;
	InitData.pSysMem = &m_Indicies[0];
	if (FAILED(pDevice->CreateBuffer(&bd, &InitData, &m_pIndexBuffer)))
		return FALSE;

	return TRUE;
}

// MATERIAL MESH
// ==============================================================================================================

void PosNormalMesh::SetVerticies(const PosNormalVertex* pVerticies, UINT numVerticies, WORD* pIndicies, UINT numIndicies)
{
	m_Verticies = std::vector < PosNormalVertex >();
	m_Indicies = std::vector<WORD>();

	m_Verticies.clear();
	m_Indicies.clear();
	m_Verticies.reserve(numVerticies);
	m_Indicies.reserve(numIndicies);

	PosNormalVertex vertex;
	for (uint32 i = 0; i < numVerticies; i++)
	{
		memcpy(&vertex, pVerticies + i, sizeof PosNormalVertex);
		m_Verticies.push_back(vertex);
	}

	for (uint32 i = 0; i < numIndicies; i++)
	{
		m_Indicies.push_back(*(pIndicies + i));
	}
}

BOOL PosNormalMesh::Render(ID3D11DeviceContext* pDeviceContext)
{
	UINT stride = sizeof(PosNormalVertex);
	UINT offset = 0;
	pDeviceContext->IASetVertexBuffers(0, 1, &m_pVertexBuffer, &stride, &offset);
	pDeviceContext->IASetIndexBuffer(m_pIndexBuffer, DXGI_FORMAT_R16_UINT, 0);
	pDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	pDeviceContext->DrawIndexed(m_Indicies.size(), 0, 0);

	return TRUE;
}

BOOL PosNormalMesh::BuildBuffers(ID3D11Device* pDevice)
{
	COM_RELEASE(m_pVertexBuffer);
	COM_RELEASE(m_pIndexBuffer);

	D3D11_BUFFER_DESC bd;
	D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&bd, sizeof(bd));
	ZeroMemory(&InitData, sizeof D3D11_SUBRESOURCE_DATA);

	// Vertex Buffer
	bd.Usage = D3D11_USAGE_IMMUTABLE;
	bd.ByteWidth = sizeof(PosNormalVertex) * m_Verticies.size();
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;
	InitData.pSysMem = &m_Verticies[0];
	if (FAILED(pDevice->CreateBuffer(&bd, &InitData, &m_pVertexBuffer)))
		return FALSE;

	// Index Buffer
	bd.Usage = D3D11_USAGE_IMMUTABLE;
	bd.ByteWidth = sizeof(WORD) * m_Indicies.size();
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;
	InitData.pSysMem = &m_Indicies[0];
	if (FAILED(pDevice->CreateBuffer(&bd, &InitData, &m_pIndexBuffer)))
		return FALSE;

	return TRUE;
}

// TEXTURE MESH
// ==============================================================================================================

void PosNormalTexMesh::SetVerticies(const PosNormalTexVertex* pVerticies, UINT numVerticies, WORD* pIndicies, UINT numIndicies)
{
	m_Verticies = std::vector < PosNormalTexVertex >();
	m_Indicies = std::vector<WORD>();

	m_Verticies.clear();
	m_Indicies.clear();
	m_Verticies.reserve(numVerticies);
	m_Indicies.reserve(numIndicies);

	PosNormalTexVertex vertex;
	for (uint32 i = 0; i < numVerticies; i++)
	{
		memcpy(&vertex, pVerticies + i, sizeof PosNormalTexVertex);
		m_Verticies.push_back(vertex);
	}

	for (uint32 i = 0; i < numIndicies; i++)
	{
		m_Indicies.push_back(*(pIndicies + i));
	}
}

BOOL PosNormalTexMesh::Render(ID3D11DeviceContext* pDeviceContext)
{
	UINT stride = sizeof(PosNormalTexVertex);
	UINT offset = 0;
	pDeviceContext->IASetVertexBuffers(0, 1, &m_pVertexBuffer, &stride, &offset);
	pDeviceContext->IASetIndexBuffer(m_pIndexBuffer, DXGI_FORMAT_R16_UINT, 0);
	pDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	pDeviceContext->DrawIndexed(m_Indicies.size(), 0, 0);

	return TRUE;
}

BOOL PosNormalTexMesh::BuildBuffers(ID3D11Device* pDevice)
{
	COM_RELEASE(m_pVertexBuffer);
	COM_RELEASE(m_pIndexBuffer);

	D3D11_BUFFER_DESC bd;
	D3D11_SUBRESOURCE_DATA InitData;
	ZeroMemory(&bd, sizeof(bd));
	ZeroMemory(&InitData, sizeof D3D11_SUBRESOURCE_DATA);

	// Vertex Buffer
	bd.Usage = D3D11_USAGE_IMMUTABLE;
	bd.ByteWidth = sizeof(PosNormalTexVertex) * m_Verticies.size();
	bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	bd.CPUAccessFlags = 0;
	InitData.pSysMem = &m_Verticies[0];
	if (FAILED(pDevice->CreateBuffer(&bd, &InitData, &m_pVertexBuffer)))
		return FALSE;

	// Index Buffer
	bd.Usage = D3D11_USAGE_IMMUTABLE;
	bd.ByteWidth = sizeof(WORD) * m_Indicies.size();
	bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	bd.CPUAccessFlags = 0;
	InitData.pSysMem = &m_Indicies[0];
	if (FAILED(pDevice->CreateBuffer(&bd, &InitData, &m_pIndexBuffer)))
		return FALSE;

	return TRUE;
}