#pragma once

#include "stdafx.h"
#include "Camera.h"

using namespace GUtils::Graphics::Camera;
using namespace DirectX;
using namespace GUtils::GMath;

Camera::Camera()
{
	Init();
}

Camera::Camera(const Vector3D& position, const Vector3D& focus, const Vector3D& up, float aspectRatio, float nearPlane, float farPlane, float fieldOfView /*= PI_F / 2*/)
{
	Init();

	// View Matrix Properties
	SetPosition(position);
	SetFocus(focus);
	SetUp(up);

	// Projection Matrix Properties
	SetAspectRatio(aspectRatio);
	SetNearPlane(nearPlane);
	SetFarPlane(farPlane);
	SetFieldOfViewRadians(fieldOfView);
}

Camera::~Camera()
{

}

void Camera::Init()
{
	SetPosition(Vector3D(0.0f, 1.0f, -5.0f));
	SetFocus(Vector3D(0.0f, 1.0f, 0.0f));
	SetUp(Vector3D(0.0f, 1.0f, 0.0f));

	m_FieldOfViewRadians = m_ScreenAspectRatio = m_NearPlane = m_FarPlane = 0.0f;
}

void Camera::SetPosition(const Vector3D& position)
{
	memcpy(&m_Postion, &position, sizeof Vector3D);
}

void Camera::SetFocus(const Vector3D& focus)
{
	memcpy(&m_Focus, &focus, sizeof Vector3D);
}

void Camera::SetUp(const Vector3D& up)
{
	memcpy(&m_Up, &up, sizeof Vector3D);
}

void Camera::GetPosition(Vector3D& position) const
{
	memcpy(&position, &m_Postion, sizeof Vector3D);
}

void Camera::GetFocus(Vector3D& focus) const
{
	memcpy(&focus, &m_Focus, sizeof Vector3D);
}

void Camera::GetUp(Vector3D& up) const
{
	memcpy(&up, &m_Up, sizeof Vector3D);
}

XMMATRIX Camera::GetViewProjectionMatrix() const
{
	return GetViewMatrix() * GetProjectionMatrix();
}

XMMATRIX Camera::GetViewMatrix() const
{
	const XMVECTOR Eye = XMVectorSet(m_Postion.xf, m_Postion.yf, m_Postion.zf, 0.0f);
	const XMVECTOR At = XMVectorSet(m_Focus.xf, m_Focus.yf, m_Focus.zf, 0.0f);
	const XMVECTOR Up = XMVectorSet(m_Up.xf, m_Up.yf, m_Up.zf, 0.0f);
	return XMMatrixLookAtLH(Eye, At, Up);
}

XMMATRIX Camera::GetProjectionMatrix() const
{
	return XMMatrixPerspectiveFovLH(m_FieldOfViewRadians, m_ScreenAspectRatio, m_NearPlane, m_FarPlane);
}