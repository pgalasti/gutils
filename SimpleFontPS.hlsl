#include "simple_struct.fx"
#include "FontStruct.fx"

/////////////
// GLOBALS //
/////////////
Texture2D shaderTexture;
SamplerState SampleType;


////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
float4 PS(PixelFontInput input) : SV_TARGET
{
	float4 color;
	
	// Sample the texture pixel at this location.
	color = shaderTexture.Sample(SampleType, input.tex);

	// If the color is black on the texture then treat this pixel as transparent.
	if (color.r == 0.0f)
	{
		color.a = 0.0f;
	}

	// If the color is other than black on the texture then this is a pixel in the font so draw it using the font pixel color.
	else
	{
		color.a = 1.0f;
		color = color * float4(0, 1, 0, 0);
	}

	return color;
}