#pragma once
#include "stdafx.h"
#include "Socket.h"
#include<iostream>
using namespace GUtils::Network;

Socket::Socket() : m_Socket(0)
{

}

Socket::~Socket()
{

}

BOOL Socket::Connect()
{
	WSADATA wsaData;
	if (WSAStartup(MAKEWORD(2, 2), &wsaData) != WSA_STARTUP_SUCCESS)
		return WSA_STARTUP_FAILURE;

	m_Socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	struct hostent* pHost = gethostbyname("mail.bigpond.com");

	SOCKADDR_IN SockAddr;
	SockAddr.sin_port = htons(110);
	SockAddr.sin_family = AF_INET;
	SockAddr.sin_addr.s_addr = *((unsigned long*)pHost->h_addr);

	connect(m_Socket, (SOCKADDR*)(&SockAddr), sizeof(SockAddr));

	char buffer[1024];
	int nDataLength = recv(m_Socket, buffer, 1024, 0);
	std::cout << buffer;

	return TRUE;
}

BOOL Socket::Disconnect()
{
	shutdown(m_Socket, 0);
	closesocket(m_Socket);

	WSACleanup();

	return TRUE;
}