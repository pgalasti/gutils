#pragma once
#include "stdafx.h"

#include "EventManager.h"

using namespace GameEngine::Logic::Events;

EventManager::EventManager() : m_ActiveQueue(0)
{

}

EventManager::~EventManager()
{

}

BOOL EventManager::AddListener(const EventListenerDelegate& eventDelegate, const EventType type)
{
	EventListenerList& eventListenerList = m_EventListeners[type];
	for (auto iter = eventListenerList.begin(); iter != eventListenerList.end(); iter++)
	{
		if (eventDelegate == (*iter))
			return LISTENER_ALREADY_REGISTERED;
	}

	eventListenerList.push_back(eventDelegate);
	return TRUE;
}

BOOL EventManager::RemoveListener(const EventListenerDelegate& eventDelegate, const EventType type)
{
	auto iterType = m_EventListeners.find(type);
	if (iterType == m_EventListeners.end())
		return TYPE_NOT_FOUND;

	EventListenerList& listeners = iterType->second;
	for (auto listenerIter = listeners.begin(); listenerIter != listeners.end(); listenerIter++)
	{
		if (eventDelegate == (*listenerIter))
		{
			listeners.erase(listenerIter);
			return TRUE;
		}
	}

	return LISTENER_NOT_FOUND;
}

BOOL EventManager::TriggerEventImmediately(const IEventDataPtr& pEvent)
{
	auto iterType = m_EventListeners.find(pEvent->GetEventType());
	if (iterType == m_EventListeners.end())
		return TYPE_NOT_FOUND;

	const EventListenerList& eventListenerList = iterType->second;
	for (auto listenerIter = eventListenerList.begin(); listenerIter != eventListenerList.end(); listenerIter++)
	{
		EventListenerDelegate listener = *listenerIter;
		listener(pEvent);
	}

	return TRUE;
}

BOOL EventManager::QueueEvent(const IEventDataPtr& pEvent)
{
	if (m_EventListeners.find(pEvent->GetEventType()) == m_EventListeners.end())
		return TYPE_NOT_FOUND;

	m_Queues[m_ActiveQueue].push_back(pEvent);
	return TRUE;
}

BOOL EventManager::AbortEvent(const EventType type, bool allOfThisType/* = false*/)
{
	for (auto eventIter = m_Queues[m_ActiveQueue].begin(); eventIter != m_Queues[m_ActiveQueue].end(); eventIter++)
	{
		IEventDataPtr pEventData = *eventIter;
		if (pEventData->GetEventType() == type)
		{
			m_Queues[m_ActiveQueue].erase(eventIter);
			if (!allOfThisType)
				break;
		}
	}

	return TRUE;
}

BOOL EventManager::Update()
{
	// Swap active queue
	ubyte8 queueToProcess = m_ActiveQueue;
	m_ActiveQueue = (m_ActiveQueue + 1) % 2;
	m_Queues[m_ActiveQueue].clear();

	while (!m_Queues[queueToProcess].empty())
	{
		// Get the next event from queue and pop it off
		IEventDataPtr pEventData = m_Queues[queueToProcess].front();
		m_Queues[queueToProcess].pop_front();

		// Get the event listener list associated to event type.
		auto iterType = m_EventListeners.find(pEventData->GetEventType());
		if (iterType == m_EventListeners.end())
		{
			// Warning message
			continue;
		}

		// Iterate through each listener and execute the listener with the event data from queue.
		const EventListenerList& eventListeners = iterType->second;
		for (auto iterListener = eventListeners.begin(); iterListener != eventListeners.end(); iterListener++)
		{
			EventListenerDelegate listener = *iterListener;
			listener(pEventData);
		}
	}

	return TRUE;
}