#include "stdafx.h"
#include "OptionLoader.h"

using GUtils::IO::OptionLoader;

OptionLoader::OptionLoader(std::ifstream& file)
{
	VLoad(file);
}

OptionLoader::OptionLoader(std::string strFilePath)
{
	std::ifstream fileToRead(strFilePath);
	VLoad(fileToRead);
	fileToRead.close();
}

void OptionLoader::VLoad(std::string strFilePath)
{
	std::ifstream fileToRead(strFilePath);
	VLoad(fileToRead);
	fileToRead.close();
}

void OptionLoader::VLoad(std::ifstream& file)
{
	if( !file.is_open() )
		return;

	std::string strLine;
	while( file.good() )
	{
		std::getline( file, strLine );
		m_LineParser.Parse( strLine, ":" );

		if( m_LineParser.getFirst() )
		{
			std::string strTag =  m_LineParser.getToken();
			if( m_LineParser.getNext() )
			{
				std::string strToken = m_LineParser.getToken();
				std::transform(strToken.begin(), strToken.end(), strToken.begin(), ::tolower);

				if( strToken[0] == '"' && strToken.size() > 2)
				{
					strToken = m_LineParser.getToken().substr( 1, strToken.size()-2 );
					m_StringMap[strTag] = strToken;
				}
				else if( !strToken.compare(std::string("true")) || !strToken.compare(std::string("false")) ) 
				{
					m_BoolMap[strTag] = strToken[0] == 't' ? true : false;
				}
				else if( strToken[0] >= '0' && strToken[0] <= '9' ) 
				{
					if( strstr(strToken.c_str(), ".") )
						m_FloatMap[strTag] = atof(strToken.c_str());
					else
						m_IntMap[strTag] = atoi(strToken.c_str());
				}
			}
		}
	}
}

const bool OptionLoader::GetFlagVal(const std::string strTag) const  
{
	auto retIter = m_BoolMap.find(strTag);
	if( retIter != m_BoolMap.end() )
		return retIter->second;
	
	return false;
}

const long OptionLoader::GetIntVal(const std::string strTag) const 
{
	auto retIter = m_IntMap.find(strTag);
	if( retIter != m_IntMap.end() )
		return retIter->second;

	return NULL;
}

const double OptionLoader::GetFloatVal(const std::string strTag) const
{
	auto retIter = m_FloatMap.find(strTag);
	if( retIter != m_FloatMap.end() )
		return retIter->second;

	return 0.0;
}

const std::string OptionLoader::GetTokenVal(const std::string strTag) const
{
	auto retIter = m_StringMap.find(strTag);
	if( retIter != m_StringMap.end() )
		return retIter->second;

	return std::string("");
}

void OptionLoader::VInit()
{
	m_BoolMap.clear();
	m_IntMap.clear();
	m_FloatMap.clear();
	m_StringMap.clear();
}