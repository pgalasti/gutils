#pragma once
#include "stdafx.h"


#include "DirectXMathConversions.h"

using namespace DirectX;

namespace GraphicsEngine {
	namespace Math {
		namespace DirectX {

			XMMATRIX InverseTranspose(CXMMATRIX M)
			{
				// Inverse-transpose is just applied to normals.  So zero out 
				// translation row so that it doesn't get into our inverse-transpose
				// calculation--we don't want the inverse-transpose of the translation.
				XMMATRIX A = M;
				A.r[3] = XMVectorSet(0.0f, 0.0f, 0.0f, 1.0f);

				XMVECTOR det = XMMatrixDeterminant(A);
				return XMMatrixTranspose(XMMatrixInverse(&det, A));
			}
		}
	}
}