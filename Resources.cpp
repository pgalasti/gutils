#pragma once
#include "stdafx.h"

#include "Resources.h"

#include "UtilFunctions.h"

#include<string>

using namespace GUtils::IO::Resources;

void PackageResource::SetName(const std::string& name)
{
	if (name.empty())
		return;

	std::string lowerName = name;
	GUtils::Utils::Functions::ToLower(lowerName);

	m_Name = name;
}

ResourceHandle::ResourceHandle(const PackageResource& resource, const uint32 size, sbyte8* pBuffer)
	: m_Resource(resource)
{
	m_Size = size;
	m_pBuffer = pBuffer;
}

ResourceHandle::~ResourceHandle()
{
	SAFE_ARR_DELETE(m_pBuffer);
}