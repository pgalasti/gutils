#include "StructTexLight.fx"
#include "LightStruct.fx"

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
PSInput VS(VSInput input)
{
	PSInput output = (PSInput)0;
	output.PosW = mul(float4(input.PosL, 1.0), World).xyz;
	output.NormalW = mul(input.Normal, (float3x3)WorldInvTranspose);
	output.PosH = mul(float4(input.PosL, 1.0f), WorldViewProjection);
	output.Textcoord = mul(float4(input.Textcoord, 0.0f, 1.0f), TexTransform).xy;

	return output;
}
