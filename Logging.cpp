#pragma once
#include "stdafx.h"

#include "Logging.h"
#include <string>

using namespace GUtils::Debug;

Logger::Logger(const std::string strPath, Logger::WriteMethod method/* = WriteOnDump*/)
{
	setPath(strPath.c_str());
	SetWriteMethod(method);
}

Logger::Logger(const char* szPath, Logger::WriteMethod method/* = WriteOnDump*/)
{
	setPath(szPath);
	SetWriteMethod(method);
}

Logger::~Logger()
{
	if (m_File.is_open())
		m_File.close();
}

std::string Logger::GetTimeStamp() const
{
	SYSTEMTIME time;
	GetSystemTime(&time);
	char pszTimeStamp[256];
	sprintf_s(pszTimeStamp, "=== %d/%d/%d :: %d:%d:%d:%d ===",
		time.wMonth,
		time.wDay,
		time.wYear,
		time.wHour,
		time.wMinute,
		time.wSecond,
		time.wMilliseconds);

	return std::string(pszTimeStamp);
}

// Will open the file stream.
// Return (bool) : If the file stream state is good.
inline const bool Logger::Startup()
{
	m_File = std::ofstream(m_StrPath);

	return m_File.good();
}

inline const bool Logger::DumpToFile()
{
	for( auto listIter = m_DebugTokens.begin(); listIter != m_DebugTokens.end(); listIter++ )
		m_File << *listIter << std::endl;

	m_DebugTokens.clear();

	return true;
}

inline void Logger::WriteTo(const std::string str)
{
	if( m_bWriteOnFly )
	{
		m_File << GetTimeStamp() << std::endl << str << std::endl;
		return;
	}
	m_DebugTokens.push_back(GetTimeStamp());
	m_DebugTokens.push_back( str );
}

inline void Logger::WriteTo(const char* szStr)
{
	WriteTo( std::string(szStr) );
}

inline void Logger::operator<<(const std::string str)
{
	WriteTo( str );
}

inline void Logger::operator<<(const char* szStr)
{
	WriteTo( szStr );
}

inline void Logger::SetWriteMethod(Logger::WriteMethod method)
{
	m_bWriteOnFly = (method == WriteOnFly);
}

// test
inline void Logger::setPath(const char* szPath)
{
	m_StrPath = std::string(szPath);
}