#ifndef STRUCT_TEXTURE_LIGHT_FXH
#define STRUCT_TEXTURE_LIGHT_FXH
#include "LightStruct.fx"

cbuffer PerObjectBuffer : register(b0)
{
	matrix WorldViewProjection;
	matrix World;
	matrix WorldInvTranspose;
	Material material;
	matrix TexTransform;
}

cbuffer PerFrameBuffer : register(b1)
{
	DirectionalLight directionalLight[3];
	PointLight pointLight[8];
	SpotLight spotLight[8];
	float3 EyePosition;
	int numDirectionalLights;
	int numPointLights;
	int numSpotLights;
}

struct VSInput
{
	float3 PosL : POSITION;
	float3 Normal : NORMAL;
	float2 Textcoord : TEXTCOORD;
};

struct PSInput
{
	float4 PosH : SV_POSITION;
	float3 PosW : POSITION;
	float3 NormalW : NORMAL;
	float2 Textcoord : TEXTCOORD;
};

#endif
