#ifndef SIMPLE_STRUCTURE_FXH
#define SIMPLE_STRUCTURE_FXH
#include "LightStruct.fx"

cbuffer PerObjectBuffer : register(b0)
{
	matrix WorldViewProjection;
}

struct VSInput
{
	float4 Pos : POSITION;
	float4 Color : COLOR;
};

struct PSInput
{
	float4 Pos : SV_POSITION;
	float4 Color : COLOR;
};

#endif