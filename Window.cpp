#pragma once
#include "stdafx.h" 

//#include "DirectXMathConversions.h"
#include "Window.h"
#include "PollInput.h"
#include "WndProc.h"

using GUtils::System::GameWindow;

GameWindow::GameWindow()
{
	m_hInstance = NULL;
	m_hWND = NULL;
	m_pGraphicsEngine = nullptr;
	m_bFullScreen = false;
	m_ApplicationName = std::string("");
	m_WindowTitle = std::string("");

	//INITIALIZE_LOG("C:\\Users\\Paul\\Documents\\errors\\GameWindow_log.txt");
	INITIALIZE_LOG("C:\\Users\\public\\GameWindow_log.txt");
}

GameWindow::~GameWindow()
{
	DELETE_LOG;

	// We don't want to do this because it's not owned by GameWindow object.
	// Need to rethink design.
	//SAFE_DELETE(m_pGraphicsEngine); 
}


BOOL GameWindow::Initialize(IGraphicEngine* pGraphicEngine, const EngineOptions& options)
{
	DEBUG_LOG(LOG, "Initializing System Window");
	if (!this->InitializeSystemWindow(options.width, options.height))
	{
		DEBUG_LOG(LOG, "IGameEngine::InitializeSystemWindow() failure");
		return FALSE;
	}

	m_pGraphicsEngine = pGraphicEngine;

	DEBUG_LOG(LOG, "Initializing Engine");
	if (!m_pGraphicsEngine->Initialize(m_hWND, options))
	{
		DEBUG_LOG(LOG, "IGameEngine::Initialize() failure");
		return FALSE;
	}

	return TRUE;
}

BOOL GameWindow::CleanUp()
{
	DEBUG_LOG(LOG, "Performing GameWindow::Cleanup()");

	if (m_pGraphicsEngine)
	{
		if (!m_pGraphicsEngine->Shutdown())
		{
			DEBUG_LOG(LOG, "GameWindow failed to perform IGraphicsEngine::Shutdown()");
		}
	}

	ShowCursor(true);

	if (m_bFullScreen)
		ChangeDisplaySettings(NULL, 0);

	DestroyWindow(m_hWND);
	m_hWND = NULL;

	UnregisterClass(m_ApplicationName.c_str(), m_hInstance);
	m_hInstance = NULL;

	pGameWindowReference = nullptr;
	return TRUE;
}

BOOL GameWindow::Execute()
{
	DEBUG_LOG(LOG, "Performing GameWindow::Execute()");
	m_Timer.Reset();
	m_Timer.Start();
	MSG msg;
	ZeroMemory(&msg, sizeof(MSG));

	bool executeLoop = true;
	
	
	while (executeLoop)
	{
		bool updateGame = true;
		// Look at message
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			// Translate and dispatch to procedure
			TranslateMessage(&msg);
			DispatchMessage(&msg);
			
			// Jump out of loop if message is to quit
			if (msg.message == WM_QUIT)
			{
				DEBUG_LOG(LOG, "GameWindow::Execute() quit message has been sent to window.");
				executeLoop = false;
			}
		}

		if (updateGame)
		{
			m_Timer.Tick();
			const float deltaTime = m_Timer.DeltaTime();

			// ========================================================================================
			// Update game logic
			if (!UpdateGameState(deltaTime))
			{
				DEBUG_LOG(LOG, "IGameEngine::Execute() -> IGameEngine::UpdateGameState() failure");
				return FALSE;
			}

			// ========================================================================================
			// Render the scene
			if (!Render(deltaTime))
			{
				DEBUG_LOG(LOG, "IGameEngine::Execute() -> IGameEngine::Render() failure");
				return FALSE;
			}

			// ========================================================================================
			// FPS Calculations
			static float fps = 0.0f;
			float latestFPS = m_pGraphicsEngine->FPS();
			uint32 latestVerticies = m_pGraphicsEngine->GetLoadedVerticiesNumber();
			uint32 latestIndicies = m_pGraphicsEngine->GetLoadedIndiciesNumber();

			if (fps != latestFPS)
			{
				fps = latestFPS;
				std::string title;
				title = m_ApplicationName
					+ std::string(" - FPS: ").append(std::to_string(latestFPS))
					+ std::string(" - Vertex: ").append(std::to_string(latestVerticies))
					+ std::string(" - Index: ").append(std::to_string(latestIndicies));

				SetWindowText(m_hWND, title.c_str());
			}
		}

	}

	return TRUE;
}

BOOL GameWindow::Render(const float dt)
{
	if (!m_pGraphicsEngine->Render(dt))
	{
		DEBUG_LOG(LOG, "IGameEngine::Render() -> m_pGraphicsEngine::Render() failure");
		return FALSE;
	}
	return TRUE;
}

LRESULT CALLBACK GameWindow::MessageHandler(HWND hwnd, UINT umsg, WPARAM wparam, LPARAM lparam)
{
	switch (umsg)
	{
	case WM_KEYDOWN:
	{
		char abcdef[2];
		abcdef[0] = MapVirtualKey(wparam, MAPVK_VK_TO_CHAR);
		abcdef[1] = 0;
		//printf("KeyDown: %s\n", abcdef);
		m_InputPoll.SetCharacter(abcdef[0]);
		int a = 0;
		break;
	}
	case WM_KEYUP:
	{
		char abcdef[2];
		abcdef[0] = MapVirtualKey(wparam, MAPVK_VK_TO_CHAR);
		abcdef[1] = 0;
		//printf("KeyUp: %s\n", abcdef);
		m_InputPoll.UnsetCharacter(abcdef[0]);
		break;
	}
	case WM_LBUTTONDOWN:
	{
		m_InputPoll.SetMouseButtonDown(PollInput::MouseButton::Left);
		int a = 0;
		break;
	}
	case WM_RBUTTONDOWN:
	{
		m_InputPoll.SetMouseButtonDown(PollInput::MouseButton::Right);
		int a = 0;
		break;
	}
	case WM_LBUTTONUP:
	{
		m_InputPoll.UnsetMouseButtonDown(PollInput::MouseButton::Left);
		int a = 0;
		break;
	}
	case WM_RBUTTONUP:
	{
		m_InputPoll.UnsetMouseButtonDown(PollInput::MouseButton::Right);
		int a = 0;
		break;
	}
	case WM_MBUTTONDOWN:
	{
		m_InputPoll.SetMouseButtonDown(PollInput::MouseButton::Middle);
		int a = 0;
		break;
	}
	case WM_MBUTTONUP:
	{
		m_InputPoll.UnsetMouseButtonDown(PollInput::MouseButton::Middle);
		int a = 0;
		break;
	}

	case WM_MOUSEMOVE:
	{
		m_InputPoll.SetCursorPosition(LOWORD(lparam), HIWORD(lparam));
		break;
	}
	default:
	{
		
	}
	}
	return DefWindowProc(hwnd, umsg, wparam, lparam);
}

BOOL GameWindow::InitializeSystemWindow(uint32 screenWidth, uint32 screenHeight)
{
	WNDCLASSEX wc;
	DEVMODE dmScreenSettings;
	int posX, posY;

	pGameWindowReference = this;
	m_hInstance = GetModuleHandle(NULL);

	if (m_hInstance == NULL)
	{
		DEBUG_LOG(LOG, "Null instance GameWindow::InitializeSystemWindow()");
		return FALSE;
	}

	wc.style = 0;// CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = m_hInstance;
	wc.hIcon = LoadIcon(NULL, IDI_ERROR);
	wc.hIconSm = wc.hIcon;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = m_ApplicationName.c_str();
	wc.cbSize = sizeof(WNDCLASSEX);

	// Register the window class.
	RegisterClassEx(&wc);

	// Determine the resolution of the clients desktop screen.
	uint32 fullScreenWidth = GetSystemMetrics(SM_CXSCREEN);
	uint32 fullScreenHeight = GetSystemMetrics(SM_CYSCREEN);

	// Setup the screen settings depending on whether it is running in full screen or in windowed mode.
	if (m_bFullScreen)
	{
		// If full screen set the screen to maximum size of the users desktop and 32bit.
		memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));
		dmScreenSettings.dmSize = sizeof(dmScreenSettings);
		dmScreenSettings.dmPelsWidth = (unsigned long)fullScreenWidth;
		dmScreenSettings.dmPelsHeight = (unsigned long)fullScreenHeight;
		dmScreenSettings.dmBitsPerPel = 32;
		dmScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

		// Change the display settings to full screen.
		ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN);

		// Set the position of the window to the top left corner.
		posX = posY = 0;
	}
	else
	{
		// If windowed then set it to 800x600 resolution.
		//screenWidth = 800;
		//screenHeight = 600;

		// Place the window in the middle of the screen.
		posX = (GetSystemMetrics(SM_CXSCREEN) - screenWidth) / 2;
		posY = (GetSystemMetrics(SM_CYSCREEN) - screenHeight) / 2;
	}

	m_hWND = CreateWindowEx(WS_EX_CLIENTEDGE, m_ApplicationName.c_str(), m_ApplicationName.c_str(),
		WS_OVERLAPPEDWINDOW,
		posX, posY, screenWidth, screenHeight, NULL, NULL, m_hInstance, NULL);

	if (m_hWND == NULL)
	{
		DEBUG_LOG(LOG, "Window handle is null GameWindow::InitializeSystemWindow()");
		return FALSE;
	}

	// Bring the window up on the screen and set it as main focus.
	ShowWindow(m_hWND, SW_SHOW);
	SetForegroundWindow(m_hWND);
	SetFocus(m_hWND);

	ShowCursor(true);

	return TRUE;

}

#pragma region deprecated
#ifdef DEPRECATED_WINDOW_CLASS

using Framework::System::SystemWindow;
SystemWindow::SystemWindow()
{
	m_ApplicationName.clear();
	m_WindowTitle.clear();
	m_hInstance = NULL;
	m_hWND = NULL;
	m_pLog = nullptr;
	m_pEngine = nullptr;
}

SystemWindow::~SystemWindow()
{
	if (m_pLog)
		m_pLog->DumpToFile();
	
	SAFE_DELETE(m_pEngine);
	SAFE_DELETE(m_pLog);
}

BOOL SystemWindow::Initialize()
{
	unsigned int windowX = 0, windowY = 0;

	m_pLog = new GUtils::Debug::Logger(ERROR_LOG_PATH);
	if (!m_pLog->Startup())
		return FALSE;

	if (!InitializeWindow(windowX, windowY))
		return FALSE;

	EngineOptions options;
	options.height = 600;
	options.width = 800;
	options.screenDepth = 1000.0f;
	options.screenNear = 0.1f;
	options.VSync = true;
	options.FullScreen = m_bFullScreen;

	m_pEngine = new SimpleEngine();
	if (!m_pEngine->Initialize(m_hWND, options))
		return FALSE;
	// Virtual input and graphics

	return TRUE;
}

BOOL SystemWindow::InitializeWindow(unsigned int& screenW, unsigned int& screenY)
{
	WNDCLASSEX wc;
	DEVMODE dmScreenSettings;
	int posX, posY;

	ApplicationHandle = this;
	m_hInstance = GetModuleHandle(NULL);

	if (m_hInstance == NULL)
	{
		m_pLog->WriteTo("Unable to initialize application instance!");
		return FALSE;
	}

	wc.style = 0;// CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = m_hInstance;
	wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);
	wc.hIconSm = wc.hIcon;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(GRAY_BRUSH);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = m_ApplicationName.c_str();
	wc.cbSize = sizeof(WNDCLASSEX);

	// Register the window class.
	RegisterClassEx(&wc);

	// Determine the resolution of the clients desktop screen.
	screenW = GetSystemMetrics(SM_CXSCREEN);
	screenY = GetSystemMetrics(SM_CYSCREEN);

	// Setup the screen settings depending on whether it is running in full screen or in windowed mode.
	if (m_bFullScreen)
	{
		// If full screen set the screen to maximum size of the users desktop and 32bit.
		memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));
		dmScreenSettings.dmSize = sizeof(dmScreenSettings);
		dmScreenSettings.dmPelsWidth = (unsigned long)screenW;
		dmScreenSettings.dmPelsHeight = (unsigned long)screenY;
		dmScreenSettings.dmBitsPerPel = 32;
		dmScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

		// Change the display settings to full screen.
		ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN);

		// Set the position of the window to the top left corner.
		posX = posY = 0;
	}
	else
	{
		// If windowed then set it to 800x600 resolution.
		screenW = 800;
		screenY = 600;

		// Place the window in the middle of the screen.
		posX = (GetSystemMetrics(SM_CXSCREEN) - screenW) / 2;
		posY = (GetSystemMetrics(SM_CYSCREEN) - screenY) / 2;
	}

	m_hWND = CreateWindowEx(WS_EX_CLIENTEDGE, m_ApplicationName.c_str(), m_ApplicationName.c_str(),
		WS_OVERLAPPEDWINDOW,
		posX, posY, screenW, screenY, NULL, NULL, m_hInstance, NULL);

	if (m_hWND == NULL)
	{
		m_pLog->WriteTo("Unable to create application window!");
		return FALSE;
	}

	// Bring the window up on the screen and set it as main focus.
	ShowWindow(m_hWND, SW_SHOW);
	SetForegroundWindow(m_hWND);
	SetFocus(m_hWND);

	ShowCursor(true);

	return TRUE;
}

BOOL SystemWindow::Shutdown()
{
	// Virtual input and graphics
	if (m_pEngine)
		m_pEngine->Shutdown();
	ShutdownWindows();

	return TRUE;
}

void SystemWindow::ShutdownWindows()
{
	ShowCursor(true);

	if (m_bFullScreen)
		ChangeDisplaySettings(NULL, 0);

	DestroyWindow(m_hWND);
	m_hWND = NULL;

	UnregisterClass(m_ApplicationName.c_str(), m_hInstance);
	m_hInstance = NULL;

	ApplicationHandle = nullptr;
}

void SystemWindow::Run()
{
	MSG msg;
	ZeroMemory(&msg, sizeof(MSG));

	for (;;)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		if (msg.message == WM_QUIT)
			return;
		else
		{
			if (!Frame())
				return;
		}

	}
}

BOOL SystemWindow::Frame()
{
	// Virtual input and graphics
	if (!m_pEngine->Render())
		return FALSE;

	SetWindowText(m_hWND, (LPCSTR)m_WindowTitle.c_str());

	return TRUE;
}

LRESULT CALLBACK SystemWindow::MessageHandler(HWND hwnd, UINT umsg, WPARAM wparam, LPARAM lparam)
{
	switch (umsg)
	{
		case WM_KEYDOWN:
		{
			//m_Input->KeyDown((unsigned int)wparam);
			return 0;
		}
		case WM_KEYUP:
		{
			//m_Input->KeyUp((unsigned int)wparam);
			return 0;
		}
		default:
		{
			return DefWindowProc(hwnd, umsg, wparam, lparam);
		}
	}
}

#endif

#pragma endregion deprecated