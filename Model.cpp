#pragma once
#include "Model.h"
#include "stdafx.h" 

using namespace GraphicsEngine::Model::DirectX;

SimpleModel::SimpleModel() : m_pVertexBuffer(nullptr), m_pIndexBuffer(nullptr)
{
	ClearBuffers();
}

SimpleModel::~SimpleModel()
{
	ClearBuffers();
}

void SimpleModel::ClearBuffers()
{
	if (m_pVertexBuffer)
		m_pVertexBuffer->Release();
	if (m_pIndexBuffer)
		m_pIndexBuffer->Release();

	m_pVertexBuffer = nullptr;
	m_pIndexBuffer = nullptr;
	m_nVertices = m_nIndicies = 0;
}

BOOL SimpleModel::InitializeModel(ID3D11Device* pDevice, const SimpleVertex* pVerticies, const UINT nVerticies, const UINT* pIndicies)
{
	if (pDevice == nullptr || pVerticies == nullptr || pIndicies == nullptr || nVerticies == 0)
		return FALSE;

	m_nVertices = nVerticies;
	m_nIndicies = nVerticies;

	D3D11_BUFFER_DESC vertexBufferDesc;
	D3D11_BUFFER_DESC indexBufferDesc;
	D3D11_SUBRESOURCE_DATA vertexData;
	D3D11_SUBRESOURCE_DATA indexData;

	vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	vertexBufferDesc.ByteWidth = sizeof(SimpleVertex) * m_nVertices;
	vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags = 0;
	vertexBufferDesc.MiscFlags = 0;
	vertexBufferDesc.StructureByteStride = 0;

	vertexData.pSysMem = pVerticies;
	vertexData.SysMemPitch = 0;
	vertexData.SysMemSlicePitch = 0;

	if (FAILED(pDevice->CreateBuffer(&vertexBufferDesc, &vertexData, &m_pVertexBuffer)))
		return FALSE;
		
	indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
	indexBufferDesc.ByteWidth = sizeof(UINT)* m_nIndicies;
	indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags = 0;
	indexBufferDesc.MiscFlags = 0;
	indexBufferDesc.StructureByteStride = 0;

	indexData.pSysMem = pIndicies;
	indexData.SysMemPitch = 0;
	indexData.SysMemSlicePitch = 0;

	if (FAILED(pDevice->CreateBuffer(&indexBufferDesc, &indexData, &m_pIndexBuffer)))
		return FALSE;

	return TRUE;
}

void SimpleModel::Render(ID3D11DeviceContext* pDeviceContext)
{
	uint32 stride;
	uint32 offset;

	stride = sizeof(SimpleVertex);
	offset = 0;

	pDeviceContext->IASetVertexBuffers(0, 1, &m_pVertexBuffer, &stride, &offset);
	pDeviceContext->IASetIndexBuffer(m_pIndexBuffer, DXGI_FORMAT_R32_UINT, 0);
	pDeviceContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}