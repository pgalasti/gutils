#pragma once
#include "stdafx.h"

#include "ActorFactory.h"

#include "XMLParser.h"

using namespace GameEngine::Logic::Actors;
using namespace GUtils::Utils::XML::PickyParser;

ActorFactory::ActorFactory()
{
	m_pIdGenerator = new IDGenerator(1);

	std::string defaultPath = DEFAULT_LOG_PATH;
	defaultPath += "ActorFactory.log";
	INITIALIZE_LOG(defaultPath);

	DEBUG_LOG(LOG, "Creating actor factory.");
}

ActorFactory::~ActorFactory()
{
	SAFE_DELETE(m_pIdGenerator);
	
	DEBUG_LOG(LOG, "Destroying actor factory.");

	DELETE_LOG
}

StrongActorPtr ActorFactory::CreateActor(const char* pszActorResource)
{
	// Parse the XML document and get the top level element
	XmlParser xmlParser;
	XmlDocument xmlDocument;
	if (!xmlParser.Parse(pszActorResource, xmlDocument))
	{
		DEBUG_LOG(LOG, std::string("Unable to parse xml resource to create actor: ") + std::string(pszActorResource));
		return StrongActorPtr();
	}
	ElementPtr pTopElement = xmlDocument.m_pTopElement;

	// Create the new actor and initalize it with the top element
	const ActorId actorId = m_pIdGenerator->GetNextId();
	StrongActorPtr pNewActor = StrongActorPtr(new Actor(actorId));
	if (!pNewActor->Init(pTopElement))
	{
		DEBUG_LOG(LOG, std::string("Unable to initialize actor using top element with name: ") + std::string(pTopElement->m_Name));
		return StrongActorPtr();
	}

	// For each child element, we need to create the appropriate component and add to new actor.
	std::vector<ElementPtr> childElements = pTopElement->GetChildElements();
	for (auto elementIter = childElements.begin(); elementIter != childElements.end(); elementIter++)
	{
		StrongActorComponentPtr pComponent(CreateComponent(*elementIter));
		if (!pComponent)
			return StrongActorPtr();
	
		pNewActor->AddComponent(pComponent);
		pComponent->SetOwner(pNewActor);
	}

	pNewActor->PostInit();
	return pNewActor;
}

void ActorFactory::AddComponentCreator(const std::string& componentName, ActorComponentCreatorFunc creationFunction)
{
	m_ActorComponentCreators[componentName] = creationFunction;
}

StrongActorComponentPtr ActorFactory::CreateComponent(ElementPtr pElement)
{
	// Find the creation function from the factory map
	std::string name(pElement->m_Name);

	auto foundCreatorIter = m_ActorComponentCreators.find(name);
	if (foundCreatorIter == m_ActorComponentCreators.end())
	{
		DEBUG_LOG(LOG, std::string("Unable to find component creator for element name: ") + name);
		return StrongActorComponentPtr();
	}

	ActorComponentCreatorFunc creationFunction = foundCreatorIter->second;
	StrongActorComponentPtr pComponent = StrongActorComponentPtr(creationFunction());

	if (!pComponent)
	{
		DEBUG_LOG(LOG, std::string("Component was not created when using element: ") + name);
		return StrongActorComponentPtr();
	}

	if (!pComponent->Init(pElement))
	{
		DEBUG_LOG(LOG, std::string("Unable to initialize component using element with name: ") + name);
		return StrongActorComponentPtr();
	}

	return pComponent;
}