#pragma once
#include "stdafx.h"
#include "ThreadPool.h"

#include<functional>
#include<future>

using namespace GUtils::Concurrency;

// NOT FINISHED

ThreadPool::ThreadPool() : ThreadPool(1)
{
	
}

ThreadPool::ThreadPool(uint32 numConcurrentThreads) : m_DoMonitor(false)
{
	this->SetConcurrentLimit(numConcurrentThreads);
	m_pMonitorThread = nullptr;
}

ThreadPool::~ThreadPool()
{
	m_RunningThreads.clear();

	this->Shutdown();
	std::lock_guard<std::mutex> lock(m_QueueMutex);
	while (!m_ThreadQueue.empty())
		m_ThreadQueue.pop();
}

void ThreadPool::SetConcurrentLimit(const uint32 numConcurrentThreads)
{
	m_Limit = numConcurrentThreads;
}

void ThreadPool::Execute()
{
	if (m_pMonitorThread)
		return;

	this->SetMonitor(true);
	m_pMonitorThread = new std::thread(&ThreadPool::ThreadMonitor, this);
}

void ThreadPool::Shutdown()
{
	this->SetMonitor(false);
	this->m_RunningThreads.clear();

	if (this->m_pMonitorThread)
		this->m_pMonitorThread->join();

	SAFE_DELETE(this->m_pMonitorThread);
}

void ThreadPool::ThreadMonitor()
{
	for (;;)
	{
		// Break monitor checking if signalled.
		if (!this->IsMonitoring())
			return;

		m_ThreadListMutex.lock();
		
		// Check the status of each running thread
		std::list<std::future<void>> threadsToKeep;
		for (auto iter = m_RunningThreads.begin(); iter != m_RunningThreads.end(); iter++)
		{
			auto status = iter->wait_for(std::chrono::milliseconds(0));
			
			switch (status)
			{
			case std::future_status::ready :
				break;
			default :
				threadsToKeep.push_back(std::move(*iter));
				break;
			}
		}

		// Remove threads that have been marked for complete.
		m_RunningThreads.clear();
		for (auto iter = threadsToKeep.begin(); iter != threadsToKeep.end(); iter++)
			m_RunningThreads.push_back(std::move(*iter));

		m_QueueMutex.lock();
		// Transfer threads over to running from queued list if limit hasn't been reached.
		while (m_RunningThreads.size() < m_Limit)
		{
			// If there's nothing in queue there's nothing to throw into running.
			if (m_ThreadQueue.empty())
				break;

			// Pop from queue and add to running thread.
			std::future<void> thread = std::async(std::launch::async, m_ThreadQueue.front());
			m_ThreadQueue.pop();
			m_RunningThreads.push_back(std::move(thread));
		}
		m_QueueMutex.unlock();

		m_ThreadListMutex.unlock();

		// Give monitor thread some rest per cycle
		Sleep(ThreadPool::MONITOR_CHECK);
	}
}

void ThreadPool::SetMonitor(const bool monitor /*=true*/)
{
	std::lock_guard<std::mutex> lock(m_MonitorMutex);
	this->m_DoMonitor = monitor;
}

bool ThreadPool::IsMonitoring()
{
	std::lock_guard<std::mutex> lock(m_MonitorMutex);
	return this->m_DoMonitor;
}

void ThreadPool::AddThread(const std::function <void(void)>& func)
{
	std::lock_guard<std::mutex> lock(m_QueueMutex);
	m_ThreadQueue.push(func);
}

void ThreadPool::AddThreads(std::list<std::function <void(void)>> functions)
{
	for (auto iter : functions)
		this->AddThread(std::move(iter));
}

void ThreadPool::AddThreads(std::queue<std::function <void(void)>> functions)
{

}

uint32 ThreadPool::GetRunningThreadCount()
{
	std::lock_guard<std::mutex> lock(m_ThreadListMutex);
	return this->m_RunningThreads.size();
}

uint32 ThreadPool::GetQueuedThreadCount()
{
	std::lock_guard<std::mutex> lock(m_QueueMutex);
	return this->m_ThreadQueue.size();
	
}

bool ThreadPool::IsRunning()
{
	return this->IsMonitoring();
}