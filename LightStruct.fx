#ifndef LIGHT_STRUCTURES_FXH
#define LIGHT_STRUCTURES_FXH

struct Material
{
	float4 Ambient;
	float4 Diffuse;
	float4 Specular;
};

struct DirectionalLight
{
	float4 Ambient;
	float4 Diffuse;
	float4 Specular;
	float3 Direction;
	float pad; // For padding
};

struct PointLight
{
	float4 Ambient;
	float4 Diffuse;
	float4 Specular;

	float3 Position;
	float Range;

	float3 Attenuation;
	float pad; // For padding
};

struct SpotLight
{
	float4 Ambient;
	float4 Diffuse;
	float4 Specular;

	float3 Position;
	float Range;

	float3 Direction;
	float Spot;

	float3 Attenuation;
	float pad; // For padding
};

#endif