#pragma once
#include "stdafx.h"

#include "ObjFormatLoader.h"

#include "TestMesh.h"
#include "FileLoader.h"
#include "Parser.h"
#include<iostream>
#include "GMath.h"

using namespace GraphicsEngine::Model;
using namespace GUtils::GMath;
using namespace GUtils::IO;
using namespace GUtils::Util;

/*static */
BOOL ObjFormatLoader::LoadFile(const char* pszPath, IMesh** pMeshData)
{
	FileLoader fileLoader;

	if (!fileLoader.Open(pszPath))
		return FALSE;

	std::list<std::string> fileLines;
	if (!fileLoader.GetFileLines(fileLines))
		return FALSE;

	if (!fileLoader.Close())
		return FALSE;
	
	std::vector<SimpleVertex> simpleVerticies;
	std::vector<PosNormalVertex> poitionsNormalVerticies;
	std::vector<WORD> indicies;
	ModelType type = DetermineFileType(fileLines);
	switch (type)
	{
	case SimpleVertexType:

		ObjFormatLoader::ExtractSimpleVertex(fileLines, simpleVerticies, indicies);

		*pMeshData = new TestMesh();
		((TestMesh*)(*pMeshData))->SetVerticies(&simpleVerticies[0], simpleVerticies.size(), &indicies[0], indicies.size());
		return TRUE;

	case PositionNormalType:
		
		ObjFormatLoader::ExtractPosNormalVertex(fileLines, poitionsNormalVerticies, indicies);

		*pMeshData = new PosNormalMesh();
		((PosNormalMesh*)(*pMeshData))->SetVerticies(&poitionsNormalVerticies[0], poitionsNormalVerticies.size(), &indicies[0], indicies.size());
		return TRUE;
	}

	return FALSE;
}

//static
BOOL ObjFormatLoader::ExtractSimpleVertex(const std::list<std::string>& fileLines, std::vector<SimpleVertex>& simpleVerticies, std::vector<WORD>& indicies)
{
	simpleVerticies.clear();
	StringParser parser;
	for (std::string line : fileLines)
	{
		// Skip comments
		if (line[0] == COMMENT_INDICATOR)
			continue;

		if (line[0] == VERTEX_INDICATOR)
		{
			// Skip normals for SimpleVertex
			if (line[1] == NORMAL_INDICATOR)
				continue;

			parser.Parse(line, " ");
			if (!parser.getFirst())
				continue;

			XMFLOAT3 pos;

			ZeroMemory(&pos, sizeof XMFLOAT3);

			std::string value;
			while (parser.getNext()) // Skip the 'VERTEX_INDICATOR' value
			{
				value = parser.getToken();
				if (!value.empty())
					break;
			}
			pos.x = (float)atof(value.c_str());

			while (parser.getNext())
			{
				value = parser.getToken();
				if (!value.empty())
					break;
			}
			pos.y = (float)atof(value.c_str());

			while (parser.getNext())
			{
				value = parser.getToken();
				if (!value.empty())
					break;
			}
			pos.z = (float)atof(value.c_str());

			SimpleVertex vertex;
			vertex.Pos = pos;
			// hardcode red for now
			vertex.Color.x = 1.0f;
			vertex.Color.w = 1.0f;

			simpleVerticies.push_back(vertex);
		}

		if (line[0] == FACE_INDICATOR)
		{
			parser.Parse(line, " ");
			if (!parser.getFirst())
				continue;

			while (parser.getNext()) // Skip the 'NORMAL_INDICATOR' value
			{
				std::string values = parser.getToken();
				if (values.empty())
					continue;

				WORD indexValue = -1;
				WORD normalValue = -1; // no used here
				if (!GetValue(values.c_str(), indexValue, normalValue))
					continue; // return FALSE;

				indicies.push_back(indexValue - 1);
			}
		}
	}

	return TRUE;
}

//static

BOOL ObjFormatLoader::ExtractPosNormalVertex(const std::list<std::string>& fileLines, std::vector<PosNormalVertex>& positionNormalVerticies, std::vector<WORD>& indicies)
{
	// it should load vertex first based on index matching index value below
	// it should then load the normal values next
	// indicies and normal indicies load last

	//o Cube_Cube.001
	//	v - 0.477888 - 0.868046 0.569747
	//	v - 0.477888 1.131954 0.569747
	//	v - 0.477888 - 0.868046 - 1.430253
	//	v - 0.477888 1.131954 - 1.430253
	//	v 1.522112 - 0.868046 0.569747
	//	v 1.522112 1.131954 0.569747
	//	v 1.522112 - 0.868046 - 1.430253
	//	v 1.522112 1.131954 - 1.430253
	//	vn - 1.000000 0.000000 0.000000
	//	vn 0.000000 0.000000 - 1.000000
	//	vn 1.000000 0.000000 0.000000
	//	vn 0.000000 0.000000 1.000000
	//	vn 0.000000 - 1.000000 0.000000
	//	vn 0.000000 1.000000 0.000000
	//	usemtl None
	//	s off
	//	f 4//1 3//1 1//1   THE SECOND VALUE MAPS THE TRIANGLE TO THE VERTEX NORMAL
	//	f 8//2 7//2 3//2
	//	f 6//3 5//3 7//3	WHICH MEANS I NEED A WAY TO FIGURE THAT OUT...
	//	f 2//4 1//4 5//4
	//	f 3//5 7//5 5//5
	//	f 8//6 4//6 2//6
	//	f 2//1 4//1 1//1
	//	f 4//2 8//2 3//2
	//	f 8//3 6//3 7//3
	//	f 6//4 2//4 5//4
	//	f 1//5 3//5 5//5
	//	f 6//6 8//6 2//6

	// FORMAT TO USE WITH SMOOTH
	//v 1.000000 - 1.000000 - 1.000000
	//	v 1.000000 - 1.000000 1.000000
	//	v - 1.000000 - 1.000000 1.000000
	//	v - 1.000000 - 1.000000 - 1.000000
	//	v 1.000000 1.000000 - 0.999999
	//	v 0.999999 1.000000 1.000001
	//	v - 1.000000 1.000000 1.000000
	//	v - 1.000000 1.000000 - 1.000000
	//	vn 0.577300 - 0.577300 0.577300
	//	vn - 0.577300 - 0.577300 0.577300
	//	vn - 0.577300 - 0.577300 - 0.577300
	//	vn - 0.577300 0.577300 - 0.577300
	//	vn - 0.577300 0.577300 0.577300
	//	vn 0.577300 0.577300 0.577300
	//	vn 0.577300 0.577300 - 0.577300
	//	vn 0.577300 - 0.577300 - 0.577300
	//	s 1
	//	f 2//1 3//2 4//3
	//	f 8//4 7//5 6//6
	//	f 5//7 6//6 2//1
	//	f 6//6 7//5 3//2
	//	f 3//2 7//5 8//4
	//	f 1//8 4//3 8//4
	//	f 1//8 2//1 4//3
	//	f 5//7 8//4 6//6
	//	f 1//8 5//7 2//1
	//	f 2//1 6//6 3//2
	//	f 4//3 3//2 8//4
	//	f 5//7 1//8 8//4

	positionNormalVerticies.clear();

	std::vector<GMath::Vector3DValues<float32>> normals;
	std::vector <std::pair<WORD, WORD>> indexNormals;
	StringParser parser;
	for (std::string line : fileLines)
	{
		// Skip comments
		if (line[0] == COMMENT_INDICATOR)
			continue;

		if (line[0] == VERTEX_INDICATOR)
		{
			parser.Parse(line, " ");
			if (!parser.getFirst())
				continue;

			if (line[1] == NORMAL_INDICATOR)
			{
				GMath::Vector3DValues<float32> normalValues = Extract3Vec(line);
				normals.push_back(normalValues);
				continue;
			}

			PosNormalVertex vertex;
			vertex.Pos = Extract3Vec(line);
			positionNormalVerticies.push_back(vertex);
		}

		if (line[0] == FACE_INDICATOR)
		{
			parser.Parse(line, " ");
			if (!parser.getFirst())
				continue;

			while (parser.getNext()) // Skip the 'NORMAL_INDICATOR' value
			{
				std::string values = parser.getToken();
				if (values.empty())
					continue;

				WORD indexValue = -1;
				WORD normalValue = -1;
				if (!GetValue(values.c_str(), indexValue, normalValue))
					continue;// return FALSE;

				indicies.push_back(indexValue - 1);

				std::pair<WORD, WORD> indexNormalIndexPair;
				indexNormalIndexPair.first = indexValue - 1;
				indexNormalIndexPair.second = normalValue - 1;
				indexNormals.push_back(indexNormalIndexPair);
			}
		}
	}

	for (uint32 i = 0; i < indexNormals.size(); i++)
	{
		int index = indexNormals[i].first;
		int normalIndex = indexNormals[i].second;
		positionNormalVerticies[index].Normal = normals[normalIndex];
	}

	return TRUE;
}

/*static */
Vector3DValues<float32> ObjFormatLoader::Extract3Vec(std::string line)
{
	GMath::Vector3DValues<float32> values;
	StringParser parser;
	std::string value;
	parser.Parse(line, " ");
	parser.getFirst();

	while (parser.getNext()) // Skip the 'VERTEX_INDICATOR' value
	{
		value = parser.getToken();
		if (!value.empty())
			break;
	}
	values.x = (float)atof(value.c_str());

	while (parser.getNext())
	{
		value = parser.getToken();
		if (!value.empty())
			break;
	}
	values.y = (float)atof(value.c_str());

	while (parser.getNext())
	{
		value = parser.getToken();
		if (!value.empty())
			break;
	}
	values.z = (float)atof(value.c_str());

	return values;
}

/*static*/ 
BOOL ObjFormatLoader::GetValue(const char* dirtyValues, WORD& value, WORD& normal)
{
	StringParser valueParser;
	valueParser.Parse(dirtyValues, "//");

	if (!valueParser.getFirst())
		return FALSE;
	std::string strValue = valueParser.getToken();

	value = (WORD)atoi(strValue.c_str());
	if (value == 0)
		return FALSE;

	while(valueParser.getNext())
	{
		strValue = valueParser.getToken();
		if (strValue.empty())
			continue;

		normal = (WORD)atoi(strValue.c_str());
		break;
	}

	return TRUE;
}

/*static */
ModelType ObjFormatLoader::DetermineFileType(const std::list<std::string>& fileLines)
{
	bool hasVertex = false;
	for (std::string line : fileLines) // Optimize this later
	{
		if (line[0] == VERTEX_INDICATOR && line[1] == NORMAL_INDICATOR)
		{
			return PositionNormalType;
		}

		if (line[0] == VERTEX_INDICATOR)
		{
			hasVertex = true;
		}
	}

	if (hasVertex)
	{
		return SimpleVertexType;
	}

	return UnknownType;
}