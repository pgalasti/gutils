#include "simple_struct.fx"
#include "FontStruct.fx"

PixelFontInput VS(VertexFontInput input)
{
	PixelFontInput output;

	// Change the position vector to be 4 units for proper matrix calculations.
	input.position.w = 1.0f;

	// Calculate the position of the vertex against the world, view, and projection matrices.
	output.position = mul(input.position, WorldViewProjection);

	// Store the texture coordinates for the pixel shader.
	output.tex = input.tex;

	return output;
}