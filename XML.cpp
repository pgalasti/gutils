#pragma once
#include "stdafx.h"

#include "XML.h"

#include "Parser.h"
#include <set>
#include <stack>

using namespace GUtils::Utils::XML;

RootElement::RootElement()
{

}

RootElement::~RootElement()
{
	//for (Attribute *pAttribute : m_Atributes)
	//{
	//	SAFE_DELETE(pAttribute);
	//}
}


XmlDocument::XmlDocument()
{
	m_pRootElement = nullptr;
	m_pTopElement = nullptr;
}

XmlDocument::~XmlDocument()
{
	//SAFE_DELETE(m_pRootElement);
	//SAFE_DELETE(m_pTopElement);
}

void RootElement::AddAttribute(const Attribute& attribute)
{
	AttributePtr pAttribute = std::make_shared<Attribute>();
	pAttribute->Key = attribute.Key;
	pAttribute->Value = attribute.Value;

	m_Atributes.push_back(pAttribute);
}


Element::Element()
{

}

Element::~Element()
{
	// Delete all attributes
	//for (auto pAttribute : m_Atributes)
	//{
	//	SAFE_DELETE(pAttribute);
	//}
	m_Atributes.clear();

	//// Recursively delete all child elements
	//for (uint32 i = 0; i < m_Elements.size(); i++)
	//{
	//	SAFE_DELETE(m_Elements[i]);
	//}
	m_Elements.clear();

}

void Element::FindElements(const char* pszElementName, std::list<Element>& elements)
{
	if (m_Name.compare(pszElementName) == 0)
		elements.push_back(*this);
	
	// Recursively find all elements
	for (auto pElement : m_Elements)
		pElement->FindElements(pszElementName, elements);
}

AttributePtr Element::GetAttributeByName(const std::string& name)
{
	for (auto pAttributeIter : m_Atributes)
	{
		if (pAttributeIter->Key == name)
		{
			return pAttributeIter;
		}
	}

	return nullptr;
}

void Element::AddAttribute(const Attribute& attribute)
{
	AttributePtr pAttribute = std::make_shared<Attribute>();
	pAttribute->Key = attribute.Key;
	pAttribute->Value = attribute.Value;

	m_Atributes.push_back(pAttribute);
}

void Element::AddChildElement(Element element)
{
	ElementPtr pElement = std::make_shared<Element>();
	pElement->m_Atributes = element.m_Atributes;
	pElement->m_Elements = element.m_Elements;
	pElement->m_Name = element.m_Name;
	pElement->m_Value = element.m_Value;
	m_Elements.push_back(pElement);
}

