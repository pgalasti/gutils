#pragma once
#include "stdafx.h"

#include "ZipPackageFile.h"
#include "GDefs.h"

using namespace GUtils::IO::Resources::Packaging;
using namespace GUtils::IO::Resources;

ZipPackageFile::ZipPackageFile(const std::string& zipFilePath)
{
	m_pZipLoader = nullptr;

	wchar_t szwPath[_MAX_PATH]; ZeroMemory(szwPath, _MAX_PATH);
	MultiByteToWideChar(0, 0, zipFilePath.c_str(), zipFilePath.length(), szwPath, zipFilePath.length());

	m_ZipFileName = std::wstring(szwPath);
}

ZipPackageFile::~ZipPackageFile()
{
	SAFE_DELETE(m_pZipLoader);
}

BOOL ZipPackageFile::Open()
{
	if (m_pZipLoader == nullptr)
		m_pZipLoader = new ZipLoader();

	if (m_pZipLoader)
		return m_pZipLoader->Init(m_ZipFileName);

	return FALSE;
}

uint32 ZipPackageFile::GetResourceSize(const PackageResource& resource)
{
	const int resourceNumber = m_pZipLoader->Find(resource.GetName());
	if (resourceNumber < 0)
		return 0;

	const int returnedSize = m_pZipLoader->GetFileLen(resourceNumber);
	if (returnedSize < 1)
		return 0;

	return (uint32)returnedSize;
}

uint32 ZipPackageFile::GetResource(const PackageResource& resource, sbyte8* pBuffer)
{
	const int resourceNumber = m_pZipLoader->Find(resource.GetName());
	if (resourceNumber < 0)
		return 0;
	
	const int returnedSize = m_pZipLoader->GetFileLen(resourceNumber);
	if (returnedSize < 0)
		return 0;

	m_pZipLoader->ReadFile(resourceNumber, pBuffer);
	return returnedSize;
}

uint32 ZipPackageFile::GetNumberOfResources() const
{
	if (m_pZipLoader == nullptr)
		return 0;

	const int numberOfFiles = m_pZipLoader->GetNumFiles();
	if (numberOfFiles < 1)
		return 0;

	return (uint32)numberOfFiles;
}

std::string ZipPackageFile::GetResourceName(const uint32 number) const
{
	std::string resourceName;
	if (m_pZipLoader != nullptr && number >= 0 && number < m_pZipLoader->GetNumFiles())
		resourceName = m_pZipLoader->GetFilename(number);

	return resourceName;
}