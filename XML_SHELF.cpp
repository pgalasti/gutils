//#pragma once
//#include "stdafx.h"
//
//#include "XML.h"
//#include "FileLoader.h"
//#include "UtilFunctions.h"
//#include "Parser.h"
//#include <set>
//#include <stack>
//
//using namespace GUtils::Utils::XML;
//using namespace GUtils::Utils::Functions;
//
//
//Element::Element()
//{
//
//}
//
//Element::~Element()
//{
//	// Delete all attributes
//	for (auto pAttribute : m_Atributes)
//	{
//		SAFE_DELETE(pAttribute);
//	}
//
//	// Recursively delete all child elements
//	for (auto pElement : m_Elements)
//	{
//		SAFE_DELETE(pElement);
//	}
//
//}
//
//void Element::FindElements(const char* pszElementName, std::list<Element>& elements)
//{
//	if (m_Name.compare(pszElementName) == 0)
//		elements.push_back(*this);
//
//	// Recursively find all elements
//	for (auto pElement : m_Elements)
//		pElement->FindElements(pszElementName, elements);
//}
//
//Attribute* Element::GetAttributeByName(const std::string& name)
//{
//	for (auto pAttributeIter : m_Atributes)
//	{
//		if (pAttributeIter->Key == name)
//		{
//			return &(*pAttributeIter);
//		}
//	}
//
//	return nullptr;
//}
//
//void Element::AddAttribute(const Attribute& attribute)
//{
//	Attribute* pAttribute = new Attribute();
//	pAttribute->Key = attribute.Key;
//	pAttribute->Value = attribute.Value;
//
//	m_Atributes.push_back(pAttribute);
//}
//
//XmlParser::XmlParser()
//{
//
//}
//
//XmlParser::~XmlParser()
//{
//
//}
//
//BOOL XmlParser::Parse(const char* pszFilePath, XmlDocument& xmlDocument)
//{
//	GUtils::IO::FileLoader fileLoader;
//	if (!fileLoader.Open(pszFilePath))
//	{
//		m_ErrorMessage = "Unable to open file path: " + std::string(pszFilePath);
//		return FALSE;
//	}
//
//	std::list<std::string> fileLines;
//	if (!fileLoader.GetFileLines(fileLines))
//	{
//		m_ErrorMessage = "Unable to read file lines for file path: " + std::string(pszFilePath);
//		return FALSE;
//	}
//
//
//	std::string giantString;
//	for (std::string line : fileLines)
//		giantString += line;
//
//	CleanString(giantString, "\n\r\t");
//
//	Element* pTopElement = nullptr;
//	uint32 start = 0;
//	RecursivelyBuildElement(&pTopElement, giantString, start);
//
//	fileLoader.Close();
//	return TRUE;
//}
//
//BOOL XmlParser::RecursivelyBuildElement(Element** ppParentElement, const std::string& remainingLines, uint32& charactersTraversed)
//{
//	std::string characterBuffer;
//
//	bool stateSeekingElementStart = true;
//	bool stateReadingElementName = false;
//	bool stateElementNameRead = false;
//	bool stateSeekAttribute = false;
//	bool stateSeekAttributeValue = false;
//	bool stateEndOfElement = false;
//	bool stateSeekElementContent = false;
//	bool stateClosingElement = false;
//	bool inStringQuotes = false;
//	Element* pNewElement = new Element();
//	if ((*ppParentElement) == nullptr)
//	{
//		*ppParentElement = nullptr;
//	}
//
//	std::string thisElementName;
//	std::string currentAttributeName;
//	for (uint32 i = 0; i < remainingLines.size(); i++)
//	{
//		const char currentCharacter = remainingLines[i];
//		++charactersTraversed;
//
//		if (stateSeekingElementStart)
//		{
//			if (currentCharacter != '<')
//				continue;
//
//			stateSeekingElementStart = false;
//			stateReadingElementName = true;
//			continue;
//		}
//
//		if (stateReadingElementName)
//		{
//			if (currentCharacter == '>' || currentCharacter == ' ' || currentCharacter == '/')
//			{
//				if (characterBuffer.empty())
//				{
//					if (currentCharacter != '/')
//						return FALSE;
//				}
//				pNewElement->m_Name = characterBuffer;
//				characterBuffer.clear();
//
//
//				stateReadingElementName = false;
//
//				if (currentCharacter == ' ')
//				{
//					stateSeekAttribute = true;
//					continue;
//				}
//				if (currentCharacter == '>')
//				{
//					stateSeekElementContent = true;
//					continue;
//				}
//				if (currentCharacter == '/')
//				{
//					stateClosingElement = true;
//					continue;
//				}
//			}
//			else
//			{
//				characterBuffer += currentCharacter;
//			}
//		}
//
//		if (stateSeekAttribute)
//		{
//			if (currentCharacter == ' ')
//				continue;
//
//			if (currentCharacter == '=')
//			{
//				if (characterBuffer.empty())
//				{
//					m_ErrorMessage = "Blank attribute for element " + std::string(pNewElement->m_Name);
//					return FALSE;
//				}
//
//				Attribute attribute;
//				currentAttributeName = attribute.Key = characterBuffer;
//				characterBuffer.clear();
//				if (pNewElement->GetAttributeByName(attribute.Key) != nullptr)
//				{
//					m_ErrorMessage = "Element " + std::string(pNewElement->m_Name) + std::string("Has a duplicate attribute: ") + std::string(attribute.Key);
//					return FALSE;
//				}
//
//				pNewElement->AddAttribute(attribute);
//				stateSeekAttribute = false;
//				stateSeekAttributeValue = true;
//				continue;
//			}
//
//			if (currentCharacter == '/')
//			{
//				stateEndOfElement = true;
//				return TRUE;
//			}
//
//			if (currentCharacter == '>')
//			{
//				stateSeekElementContent = true;
//				stateSeekAttribute = false;
//				continue;
//			}
//
//			characterBuffer += currentCharacter;
//			continue;
//		}
//
//		if (stateSeekAttributeValue)
//		{
//			if (currentCharacter == '"')
//			{
//				if (!inStringQuotes)
//				{
//					inStringQuotes = true;
//					continue;
//				}
//
//				if (characterBuffer.empty())
//				{
//					m_ErrorMessage = "Element: " + std::string(pNewElement->m_Name) + std::string(" contains an empty attribute value for attribute") + std::string(currentAttributeName);
//					return FALSE;
//				}
//
//
//				Attribute* pAttribute = pNewElement->GetAttributeByName(currentAttributeName);
//				pAttribute->Value = characterBuffer;
//				characterBuffer.clear();
//				stateSeekAttributeValue = false;
//				inStringQuotes = false;
//				stateSeekAttribute = true;
//				continue;
//			}
//
//
//
//			characterBuffer += currentCharacter;
//		}
//
//		if (stateSeekElementContent)
//		{
//			if (currentCharacter == ' ')
//				continue;
//
//			if (currentCharacter == '<')
//			{
//				// Grab content
//				pNewElement->m_Value = characterBuffer;
//				const char* debug = pNewElement->m_Name.c_str();
//				characterBuffer.clear();
//				// Find remaining elements
//				uint32 traversed = 0;
//				if (!RecursivelyBuildElement(&pNewElement, remainingLines.substr(i, remainingLines.size()), traversed))
//					return FALSE;
//				charactersTraversed += traversed;
//				i += traversed;
//
//				if (pNewElement)
//				{
//					//stateSeekingElementStart = true;
//					(*(ppParentElement))->m_Elements.push_back(pNewElement);
//				}
//
//				continue;
//			}
//
//			characterBuffer += currentCharacter;
//		}
//
//		if (stateClosingElement)
//		{
//			if (currentCharacter != '>')
//				continue;
//
//			if (pNewElement->m_Name.empty())
//			{
//				SAFE_DELETE(pNewElement);
//			}
//			--charactersTraversed;
//			return TRUE;
//		}
//
//	}
//
//	return TRUE;
//}