-- Helper functions for object orientation management

function class(baseClass, body)
	local ret = body or {};

	-- If there's a base class, attach our new class to it
	if(baseClass ~= nil) then
		setmetatable(ret, ret);
		ret.__index = baseClass;
	end

	-- Add the Create() function
	ret.Create = function(self, constructionData, originalSubClass)

		local object;

		if(self.__index ~= nil) then
			if(originalSubClass ~= nil) then
				object = self.__index:Create(constructionData, originalSubClass);
			else
				object = self.__index:Create(constructionData, self);
			end
		else
			object = constructionData or {};
		end

		setmetatable(object, object);
		object.__index = self;

		-- Copy any operators over
		if(self.__operators ~= nil) then
			for key, val in pairs(self.__operators) do
				object[key] = val;
			end
		end

		return object;
	end

	return ret;
end