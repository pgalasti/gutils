require "InitOOFramework"

-- 2D Vector Class
Vector2D = class(nil,
{
	x = 0, y = 0
})

function Vector2D:Length()
	return math.sqrt((self.x*self.x) + (self.y*self.y))
end


-- 3D Vector Class
Vector3D = class(nil,
{
	x = 0, y = 0, z = 0
})

function Vector3D:Length()
	return math.sqrt((self.x*self.x) + (self.y*self.y) + (self.z*self.z));
end

testInScript = 100
