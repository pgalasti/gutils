#include "simple_struct.fx"

//--------------------------------------------------------------------------------------
// Vertex Shader
//--------------------------------------------------------------------------------------
PSInput VS(VSInput input)
{
	PSInput output = (PSInput)0;
	output.Pos = mul(input.Pos, WorldViewProjection);
	output.Color = input.Color;

	return output;
}


