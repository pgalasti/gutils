#pragma once
#include "stdafx.h"

#include "ShapeBuilder.h"
#include "TestMesh.h"

using namespace GraphicsEngine::Model;

/*static */
void ShapeBuilder::CreateBox(const float width, const float height, const float depth, TestMesh& meshData)
{
	// TEMP CODE BEGIN 
	SimpleVertex vertices[] =
	{
		{ XMFLOAT3(-width, height, -depth), XMFLOAT4(0.0f, 0.0f, 1.0f, 1.0f) },
		{ XMFLOAT3(width, height, -depth), XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f) },
		{ XMFLOAT3(width, height, depth), XMFLOAT4(0.0f, 1.0f, 1.0f, 1.0f) },
		{ XMFLOAT3(-width, height, depth), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f) },
		{ XMFLOAT3(-width, -height, -depth), XMFLOAT4(1.0f, 0.0f, 1.0f, 1.0f) },
		{ XMFLOAT3(width, -height, -depth), XMFLOAT4(1.0f, 1.0f, 0.0f, 1.0f) },
		{ XMFLOAT3(width, -height, depth), XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f) },
		{ XMFLOAT3(-width, -height, depth), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) },
	};
	WORD indices[] =
	{
		3, 1, 0,
		2, 1, 3,

		0, 5, 4,
		1, 5, 0,

		3, 4, 7,
		0, 4, 3,

		1, 6, 5,
		2, 6, 1,

		2, 7, 6,
		3, 7, 2,

		6, 4, 5,
		7, 4, 6,
	};

	meshData.SetVerticies(vertices, 8, indices, 36);
}