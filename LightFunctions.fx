#include "LightStruct.fx"

void ComputeDirectionalLight(
	Material material,
	DirectionalLight lightSource,
	float3 normal, 
	float3 toEyeVector,
	out float4 ambient, 
	out float4 diffuse, 
	out float4 specular)
{
	ambient = float4(0.0f, 0.0f, 0.0f, 0.0f);
	diffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
	specular = float4(0.0f, 0.0f, 0.0f, 0.0f);

	// Light vector opposite of incoming light to perform normal calculation
	float3 lightVector;
	lightVector = -lightSource.Direction;
	
	// Ambient is for scattered light
	ambient = material.Ambient * lightSource.Ambient;

	// Diffuse factor will be between -1.0 and 1.0. Anything over 0.0 will reflect diffuse and specular light.
	float diffuseFactor;
	diffuseFactor = dot(lightVector, normal);
	
	[flatten]
	if (diffuseFactor > 0.0f)
	{
		float3 reflection;
		reflection = reflect(-lightVector, normal);

		// Specular factor 
		float specFactor;
		specFactor = pow(max(dot(reflection, toEyeVector), 0.0f), material.Specular.w);

		diffuse = diffuseFactor * material.Diffuse * lightSource.Diffuse;
		specular = specFactor * material.Specular * lightSource.Specular;
	}
}

void ComputerPointLight(
	Material material,
	PointLight lightSource,
	float3 position,
	float3 normal, float3 toEye,
	out float4 ambient,
	out float4 diffuse,
	out float4 specular)
{
	ambient = float4(0.0f, 0.0f, 0.0f, 0.0f);
	diffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
	specular = float4(0.0f, 0.0f, 0.0f, 0.0f);

	float3 lightVector;
	lightVector = lightSource.Position - position;

	float distance = length(lightVector);

	if (distance > lightSource.Range)
		return;

	lightVector /= distance;

	ambient = material.Ambient * lightSource.Ambient;

	float diffuseFactor = dot(lightVector, normal);

	[flatten]
	if (diffuseFactor > 0.0f)
	{
		float3 vec;
		vec = reflect(-lightVector, normal);
		float specFactor = pow(max(dot(vec, toEye), 0.0f), material.Specular.w);

		diffuse = diffuseFactor * material.Diffuse * lightSource.Diffuse;
		specular = specFactor * material.Specular * lightSource.Specular;
	}

	float att = 1.0f / dot(lightSource.Attenuation, float3(1.0f, distance, distance*distance));

	diffuse *= att;
	specular *= att;
}

void ComputeSpotLight(
	Material material,
	SpotLight lightSource,
	float3 position,
	float3 normal,
	float3 toEye,
	out float4 ambient,
	out float4 diffuse,
	out float4 specular)
{
	ambient = float4(0.0f, 0.0f, 0.0f, 0.0f);
	diffuse = float4(0.0f, 0.0f, 0.0f, 0.0f);
	specular = float4(0.0f, 0.0f, 0.0f, 0.0f);

	float3 lightVector;
	lightVector = lightSource.Position - position;

	float distance = length(lightVector);

	if (distance > lightSource.Range)
		return;

	lightVector /= distance;

	ambient = material.Ambient * lightSource.Ambient;

	float diffuseFactor = dot(lightVector, normal);

	[flatten]
	if (diffuseFactor > 0.0f)
	{
		float3 vec;
		vec = reflect(-lightVector, normal);
		float specFactor = pow(max(dot(vec, toEye), 0.0f), material.Specular.w);

		diffuse = diffuseFactor * material.Diffuse * lightSource.Diffuse;
		specular = specFactor * material.Specular * lightSource.Specular;
	}

	float spot = pow(max(dot(-lightVector, lightSource.Direction), 0.0f), lightSource.Spot);

	float att = spot / dot(lightSource.Attenuation, float3(1.0f, distance, distance*distance));
	
	ambient *= spot;
	diffuse *= att;
	specular *= att;
}